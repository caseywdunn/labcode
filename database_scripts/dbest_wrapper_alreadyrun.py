
import sys,os
import sqlite3
from Bio import SeqIO
import mkdbdir
import os.path
import dbest_wrapper

"""
this is going to go ahead and run the mkdbdir and individual scripts 
for automation of the dbest

the only input is the ncbi_id
"""

def get_parent_info(_id,cur):
	cur.execute("SELECT id from attributes where name = ?",("parent_id",))
	paid = str(cur.fetchall()[0][0])
	cur.execute("SELECT value from directories_triple where directories_id = "+str(_id)+" and attribute_id = "+str(paid),())
	parentid = str(cur.fetchall()[0][0])
	#print "parent:", parentid
	cur.execute("SELECT * from directories where id = ?",(parentid,))
	a = cur.fetchall()[0]
	padir = str(a[1])
	patype = str(a[2])
	return [parentid,padir,patype]


def run_partigene(ncbi_id,in_dir,con,cur):
	parti_id = mkdbdir.partigene_create(ncbi_id,con,cur)
	cur.execute("SELECT path from directories where id = ?",(parti_id,))
	partigenedir = str(cur.fetchall()[0][0])
	curdir = os.getcwd()
	print "changing directory to "+partigenedir
	os.chdir(partigenedir)
	#need to copy the partigene directory here
	assert os.path.exists(in_dir+"/partigene")
	os.system("cp -r "+in_dir+"/partigene .")
	#need to copy the postcluster directory here
	assert os.path.exists(in_dir+"/postcluster")
	os.system("cp -r "+in_dir+"/postcluster .")
	return parti_id

def run_blastx(parti_id,in_blastfile,con,cur):
	blastx_id = mkdbdir.blastx_create(parti_id,con,cur)
	cur.execute("SELECT path from directories where id = ?",(blastx_id,))
	blastdir = str(cur.fetchall()[0][0])
	assert os.path.exists(in_blastfile)
	print "copying blast file"
	os.system("cp "+in_blastfile+" "+blastdir+"/processed.blast.xml")
	return blastx_id
	
def run_prot4est(blastx_id,parti_id,in_dir,con,cur):
	prot4est_id = mkdbdir.prot4est_create([blastx_id,parti_id],con,cur)
	cur.execute("SELECT path from directories where id = ?",(prot4est_id,))
	prot4est_dir = str(cur.fetchall()[0][0])
	#change directory
	os.chdir(prot4est_dir)
	assert os.path.exists(in_dir)
	print "copying prot4est"
	os.system("cp -r "+in_dir+"/* .")
	return [prot4est_id,prot4est_dir]
	
def run_blast2go(blastx_id,con,cur):
	blast2go_id = mkdbdir.blast2go_create(blastx_id,con,cur)
	cur.execute("SELECT path from directories where id  = ?", (blast2go_id,))
	blast2go_dir = str(cur.fetchall()[0][0])
	#change directory
	os.chdir(blast2go_dir)
	os.system("python /home/smitty/Dropbox/programming/parseblast.py")
	os.system("java -Xms1024m -Xmx2048m -jar ~/apps/b2g4pipe/blast2go.jar -in "+parseblastfile+" -v -a -out "+parseblastfile+".annot -prop ~/blast2go/blast2go.properties")

def enter_sequences_into_db_from_prot4est(fid,fpath,con,cur):
	dbest_wrapper.enter_sequences_into_db_from_prot4est(fid,fpath,con,cur)

nrdb = "/home/smitty/lib/blast_data/nr"

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: dbest_wrapper_already_run ncbi_id partigene_dir blast.xml prot4est_dir"
		sys.exit(0)
	con = sqlite3.connect(mkdbdir.database)
	cur = con.cursor()
	ncbi_id = sys.argv[1]
	partigene_dir = sys.argv[2]
	blast_xml = sys.argv[3]
	prot4est_dir = sys.argv[4]
	parti_id = run_partigene(ncbi_id,partigene_dir,con,cur)
	blastx_id = run_blastx(parti_id,blast_xml,con,cur)
	p4estid,p4estdir = run_prot4est(blastx_id,parti_id,prot4est_dir,con,cur)
	enter_sequences_into_db_from_prot4est(p4estid,p4estdir,con,cur)
