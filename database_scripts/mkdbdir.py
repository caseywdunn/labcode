import sys,os
import sqlite3 #this should be mysql when it is actually up and running but for now this will do
from Bio import Entrez

"""
NOTES AND GENERAL STANDARDS
the mkdir commands general have the pattern species name (with underscores instead of spaces)
and periods seperating the name, the command, and the guid from the directory table in the database

CURRENT TYPES OF ANALYSES OR RUNS OR DIRECTORIES SUPPORTED IN THIS SCRIPT

assemblies
partigene

annotations
prot4est,blastx,blastp,blast2go
"""


#this is obviously needing to be hardcoded to the database stats (where, how to connect, etc.)
#in the future it would actually be easier to just have some sort of configuration file to read

database = "/home/smitty/Dropbox/programming/web2py/applications/dunn_db/databases/storage.sqlite"
basedir = "/home/smitty/Desktop/test"
types = ['partigene','blastx','blastp','blast2go','prot4est','oases']
types.sort()

def print_help(typ):
	print "help for " + typ
	print "======================"
	if typ == "partigene":
		print "typical usage: mkdbdir partigene ncbiid"
		print "-----"
		print "The ncbi that is given will serve as the search term in the partigene protocol. It is better if the name with this ncbi id is entered in the taxonomy table beforehand but if it isn't it will be entered (assuming that a reasonable name can be found in NCBI given the number submitted"
	if typ == "prot4est":
		print "typical usage: mkdbdir prot4est parent_guid_id1 parent_guid_id2"
		print "-----"
		print "The parent_guid_id should refer to a parent that would have the relevant information to perform the prot4est analyses. One of these should be a blast type of analysis and the other should be an assembly or sopme other type that has the nucleotide information"
	if typ == "blastx":
		print "typical usage: mkdbdir blastx parent_guid_id"
	if typ == "blastp":
		print "typical usage: mkdbdir blastp parent_guid_id"
	if typ == "blast2go":
		print "typical usage: mkdbdir blast2go parent_guid_id"
		print "-----"
		print "The parent_guid_id should refer to a blastx, blastp, blast run"

def partigene_create(arg,con,cur):
	ncbi_id = arg
	found = False
	#first see if there is a species with this ncbi_id in the database
	cur.execute("SELECT id,name from taxa where ncbi_id = ?",(ncbi_id,))
	a = cur.fetchall()
	name = ""
	tid = ""
	if len(a) != 0:
		found = True
		name = a[0][1]
		tid = a[0][0]
	#if there isn't one, then check to see if there is a name that is the same, 
	#based on the names derived from ncbi
	derived_name = ""
	if found == False:
		Entrez.email = "something@gmail.com"
		handle = Entrez.efetch(db="taxonomy",id=ncbi_id,report="brief")
		t = handle.read()
		derived_name = t.split("<pre>")[1].split("</pre>")[0].strip()
		cur.execute("SELECT id from taxa where name = ?",(derived_name,))
		a = cur.fetchall()
		if len(a) != 0:
			found = True
			name = derived_name
			tid = a[0][0]
	#finally, if there isn't one, we can enter that taxon into the database complete
	#with the ncbi_id
	if found == False:
		#assuming that derived name worked from before -- if not it will crash
		if len(derived_name) == 0:
			print "you need to just go ahead and enter this taxon in the database"
			print "I got this nothing from searching NCBI"
			sys.exit(0)
		tid = cur.execute("INSERT into taxa (ncbi_id,name) values (?, ?);" ,(ncbi_id,derived_name))
		tid = tid.lastrowid
		con.commit()
		name = derived_name
	id_ = cur.execute("INSERT into directories (type) values (?);" ,("partigene",))
	id_ = id_.lastrowid
	cur.execute("UPDATE directories set path = '"+basedir+"/assemblies/"+name.replace(" ","_")+".partigene."+str(id_)+"' where id = "+str(id_)+";")
	mkdircmd = "mkdir "+basedir+"/assemblies/"+name.replace(" ","_")+".partigene."+str(id_)
	con.commit()
	print "making directory "+mkdircmd
	os.system(mkdircmd)
	#add some initial attributes
	#taxon id
	cur.execute("SELECT id from attributes where name = ?",("taxa_id",))
	a = cur.fetchall()
	taid = str(a[0][0])
	cur.execute("INSERT into directories_triple (directories_id,attribute_id,value) values (?,?,?);",(id_,taid,tid))
	con.commit()
	return id_

def blastx_create(arg,con,cur):
	#get the parent
	pid = arg
	cur.execute("SELECT directories_triple.value from directories_triple where directories_triple.directories_id = ? and directories_triple.attribute_id = 1;",(pid,))
	taxonid = str(cur.fetchall()[0][0])
	cur.execute("SELECT name from taxa where id = ?;",(taxonid,))
	name = str(cur.fetchall()[0][0])
	id_ = cur.execute("INSERT into directories (type) values (?);" ,("blastx",))
	id_ = id_.lastrowid
	cur.execute("UPDATE directories set path = '"+basedir+"/annotations/"+name.replace(" ","_")+".blastx."+str(id_)+"' where id = "+str(id_)+";")
	mkdircmd = "mkdir "+basedir+"/annotations/"+name.replace(" ","_")+".blastx."+str(id_)
	con.commit()
	print "making directory "+mkdircmd
	os.system(mkdircmd)
	#add some initial attributes
	#taxon id
	cur.execute("SELECT id from attributes where name = ?",("taxa_id",))
	a = cur.fetchall()
	taid = str(a[0][0])
	cur.execute("INSERT into directories_triple (directories_id,attribute_id,value) values (?,?,?);",(id_,taid,taxonid))
	con.commit()
	#parent id
	cur.execute("SELECT id from attributes where name = ?",("parent_id",))
	a = cur.fetchall()
	paid = str(a[0][0])
	cur.execute("INSERT into directories_triple (directories_id,attribute_id,value) values (?,?,?);",(id_,paid,pid))
	con.commit()
	return id_

def blastp_create(arg,con,cur):
	#get the parent
	pid = arg
	cur.execute("SELECT directories_triple.value from directories_triple where directories_triple.directories_id = ? and directories_triple.attribute_id = 1;",(pid,))
	taxonid = str(cur.fetchall()[0][0])
	cur.execute("SELECT name from taxa where id = ?;",(taxonid,))
	name = str(cur.fetchall()[0][0])
	id_ = cur.execute("INSERT into directories (type) values (?);" ,("blastp",))
	id_ = id_.lastrowid
	cur.execute("UPDATE directories set path = '"+basedir+"/annotations/"+name.replace(" ","_")+".blastp."+str(id_)+"' where id = "+str(id_)+";")
	mkdircmd = "mkdir "+basedir+"/annotations/"+name.replace(" ","_")+".blastp."+str(id_)
	con.commit()
	print "making directory "+mkdircmd
	os.system(mkdircmd)
	#add some initial attributes
	#taxon id
	cur.execute("SELECT id from attributes where name = ?",("taxa_id",))
	a = cur.fetchall()
	taid = str(a[0][0])
	cur.execute("INSERT into directories_triple (directories_id,attribute_id,value) values (?,?,?);",(id_,taid,taxonid))
	con.commit()
	#parent id
	cur.execute("SELECT id from attributes where name = ?",("parent_id",))
	a = cur.fetchall()
	paid = str(a[0][0])
	cur.execute("INSERT into directories_triple (directories_id,attribute_id,value) values (?,?,?);",(id_,paid,pid))
	con.commit()
	return id_

def prot4est_create(args,con,cur):
	blastT = False
	seqT = False
	cur.execute("SELECT type from directories where id = ?",(args[0],))
	type1 = str(cur.fetchall()[0][0])
	cur.execute("SELECT type from directories where id = ?",(args[1],))
	type2 = str(cur.fetchall()[0][0])
	one = True
	if type1 in ["blastx","blastp"]:
		blastT = True
	elif type2 in ["blastx","blastp"]:
		blastT = True
		one = False
	if one == False and type1 in ['partigene','genome'] :
		seqT = True
	elif type2 in ['partigene','genome']:
		seqT = True
	oneid = args[0]
	twoid = args[1]
	if blastT == False or seqT == False:
		print_help(prot4est)
		sys.exit(0)
	#we are going to make sure that oneid is the blast and twoid is the seq
	if one == False:
		oneid = args[1]
		twoid = args[0]
		temp = type1
		type1 = type2
		type2 = temp
	cur.execute("SELECT directories_triple.value from directories_triple where directories_triple.directories_id = ? and directories_triple.attribute_id = 1;",(oneid,))
	taxonid = str(cur.fetchall()[0][0])
	cur.execute("SELECT name from taxa where id = ?;",(taxonid,))
	name = str(cur.fetchall()[0][0])
	id_ = cur.execute("INSERT into directories (type) values (?);" ,("prot4est",))
	id_ = id_.lastrowid
	cur.execute("UPDATE directories set path = '"+basedir+"/annotations/"+name.replace(" ","_")+".prot4est."+str(id_)+"' where id = "+str(id_)+";")
	mkdircmd = "mkdir "+basedir+"/annotations/"+name.replace(" ","_")+".prot4est."+str(id_)
	con.commit()
	print "making directory "+mkdircmd
	os.system(mkdircmd)
	#add some initial attributes
	#taxon id
	cur.execute("SELECT id from attributes where name = ?",("taxa_id",))
	a = cur.fetchall()
	taid = str(a[0][0])
	cur.execute("INSERT into directories_triple (directories_id,attribute_id,value) values (?,?,?);",(id_,taid,taxonid))
	con.commit()
	#parent id
	cur.execute("SELECT id from attributes where name = ?",("parent_id",))
	a = cur.fetchall()
	paid = str(a[0][0])
	cur.execute("INSERT into directories_triple (directories_id,attribute_id,value) values (?,?,?);",(id_,paid,oneid))
	con.commit()
	#parent id
	cur.execute("INSERT into directories_triple (directories_id,attribute_id,value) values (?,?,?);",(id_,paid,twoid))
	con.commit()
	return id_

def blast2go_create(arg,con,cur):
	pid = arg
	cur.execute("SELECT directories_triple.value from directories_triple where directories_triple.directories_id = ? and directories_triple.attribute_id = 1;",(pid,))
	taxonid = str(cur.fetchall()[0][0])
	cur.execute("SELECT name from taxa where id = ?;",(taxonid,))
	name = str(cur.fetchall()[0][0])
	id_ = cur.execute("INSERT into directories (type) values (?);" ,("blast2go",))
	id_ = id_.lastrowid
	cur.execute("UPDATE directories set path = '"+basedir+"/annotations/"+name.replace(" ","_")+".blast2go."+str(id_)+"' where id = "+str(id_)+";")
	mkdircmd = "mkdir "+basedir+"/annotations/"+name.replace(" ","_")+".blast2go."+str(id_)
	con.commit()
	print "making directory "+mkdircmd
	os.system(mkdircmd)
	#add some initial attributes
	#taxon id
	cur.execute("SELECT id from attributes where name = ?",("taxa_id",))
	a = cur.fetchall()
	taid = str(a[0][0])
	cur.execute("INSERT into directories_triple (directories_id,attribute_id,value) values (?,?,?);",(id_,taid,taxonid))
	con.commit()
	#parent id
	cur.execute("SELECT id from attributes where name = ?",("parent_id",))
	a = cur.fetchall()
	paid = str(a[0][0])
	cur.execute("INSERT into directories_triple (directories_id,attribute_id,value) values (?,?,?);",(id_,paid,pid))
	con.commit()
	return id_

if __name__ == "__main__":
	if len(sys.argv) <= 1:
		print "usage: python mkdbdir.py typeofrun ..."
		print "	current supported types:"
		print "\t"+", ".join(types)
		print " "
		print "	for more info: mkdbdir help type"
		sys.exit(0)
	con = sqlite3.connect(database)
	cur = con.cursor()

	intype = sys.argv[1]

	if intype == "help":
		if len(sys.argv) > 2:
			if sys.argv[2] not in types:
				print sys.argv[2] + " not recognized"
				sys.exit(0)
			print_help(sys.argv[2])
		else:
			print "enter a help term"
		sys.exit(0)
	if intype not in types:
		print sys.argv[1] + " not recognized"
		sys.exit(0)
	#get the type of directory structure that the user wants to create
	###
	# partigene
	###
	if intype == "partigene":
		print "mkdbdir partigene"
		#partigene will require a ncbitaxonid for this (which will fetch the name from the 
		#database). if the name isn't there then it will crash - or it could insert it depending
		#on which is cleaner
		if len(sys.argv) != 3:
			print_help('partigene')
			sys.exit(0)
		partigene_create(sys.argv[2],con,cur)
		#now we can run partigene
		#create the config file so that partigene can run what is in the config
		sys.exit(0)
	###
	# prot4est
	###
	if intype == "prot4est":
		print "mkdbdir prot4est"
		if len(sys.argv) != 4:
			print_help('prot4est')
			sys.exit(0)
		#this requires that there are two parents, one that is blast and one that is not
		prot4est_create(sys.argv[2:],con,cur)
		sys.exit(0)
	###
	# blastx
	###
	if intype == "blastx":
		print "mkdbdir blastx"
		if len(sys.argv) != 3:
			print_help('blastx')
			sys.exit(0)
		blastx_create(sys.argv[2],con,cur)
		#now conduct the blast
		sys.exit(0)
	###
	# blastp
	###
	if intype == "blastp":
		print "mkdbdir blastp"
		if len(sys.argv) != 3:
			print_help('blastp')
			sys.exit(0)
		blastp_create(sys.argv[2],con,cur)
		#now conduct the blast
		sys.exit(0)
	###
	# blast2go
	###
	if intype == "blast2go":
		print "mkdbdir blast2go"
		if len(sys.argv) != 3:
			print_help('blast2go')
			sys.exit(0)
		#get the parent
		blast2go_create(sys.argv[2],con,cur)
		#now conduct the blast
		sys.exit(0)
	con.close()
