CREATE TABLE auth_user(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    first_name CHAR(128),
    last_name CHAR(128),
    email CHAR(512),
    password CHAR(512),
    registration_key CHAR(512),
    reset_password_key CHAR(512)
);
CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE auth_group(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    role CHAR(512),
    description TEXT
);
CREATE TABLE auth_membership(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    group_id INTEGER REFERENCES auth_group(id) ON DELETE CASCADE
);
CREATE TABLE auth_permission(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    group_id INTEGER REFERENCES auth_group(id) ON DELETE CASCADE,
    name CHAR(512),
    table_name CHAR(512),
    record_id INTEGER
);
CREATE TABLE auth_event(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    time_stamp TIMESTAMP,
    client_ip CHAR(512),
    user_id INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    origin CHAR(512),
    description TEXT
);
CREATE TABLE directories(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    path TEXT,
    type TEXT,
    last_updated_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    last_updated_on TIMESTAMP
);
CREATE TABLE attributes(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    description TEXT
);
CREATE TABLE directories_triple(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    directories_id INTEGER REFERENCES directories(id) ON DELETE CASCADE,
    attribute_id INTEGER REFERENCES attributes(id) ON DELETE CASCADE,
    value TEXT,
    last_updated_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    last_updated_on TIMESTAMP
);
CREATE TABLE taxa(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    ncbi_id TEXT,
    last_updated_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    last_updated_on TIMESTAMP
);
CREATE TABLE sequences(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    sequence_name TEXT,
    parent_id TEXT,
    taxa_id INTEGER REFERENCES taxa(id) ON DELETE CASCADE,
    nucleotide_seq TEXT,
    quality_seq TEXT,
    translated_seq TEXT,
    translation_method TEXT,
    masked_seq TEXT,
    human_readable_blast_hit TEXT,
    last_updated_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    last_updated_on TIMESTAMP
);
CREATE TABLE clades(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    parent_id INTEGER,
    last_updated_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    last_updated_on TIMESTAMP
);
CREATE TABLE clade_map(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    clade_id INTEGER REFERENCES clades(id) ON DELETE CASCADE,
    taxon_id INTEGER REFERENCES taxa(id) ON DELETE CASCADE
);
CREATE TABLE cluster_map(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    cluster_id INTEGER,
    sequence_id INTEGER REFERENCES sequences(id) ON DELETE CASCADE,
    last_updated_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    last_updated_on TIMESTAMP
);
CREATE TABLE cluster_runs(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    method TEXT,
    version TEXT,
    method_call TEXT,
    inflation DOUBLE,
    cutoff DOUBLE,
    last_updated_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    last_updated_on TIMESTAMP
);
