import sys, dendropy
import os
import itertools
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from ete3 import Tree
from collections import defaultdict

# returns a list consisting of elements (tree, DNCs for tree)
# input: Dendropy treelist
def tree_DNC_mapping(treelist):
    tree_DNCs = []
    for tree in treelist:
        ete_tree = Tree(tree.as_string("newick")[5:])
        DNCs = defaultdict(list)
        for leaf in ete_tree.get_leaves():
            DNC=leaf.name.split('@')[1][:-3]
            leaf.add_feature("DNC", DNC)
            DNCs[DNC].append(leaf.name)

        # filter out singletons
        removed_uniques = dict(filter(lambda (x,y): len(y)>1, DNCs.items()))
        tree_DNCs.append((ete_tree,removed_uniques))

    return tree_DNCs

# computes number of DNC non-singleton monophylies as well as number of DNCs for a given tree
# input: ete2 tree, dictionary of DNCs associated with that tree, output in which to write trees with non-monophyletic DNCs
def monophylies(tree, DNC_dict, non_mono_file):

    num_DNCs = len(DNC_dict.keys())
    num_monophylies = 0
    flag=0

    for leaves in DNC_dict.values():
        if tree.check_monophyly(values=leaves, target_attr="name")[0]:
            num_monophylies = num_monophylies+1
        else:
            flag = 1
            """
            with open(non_mono_file, 'a') as f:
                for item in leaves:
                    f.write("%s\n" % item)
                f.write('\n')

    if flag == 1:
        with open(non_mono_file, 'a') as f:
            f.write(tree.write(format=5))
            f.write('\n')
            f.write('=============')
            """

    # output: number of DNC combinations, number of DNC combinations that are monophyletic
    return num_DNCs, num_monophylies

# creates a histogram of branch lengths for genes with the same DNC across all trees
def histogram(tree_DNCs):
    branch_lengths=[]
    for tree, DNC_dict in tree_DNCs:
        for leaves in DNC_dict.values():
            leaf_pairs = list(itertools.combinations(leaves,2))
            ancestors = list(map(lambda (x,y): tree.get_common_ancestor(x,y), leaf_pairs))

            for pair,ancestor in itertools.izip(leaf_pairs, ancestors):
                branch_length=max(ancestor.get_distance(pair[0]),ancestor.get_distance(pair[1]))
                branch_lengths.append(branch_length)        

    with open("branch_lengths_list.txt", 'w') as f:
        f.write(str(branch_lengths))

    # figure style based on Randal Olson's guides
    plt.figure()
    ax = plt.subplot(111)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.xlabel("Branch Lengths", fontsize=16)
    plt.ylabel("Count", fontsize=16)
    plt.hist(branch_lengths, color="#3F5D7D", bins=100)
    plt.savefig("branch-length-dist.png", bbox_inches="tight")

# redoes the histogram for branch lengths
def histogram2(branch_length_file):
    branch_lengths=[]
    with open("branch_lengths_list.txt", 'r') as f:
        branch_lengths=list(f.read())

    # figure style based on Randal Olson's guides
    plt.figure()
    ax = plt.subplot(111)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.xlabel("Branch Lengths", fontsize=16)
    plt.ylabel("Count", fontsize=16)
    plt.hist(branch_lengths, color="#3F5D7D", bins=1000)
    plt.savefig("branch-length-dist.png", bbox_inches="tight")

if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser(description="Computes proportion of sequences with same DN,C from trinity that fall into different gene trees")
    p.add_argument('-t', '--tree', help='sequence file')
    p.add_argument('-o', '--out', help='file to output non-monophyletic tips and associated trees to')
    opts = p.parse_args()

    file_name = opts.tree
    out=opts.out
    treelist = dendropy.TreeList()
    treelist.read_from_path(file_name, schema="newick")

    tree_DNCs = tree_DNC_mapping(treelist)

    # computer number of DNCs and monophylies across all trees
    num_DNCs=0
    num_monophylies=0
    num_singles=0
    for tree, DNC_dict in tree_DNCs:
        x,y=monophylies(tree,DNC_dict,out)
        num_DNCs = num_DNCs+x
        num_monophylies=num_monophylies+y

    print "number of sp+DN+c combinations: ", num_DNCs
    print "number of sp+DN+c combinations that are not monophyletic: ", num_DNCs-num_monophylies

    # creates histogram
    histogram(tree_DNCs)
