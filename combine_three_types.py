import sys

"""
should combine three files for the SOLID, helicos, illumina data
"""

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python combine_three_types.py helicos_in illumina_in SOLID_in outfile"
		sys.exit(0)
	master_contigs = []
	h_data = {}#key is contig, value is list of forward reverse list
	i_data = {}
	s_data = {}
	print "reading helicos"

	infh = open(sys.argv[1],"r")
	first = True
	count = 0
	current = None
	currentlist = []
	for i in infh:
		if first == True:
			first = False
			continue
		spls = i.strip().split("\t")
		if current == None:
			current = spls[0]
		if current != spls[0]:
			if current not in master_contigs:
				master_contigs.append(current)
			h_data[current] = currentlist
			current = spls[0]
			currentlist = []
		fr = [spls[1],spls[2]]
		currentlist.append(fr)
		count += 1
		if count % 100000 == 0:
			print count,len(master_contigs),len(h_data)
	infh.close()
	#put in last one
	if current not in master_contigs:
		master_contigs.append(current)
	h_data[current] = currentlist
	
	print "reading illumina"
	
	infi = open(sys.argv[2],"r")
	first = True
	count = 0
	current = None
	currentlist = []
	for i in infi:
		if first == True:
			first = False
			continue
		spls = i.strip().split("\t")
		if current == None:
			current = spls[0]
		if current != spls[0]:
			if current not in master_contigs:
				master_contigs.append(current)
			i_data[current] = currentlist
			current = spls[0]
			currentlist = []
		fr = [spls[1],spls[2]]
		currentlist.append(fr)
		count += 1
		if count % 100000 == 0:
			print count,len(master_contigs),len(i_data)
	infi.close()
	if current not in master_contigs:
		master_contigs.append(current)
	i_data[current] = currentlist
	
	print "reading solid"
	
	infs = open(sys.argv[3],"r")
	first = True
	count = 0
	current = None
	currentlist = []
	for i in infs:
		if first == True:
			first = False
			continue
		spls = i.strip().split("\t")
		if current == None:
			current = spls[0]
		if current != spls[0]:
			if current not in master_contigs:
				master_contigs.append(current)
			s_data[current] = currentlist
			current = spls[0]
			currentlist = []
		fr = [spls[1],spls[2]]
		currentlist.append(fr)
		count += 1
		if count % 100000 == 0:
			print count,len(master_contigs),len(s_data)
	infs.close()
	if current not in master_contigs:
		master_contigs.append(current)
	s_data[current] = currentlist

	outf = open(sys.argv[4],"w")
	outf.write("contig\tHF\tHR\tIF\tIR\tSF\tSR\n")
	for i in master_contigs:
		print "writing contig: "+i
		he = []
		try:
			he = h_data[i]
		except:
			he = []
		il = []
		try:
			il = i_data[i]
		except:
			il = []
		so = []
		try:
			so = s_data[i]
		except:
			so = []
		if len(he) == 0:
			te = il
			if len(te) == 0:
				te = so
			for j in te:
				he.append([0,0])
		if len(il) == 0:
			te = he
			if len(te) == 0:
				te = so
			for j in te:
				il.append([0,0])
		if len(so) == 0:
			te = he
			if len(te) == 0:
				te = il
			for j in te:
				so.append([0,0])
		if len(he) != len(il):
			print "problem between length helicos and illumina",len(he),len(il)
			outf.write(str(he)+"\n")
			outf.write(str(il))
			sys.exit(0)
		if len(he) != len(so):
			print "problem between length helicos and solid",len(he),len(so)
			sys.exit(0)
		if len(il) != len(so):
			print "problem between length illumina and solid",len(il),len(so)
			sys.exit(0)
		for j in range(len(he)):
			stri = str(i)+"\t"+str(he[j][0])+"\t"+str(he[j][1])+"\t"+str(il[j][0])+"\t"+str(il[j][1])+"\t"+str(so[j][0])+"\t"+str(so[j][1])
			outf.write(stri+"\n")
		
	outf.close()
