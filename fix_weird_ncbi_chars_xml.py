import sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python fix_weird_ncbi_chars_xml.py infile.xml outfile.xml"
		sys.exit(0)
	result_handle = open(sys.argv[1],"rU")
	result_handle2 = open(sys.argv[2],"w")
	for k in result_handle:
		result_handle2.write(str(unicode(k,errors="replace").replace(u'\ufffd',"-")))
	result_handle.close()
	result_handle2.close()