#!/usr/bin/env python

import sys
import os
import random
from Bio import AlignIO
import numpy as np

Usage = """
Generates bootstrap replicates in phylip format
given an alignment in fasta format and number of
replicates.

Usage:
  python bootstrap_reps.py fasta_alignment num_reps

"""

if len(sys.argv) < 2:
	print Usage
else:
	msa = AlignIO.read(sys.argv[1], "fasta")
	msa_array = np.array([list(rec) for rec in msa], np.character, order="F")
	num_reps = int(sys.argv[2])
	filename = os.path.basename(sys.argv[1]).split(".")[0]

	taxa = []
	for record in msa:
		taxa.append(record.id)
	maxlen = max(taxa, key=len)

	for k in range(0, num_reps):
		BSk =  np.empty(msa_array.shape, dtype=str, order='F')
		for i in range(0, msa_array.shape[1]):
			ran_num = random.choice(range(0, msa_array.shape[1]))
			BSk[:,i] = msa_array[:,ran_num]
		outfile = os.path.join(os.getcwd(), filename + ".BS" +str(k)+".phy")
		with open(outfile, "w") as f:
			print >>f, " "*2, len(msa), " "*4, msa.get_alignment_length()
			for s, taxon in enumerate(taxa):
				if len(taxon) < len(maxlen):
					print >>f, taxon, " "*(len(maxlen)-len(taxon)), "".join(BSk[s])
				else:
					print >>f, taxon, "", "".join(BSk[s])
