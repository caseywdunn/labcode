from Bio import SeqIO
import sys

"""
used to validate the seq length with the header sequence lenght
"""


if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python validate_seq_lengths.py 454AllContigs.fna"
		sys.exit(0)
	handle = open(sys.argv[1],"rU")
	for i in SeqIO.parse(handle,"fasta"):
		dlen = i.description.split(" ")[2].split("=")[1]
		diff = (int(dlen) - len(i.seq))
		if diff != 0:
			print "error: ",i.id,"descr_line_len:",dlen,"actual_len:",len(i.seq)
	handle.close()
