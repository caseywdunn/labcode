import sys,os,sqlite3
import os.path
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from colors import *

"""
this will take the output from the mcl analysis and write out the fasta
files for each cluster (excluding the genbank names)

this requires the directory structure to be somewhat predictable,
less so than the directory structure for the script that requires
the directory. 

this will use the main mollusk.db for translations and will look in
DIR/species/BLAST.db for the blast information (if required). 

USE THIS ONE!

this also requires identifying the outgroup taxa as only clusters with
at least 2 ingroup taxa will be included and at least 4 total taxa. 
"""

database = "mollusk.db"

outgroups = ["Lingula_anatina","Schmidtea_mediterranea", "Chaetopterus_variopedatus","Terebratalia_transversa","Cerebratulus_lacteus", "Carinoma_mutabilis","Capitella","Helobdella_robusta"]

SMALLESTCLUSTER = 4
SMALLESTINGROUP = 1

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python extract_clusters_from_mcl_out.py mcl_in OUTDIR"
		sys.exit(0)

	mcl_in = sys.argv[1]
	mcl_file = open(mcl_in,"rU")
	clusters = {}
	count = 0
	for i in mcl_file:
		spls = i.strip().split("\t")
		tcl = []
		for j in spls:
			try:
				int(j)
			except:
				tcl.append(j)
		clusters[count]=tcl
		count += 1

	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()

	species_names = []
	species_names_id = {} #key species value id
	cur.execute("SELECT id,name FROM species_names;")
	for i in cur:
		species_names.append(str(i[1]))
		species_names_id[str(i[1])] = str(i[0])

	OUTDIR = sys.argv[2]
	count = 0
	error_file = open("cluster_errors","w")
	for i in clusters:
		if len(clusters[i]) >= SMALLESTCLUSTER:
			print i,len(clusters[i])
			ingroup_samp = []
			cluster_out_seqs = []
			for j in clusters[i]:
				sp_name = j.split("_")[0]+"_"+j.split("_")[1]
				#in case the species is denoted with a single name
				cid_index = 2
				if sp_name not in species_names:
					sp_name = j.split("_")[0]
					cid_index = 1
				found = False
				seq = None
				cid = ""
				cid_spls = j.split("_")
				if len(cid_spls) == cid_index-1:
					cid = cid_spls[cid_index]
				else:
					for n in cid_spls[cid_index:]:
						cid += n+"_"
					cid = cid[:-1]
				#print cid
				cur.execute("SELECT seq FROM translated_seqs WHERE edited_seq_id = ? and species_names_id = ?",(cid,species_names_id[sp_name]))
				a = cur.fetchall()
				if len(a) > 0:
					seq = SeqRecord(Seq(str(a[0][0])))
					seq.id = sp_name+"_"+cid
					seq.description = ""
					seq.name = ""
					cluster_out_seqs.append(seq)
					if sp_name not in outgroups and sp_name not in ingroup_samp:
						ingroup_samp.append(sp_name)
				else:
					seq = None
				if seq == None:
					print "ERROR:no break"
					error_file.write(j+"\n")
					print j
					#sys.exit(0)

			if len(ingroup_samp) >= SMALLESTINGROUP:
				cluster_out_seqfile = open(OUTDIR+"/"+str(count)+".fasta","w")
				SeqIO.write(cluster_out_seqs,cluster_out_seqfile,"fasta")
				cluster_out_seqfile.close()
			count += 1
	error_file.close()
	mcl_file.close()
		
