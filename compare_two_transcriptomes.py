import sys,os
import subprocess
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastnCommandline
from Bio.Blast import NCBIXML

DEBUG = False

def blast2(seq1,dbf):
	in1 = open(seq1.id,"w")
	in1.write(">"+seq1.id+"\n"+seq1.seq.tostring().upper()+"\n")
	in1.close()
	cline = NcbiblastnCommandline(query=seq1.id, db=dbf, evalue=0.00001, num_threads=12,outfmt=5,out=seq1.id+".xml")
	#print cline
	return_code = subprocess.call(str(cline), shell=(sys.platform!="win32"))
	#sys.exit()
	
def ERROR_RATE(length):
	return 0.02 * length

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python compare_two_transcriptomes.py transcript1 transcript2 outfile"
		sys.exit(0)
	#create a blastDB for the infile
	print "making db"
	cmd = "makeblastdb -in "+sys.argv[1]+" -dbtype nucl -out "+ sys.argv[1]+".db"
	os.system(cmd)
	print "finished making db"
	#first file
	dbseqsf = open(sys.argv[1],"rU")
	dbseqs_dict = {}
	for i in SeqIO.parse(dbseqsf,"fasta"):
		dbseqs_dict[i.id] = i
	dbseqsf.close()
	#second file
	infile = open(sys.argv[2],"rU")
	allseqs = []
	allseqs_dict = {}
	for i in SeqIO.parse(infile,"fasta"):
		allseqs.append(i)
		allseqs_dict[i.id] = i
	allseqs.reverse()
	outfile = open(sys.argv[3],"w")
	outfile.write("id\tnumhits\tnumnonoverlapping\tlength_diff\thits\thit_seq_length_left\n")
	while len(allseqs) > 0:
		i = allseqs.pop()
		match = False
		blast2(i,sys.argv[1]+".db")
		result_handle = open(i.id+".xml","rU")
		#print i.id
		outstr = str(i.id)+"\t"
		numhits = 0
		numnonoverlapping = 0
		frontsub = 0
		rearsub = 0
		completesuper = 0
		length_diff = 0
		seqoverlap = [0] * len(i.seq)
		hit = 0
		hits = []
		hits_ldiff = []
		try:
			for k in NCBIXML.parse(result_handle):
				qutitle = str(k.query)
				for m in k.alignments:
					hit += 1
					hsp = m.hsps[0]
					#print hsp
					if (hsp.positives+ERROR_RATE(hsp.align_length)) < hsp.align_length:
						continue
					numhits += 1
					#print hsp.query_start,hsp.query_end
					qst = hsp.query_start-1
					qed = hsp.query_end-1
					if qed < qst:
						t = qed
						qed = qst
						qst = t
					overlap = False
					for p in range(qst,(qed+1)):
						if seqoverlap[p] != 0:
							overlap = True
						seqoverlap[p] += 1
					if overlap == False:
						numnonoverlapping += 1
					hitid = str(m.hit_def)
					hseq = dbseqs_dict[hitid.split(" ")[0]]
					length_diff += len(hseq.seq)
					if "isogroup" in hitid:
						nm = hitid.split(" ")[1].split("=")[1]
						hits.append(nm)
					else:
						hits.append(hitid.split(" ")[0])
					alignl = abs(hsp.sbjct_start-hsp.sbjct_end)
					hits_ldiff.append(len(hseq.seq)-(alignl+1))
			result_handle.close()
			os.remove(i.id)#remove file
			os.remove(i.id+".xml")#remove file
		except:#fix weird nonunicode characters
			result_handle.close()
			result_handle = open(i.id+".xml","rU")
			result_handle2 = open(i.id+".2.xml","w")
			for k in result_handle:
				result_handle2.write(str(unicode(k,errors="replace").replace(u'\ufffd',"-")))
			result_handle.close()
			result_handle2.close()
			result_handle2 = open(i.id+".2.xml","rU")
			for k in NCBIXML.parse(result_handle2):
				qutitle = str(k.query)
				for m in k.alignments:
					hit += 1
					hsp = m.hsps[0]
					#print hsp
					if (hsp.positives+ERROR_RATE(hsp.align_length)) < hsp.align_length:
						continue
					numhits += 1
					#print hsp.query_start,hsp.query_end
					qst = hsp.query_start-1
					qed = hsp.query_end-1
					if qed < qst:
						t = qed
						qed = qst
						qst = t
					overlap = False
					for p in range(qst,(qed+1)):
						if seqoverlap[p] != 0:
							overlap = True
						seqoverlap[p] += 1
					if overlap == False:
						numnonoverlapping += 1
					hitid = str(m.hit_def)
					hseq = dbseqs_dict[hitid.split(" ")[0]]
					length_diff += len(hseq.seq)
					hits.append(hitid.split(" ")[0])
					alignl = abs(hsp.sbjct_start-hsp.sbjct_end)
					hits_ldiff.append(len(hseq.seq)-(alignl+1))
			result_handle2.close()
			os.remove(i.id)#remove file
			os.remove(i.id+".xml")#remove file
			os.remove(i.id+".2.xml")#remove file
		length_diff = (len(i.seq)-length_diff)
		outstr += str(numhits)+"\t"+str(numnonoverlapping)+"\t"+str(length_diff)+"\t"
		for k in hits:
			outstr += str(k)+","
		outstr = outstr[:-1]
		outstr += "\t"
		for k in hits_ldiff:
			outstr += str(k) + ","
		outstr = outstr[:-1]
		outstr += "\n"
		outfile.write(outstr)
		print len(allseqs) 
	outfile.close()
