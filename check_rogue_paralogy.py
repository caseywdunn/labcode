import sys,os,newick3,phylo3
from scipy.stats.mstats import mquantiles
from numpy import *
import numpy

"""
should check to see if there are particular taxa that are
causing the paralogy problems (maybe just simple stats since 
mono masking is already done), are there taxa that are frequent 
are there taxa that are in high percentage
"""

fileending = ".mm"
checkthese = True

totalnumberspecies = 46

"""
assumes the style species@number
"""
def get_list_species_name(node):
    lvs = node.leaves()
    nm = []
    sp_dict = {}
    for i in lvs:
        tnm = i.label.split("@")[0]
        if tnm not in sp_dict:
            sp_dict[tnm] = 0
        sp_dict[tnm] += 1
    return sp_dict

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python check_rogue_paralogy.py DIR outfile"
        sys.exit(0)
    DIR = sys.argv[1]
    total_sp_dict = {}
    total_sp_dictp = {}
    lookup = {}
    for i in os.listdir(DIR):
        filen = DIR+"/"+i
        if filen[-len(fileending):] == fileending:
            infile = open(filen,"r")
            intree = newick3.parse(infile.readline())
            infile.close()
            #print "Leaves:",len(intree.leaves())
            tsp_dict = get_list_species_name(intree)
            totalnum = len(intree.leaves())
            if len(tsp_dict) == 1:
                continue
            for j in tsp_dict:
                if j not in total_sp_dict:
                    total_sp_dict[j] = []
                    total_sp_dictp[j] = []
                total_sp_dict[j].append(float(tsp_dict[j]))
                total_sp_dictp[j].append(tsp_dict[j]/float(totalnum))
            if len(tsp_dict)/float(totalnumberspecies) > 0.5:#len(intree.leaves())
                lookup[DIR+"/"+i] = len(tsp_dict)
    outfile = open(sys.argv[2],"w")
    outfile.write("species\tmean\tmax\tupper_quantile\tmeanp\tmax\n")
    for i in total_sp_dict:
        outfile.write(i+"\t"+str(mean(total_sp_dict[i]))+"\t"+str(max(total_sp_dict[i]))+"\t"+str(mquantiles(total_sp_dict[i])[-1])+"\t"+str(mean(total_sp_dictp[i]))+"\t"+str(max(total_sp_dictp[i]))+"\n")
    outfile.close()

    if checkthese == True:
        for i in lookup:
            fileending = ".pp"
            infile = open(i+fileending,"r")
            maxn = 0
            for j in infile:
                intree = newick3.parse(j)
                llvs = len(intree.leaves())
                if llvs > maxn:
                    maxn = llvs
            if maxn/float(lookup[i]) < 0.5:
                print i,lookup[i],maxn/float(lookup[i])
            infile.close()
