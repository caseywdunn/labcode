#!/usr/bin/env python	

# Written by Casey Dunn, Brown University
# http://dunnlab.org
# Copyright (c) 2012 Brown University. All rights reserved.
# 
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This tool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.


import sys
from collections import defaultdict


Usage = """
Usage:

bowtie_map_to_counts.py bowtie.sam > bowtie.counts 

"""

if len(sys.argv) < 2:
	print Usage
else:
	map_name = sys.argv[1]
	
	
	map = defaultdict(set) #A dictionary with key read and value set of genes that the read maps to
	map_handle = open(map_name, "rU")
	
	# Loop over the lines, and build a map of all the references that each read hits
	n = 0
	for line in map_handle:
		if line[0] == '@' :
			continue
		n = n + 1
		line = line.strip()
		fields = line.split('\t')
		if len(fields)<3:
			raise ValueError("Read number {0} is poorly formed:\n  {1}".format(n, line))
		read = fields[0]
		ref = fields[2]
		map[read].add(ref)
		#if n % 100000 == 0:
			# print n, "lines read"
	
	# Go through the read map, and convert this to counts for each reference sequence
	
	counts = defaultdict(int)
	
	for key, hits in map.iteritems():
		# Check to see if the read mapped uniquely, if not skip it
		if len(hits) > 1:
			continue
		
		ref = list(hits)[0]
		counts[ref] = counts[ref] + 1
		
	# Print out the results
	print "reference\tcount"
	for ref, count in counts.iteritems():
		print ref + "\t" + str(count)
