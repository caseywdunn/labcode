
import sys,os,sqlite3

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python convert_sqlite_to_mysql_seqs.py infile.db outfile"
        sys.exit(0)
    dbname = sys.argv[1]
    if os.path.exists(dbname) == False:
        print "the database doesn't exist"
        sys.exit(0)
    con = sqlite3.connect(dbname)
    species = []
    cur = con.cursor()
    outfile = open(sys.argv[2],"w")
    cur.execute("SELECT * FROM translated_seqs;")
    b = cur.fetchall()
    for j in b:
        outfile.write("INSERT INTO sequences (taxa_id,sequence_name,translated_seq,masked_seq) values ("+str(j[1])+",'"+str(j[3])+"','"+str(j[4])+"','"+str(j[6])+"');\n")
    outfile.close()
    con.close()
