import sys
from Bio import SeqIO

"""
this assumes fastq from the short read archive
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python convert_fastq_to_demo_qseq.py infile.fastq outfile.qseq"
		sys.exit()
	infile = open(sys.argv[1],"r")
	outfile = open(sys.argv[2],"w")
	for i in SeqIO.parse(infile,"fastq"):
		string = "EAS1745	19	1	1	1879	1007	0	1	"
		string+= str(i.seq)
		string+="\t"
		string+=str('h'*len(i.seq))
		string+="\t1"
		outfile.write(string+"\n")
	infile.close()
	outfile.close()