from Bio import SeqIO
from numpy import median
import sys

"""
trim the length off the front
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python trim_illumina_fastq_front.py infile length outfile"
		sys.exit(0)
	count = 0

	infile = open(sys.argv[1],"r")
	length = int(sys.argv[2])
	outfile = open(sys.argv[3],"w")
	outfile.close()
	outfile = open(sys.argv[3],"a")
	seqs = []
	for i in SeqIO.parse(infile,"fastq-illumina"):
		seqs = [i[length:]]
		SeqIO.write(seqs,outfile,"fastq-illumina")
		count += 1
		if count % 10000 == 0:
			print count	
	infile.close()
	outfile.close()
