import sys,os,math
import subprocess
from Bio import SeqIO
from Bio.Blast import NCBIXML
import align

"""
this is for the creation of the mcl input when using the mpiblast results that results
in a single file for all the blast results. 


"""

"""
string 1 = a
string 2 = b
"""
def test_similarity(str1,str2):
	a = map(None, str(str1))
	b = map(None, str(str2))
	#print a,b
	A = align.Alignment()
	c, x, y, s = A.align(a, b)
	#print c
	return float(c)

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python examine_trans_splicing_after_blast.py infile.xml outfile"
		sys.exit(0)
	#open infile
	keepstarts = []
	infile = open(sys.argv[1],"rU")
	outfile = open(sys.argv[2],"w")
	count = 0
	count2 = 0
	for k in NCBIXML.parse(infile):
		qutitle = str(k.query)
		print count,qutitle,len(keepstarts)
		for m in k.alignments:
			count += 1
			if int(m.hsps[0].align_length) > 6 and int(m.hsps[0].query_start < 2):
				count2 += 1
				##print m.hsps[0]
				##print m.hsps[0].query
				##print m.hsps[0].sbjct
				ts = str(m.hsps[0].query).replace("-","")[0:50]
				similar = False
				for j in keepstarts:
					try:
						c = test_similarity(ts,j)
						if c <= (float(len(ts)) * (1./6.)):
							similar = True
							keepstarts[j] += 1
							break
					except:
						#print "error"
						continue
				if similar == False:
					keepstarts.append([ts])
				break#this should keep from multiple hits
	print count,count2
	for i in keepstarts:
		print i,keepstarts[i]
	outfile.close()

