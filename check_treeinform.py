#!/usr/bin/env python

import sys, re
from collections import defaultdict

def main():
    # Written by Casey Dunn, http://dunnlab.org

	if len(sys.argv) < 3:
		print """

	Annotates a file of phylogenies with taxa proposed for fusion so that they 
	can be checked. 


	Usage:
	python check_treeinform.py pre-fusion-all.tre candidates.tsv

		"""

	else:
		tree_name = sys.argv[1]
		candidates_name = sys.argv[2]

		

		candidate_file = open(candidates_name, 'ru')

		# Create a dictionary of lists, with key reassignment name and list of sequence ids
		reassignments = defaultdict(list)
		for line in candidate_file:
			line = line.strip()
			elements = line.split('\t')
			id = elements[1]
			species = elements[2].replace(' ', '_')
			id = species + "@" + id
			reassign = "REASSIGN." + elements[0]
			reassignments[reassign].append(id)

		# Create a dictionary with key sequence id and value string that describes fusion
		candidates = dict()
		for reassign in reassignments.keys():
			tag = "_FUSE_" + reassign + "_" + "+".join(reassignments[reassign])
			for id in reassignments[reassign]:
				candidates[id] = tag
		
		print len(candidates.keys())


		# Annotate the fusion candidate tips in the tree
		trees = open(tree_name, 'ru')

		for tree in trees:
			tree = tree.strip()
			for candidate in candidates.keys():
				tag = candidates[candidate]
				tree = re.sub(candidate + ":", candidate + tag + ":", tree)
			print(tree)


if __name__ == "__main__":
	main()