import sys
from Bio import SeqIO

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python break_up_fasta.py infile.fna numberoffinalfiles"
		sys.exit(0)
	infile = open(sys.argv[1],"rU")
	seqs = list(SeqIO.parse(infile,"fasta"))
	infile.close()
	total = len(seqs)
	breakinto = int(sys.argv[2])
	start = 0
	for i in range(breakinto):
		outfile = open(sys.argv[1]+"."+str(i),"w")
		if i+1 == breakinto:
			SeqIO.write(seqs[start:],outfile,"fasta")
		else:
			SeqIO.write(seqs[start:start+int(total/breakinto)],outfile,"fasta")
			start = start+int(total/breakinto)
		outfile.close()
			
