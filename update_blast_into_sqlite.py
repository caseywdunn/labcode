import sys,sqlite3,os
from Bio import SeqIO
from Bio.Blast import NCBIXML
from colors import *


#for checking on parasites
#Trematoda Taxonomy ID:6178
#Microsporidia Taxonomy ID: 6029
PARASITES = [6178,6029]
pdb = "/home/smitty/Desktop/pln.178.db"


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python update_trans_seqs_into_sqlite.py species_name file database"
		sys.exit(0)
	database = sys.argv[3]
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
		sys.exit(0)
	con = sqlite3.connect(database)

	#BEGIN PARASITES
	tax_con = sqlite3.connect(pdb)
	#each position corresponds to a taxon
	lefts = []
	rights = []
	
	for i in PARASITES:
		c = tax_con.cursor()
		c.execute("select left_value,right_value from taxonomy where ncbi_id=?", (i,))
		c.close()
		fl = None
		fr = None
		for j in c:
			fl = int(j[0])
			fr = int(j[1])
		lefts.append(fl)
		rights.append(fr)
	#END PARASITES
	
	
	species_name = sys.argv[1]
	cur = con.cursor()
	cur.execute("SELECT id FROM species_names where name = ?;",(species_name,))
	a = cur.fetchall()
	if len(a) == 0:
		print RED,"no species",species_name,"in database",RESET
		con.close()
		sys.exit(0)
	else:
		db_taxon_id = a[0][0]
		#check to see if the file is already in the databases here
		filen = os.path.join(os.path.abspath("."),sys.argv[2])
		cur.execute("SELECT id FROM blast_results where file = ?;",(filen,))
		b = cur.fetchall()
		if len(b) > 0:
			print RED+sys.argv[2]+" file already entered"+RESET
			sys.exit(0)
		#end check
		print BLUE+"found",species_name+RESET,"id"+GREEN,db_taxon_id,RESET
		handle = open(sys.argv[2],"rU")
		blaststoenter = []
		#blasts = list(NCBIXML.parse(handle))
		print "done reading blasts"
		for i in NCBIXML.parse(handle):
			if len(i.alignments) > 0:
				print len(i.alignments)
			count = 1
			qu_full = str(i.query)
			qu_ed = str(i.query.split(" ")[0])
			if qu_ed[:3] == "jgi":
					qu_ed = qu_ed.split("|")[2]
			if len(qu_ed) == 8:
				try:
					int(qu_ed[3:])
					qu_ed = qu_ed[3:]
				except:
					qu_ed = qu_ed
			bad = False
			#each hit for each seq
			first = True
			for m in i.alignments:
				evalue = m.hsps[0].expect
				#try:
				#	evalue = -math.log10(m.hsps[0].expect) #get the first hit
				#except:
				#	evalue = 180 #largest -log10(evalue) seen
				hit_id = str(m.hit_id)
				hit_def = str(m.hit_def)
				hit_acc = ""
				try:
					if first == True:
						hit_name = m.hit_def.split("[")[1].split("]")[0] 
						c = tax_con.cursor()
						c.execute("select left_value,right_value,ncbi_id from taxonomy where name=?", (str(hit_name),))
						c.close()
						fl = None
						fr = None
						fncbi_id = None
						for n in c:
							fl = int(n[0])
							fr = int(n[1])
							fncbi_id = int(n[2])
						for n in range(len(lefts)):
							if fl > lefts[n] and fr < rights[n]:
								bad = True
						first = False
					hit_acc = str(m.hit_id.split("|")[1])
				except: #just no name hit for parasite
					hit_acc = str(m.hit_id.split("|")[1]) #verify this is the accession
				if bad == False:
					blaststoenter.append([db_taxon_id, qu_full, qu_ed, count, hit_id, hit_def,hit_acc,str(evalue),filen])
				count +=1
		handle.close()
		cur=con.cursor()
		for i in blaststoenter:
				cur.execute("INSERT INTO blast_results(species_names_id,query_full_id,query_edited_id,hit_num,hit_id,hit_def,hit_accession,evalue,file) values(?,?,?,?,?,?,?,?,?)",(i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7],i[8]))
		con.commit()
	con.close()
