import sys,os

"""
this takes a file with sites (starting from 0) to remove , one on each line and a raxml phylip file

it outputs the raxml file with sites removed
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python remove_undetermined_raxml_sites.py infile.und infile.phy"
		sys.exit(0)
	inund = open(sys.argv[1],"r")
	und = []
	for i in inund:
		try:
			und.append(int(i.strip()))
		except:
			continue
	infile = open(sys.argv[2],"r")
	first = True
	outfile = open(sys.argv[2]+".cleaned","w")
	for i in infile:
		if first == True or len(i) < 2:
			outfile.write(i)
			first = False
			continue
		spls = i.split("\t")
		seqstr = ""
		count = 0
		for j in spls[1]:
			if count not in und:
				seqstr += j
			count += 1
		print len(seqstr)
		outfile.write(spls[0])
		outfile.write("\t")
		outfile.write(seqstr)
		outfile.write("\n")
	infile.close()
	outfile.close()
	
