import sys,os

if __name__ == '__main__':
	if len(sys.argv) != 3:
		print "python mcl_dnsvth_dth_calc.py mcl_in output"
		sys.exit(0)
	mcl_in = open(sys.argv[1],"r")
	thresholds = range(0,180)
	nn = [0] * 180
	se = [0] * 180
	nsv = [0] * 180
	cur = None
	highest = None
	print "processing mcl file"
	for i in mcl_in:
		spls = i.strip().split("\t")
		num = float(spls[2])
		for j in range(0,int(num)):
			se[j] += 0.5
		if cur == None:
			cur = spls[1]
			highest = num
			continue
		if cur != spls[1]:
			for j in range(0,int(highest)):
				nn[j] += 1
			cur = spls[1]
			highest = num
		else:
			if num > highest:
				highest = num
	#finish the last one
	for j in range(0,int(highest)):
		nn[j] += 1
	mcl_in.close()
	print "finished with mcl"
	print "writing the file"
	output = open(sys.argv[2],"w")
	for i in range(len(nsv)):
		nsv[i] = float(se[i])/nn[i]
		output.write(str(thresholds[i])+"\t"+str(nn[i])+"\t"+str(se[i])+"\t"+str(nsv[i]))
		output.write("\n")
	output.close()