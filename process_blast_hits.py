#!/usr/bin/python
import sys,os
from Bio.Blast import NCBIXML
#seems to be no different but better to use the one with the better name?
#result_handle = open("Naut_I.blast")
#blast_records = list(NCBIXML.parse(result_handle))
#result_handle.close()

#Lingo:
#query - a sequence that is being checked against the blast db
#subject - a sequence that was hit by the query

def usage():
	print "usage: python process_blast.py blast_xml_file outfile"

def average(values):
	return sum(values,0.0) / len(values)

if __name__ == "__main__":
	if len(sys.argv) != 3:
		usage()
		sys.exit()
	
	result_handle = open(sys.argv[1],"rU")

	hits_out = open(sys.argv[2],"w")
	
	hits_out.write("id\tnum_hits\tbest_length\tavg_length\tbest_frac\tavg_frac\n")
	
	nohit_count = 0
	hit_count = 0
	nrecs = 1
	#for i in blast_records: # Loop through the records, which have one query per record
	for i in NCBIXML.parse(result_handle):
		print nrecs
		if len(i.alignments) > 0:
			alignment_lens = [] # Each element is the total alignment length (summed across hsps) for each subject
			alignment_fracs = [] # Each element is the total sequence length for each subject (regardless of the size of the alignment)
			for j in i.alignments:
				full = 0
				subject_length = float(j.length) # Length of the subject, presumed to be the full length of the gene
				title = j.title # Name of the subject
				#remove doubles
				keep = []
				for k in j.hsps:
					equal = False
					minl = min(k.sbjct_start,k.sbjct_end)
					maxl = max(k.sbjct_start,k.sbjct_end)
					for l in keep:		
						tminl = min(l.sbjct_start,l.sbjct_end)
						tmaxl = max(l.sbjct_start,l.sbjct_end)
						if minl == tminl and maxl == tmaxl:
							equal = True
					if equal == False:
						keep.append(k)
				#use the list with doubles removed
				for k in keep:
					minl = min(k.sbjct_start,k.sbjct_end)
					maxl = max(k.sbjct_start,k.sbjct_end)
					firstequal = False
					for l in keep:
						if k != l:
							tminl = min(l.sbjct_start,l.sbjct_end)
							tmaxl = max(l.sbjct_start,l.sbjct_end)
							if tmaxl > maxl and tminl < minl:
								minl = 0
								maxl = 0
								break
							else:
								if tmaxl > minl and tminl < minl:
									minl = l.sbjct_end
								if tminl < maxl and tmaxl > maxl:
									maxl = l.sbjct_start
					#full += (k.sbjct_end - k.sbjct_start)
					full += (maxl - minl)
				alignment_lens.append(full)
				alignment_fracs.append(full/subject_length)
				if full < 0 or full/subject_length > 1:
					print "ERROR IN LENGTHS!!!"
					if full < 0 :
						print "full < 0:","full = ",full
					else:
						print "full/subject_length > 1",full,subject_length
					for k in j.hsps:
						print k.sbjct_start,k.sbjct_end,full,subject_length,len(keep)
					sys.exit(0)
			hits_out.write(str(hit_count+nohit_count)+"\t"+str(len(alignment_lens))+"\t"+str(max(alignment_lens))+"\t"+str(average(alignment_lens))+"\t"+str(max(alignment_fracs))+"\t"+str(average(alignment_fracs))+"\n")
			hit_count += 1
		else:
			#hits_out.write(str(hit_count+nohit_count)+"\t0\t0\t0\n")
			nohit_count += 1
		nrecs += 1
		if hit_count+nohit_count == 314:
			sys.exit(0)
	result_handle.close()
	hits_out.close()
			
	print "no hit: " + str(nohit_count)
	print "hits: " + str(hit_count)

	r_out = open(sys.argv[2]+".plot.r","w")
	r_out.write("a = read.table('"+sys.argv[2]+"',header=T)\n")
	r_out.write("par(mfrow=c(3,2)) \n")
	r_out.write("hist(a[,2],xlab='hits per record',main='Histogram of hits per record')\n")
	r_out.write("plot(a[,3],a[,4],xlim=c(0,max(a[,3])),ylim=c(0,max(a[,3])),xlab = 'best length',ylab='average length',main='Average hit length vs best hit length')\n")
	r_out.write("hist(a[,3],xlab='best length per record',main='Histogram of best length')\n")
	r_out.write("hist(a[,3],xlab='best length per record',xlim=c(0,mean(a[,3])),1000,main='Histogram of best length - zoomed')\n")
	r_out.write("hist(a[,4],xlab='best fraction per record',main='Histogram of best fraction')\n")
	r_out.write("hist(a[,5],xlab='average fraction per record',main='Histogram of average fraction')\n")
	r_out.close()
	os.system("R --file="+sys.argv[2]+".plot.r")
