#!/usr/bin/env python

import sys
import os
from Bio import AlignIO

Usage = """
Converts FASTA alignment into phylip-sequential
without restriction in length of taxa names.

Usage:
  python fasta2phylip_sequential.py fasta_alignment

"""

if len(sys.argv) < 1:
        print Usage
else:
        msa = AlignIO.read(sys.argv[1], "fasta")
        filename = os.path.basename(sys.argv[1]).split(".")[0]

        taxa = []
        for record in msa:
                taxa.append(record.id)
        maxlen = max(taxa, key=len)

        outfile = os.path.join(os.getcwd(), filename + ".phy")
        with open(outfile, "w") as f:
		print >>f, " "*2, len(msa), " "*4, msa.get_alignment_length()
                for record in msa:
			if len(record.id) < len(maxlen):
				print >>f, record.id, " "*(len(maxlen)-len(record.id)), record.seq
                        else:
                                print >>f, record.id, "", record.seq
