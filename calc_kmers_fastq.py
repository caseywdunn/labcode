from Bio import SeqIO
import sys
import operator

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "python calc_kmers_fastq.py inseq (k)"
		sys.exit(0)
	kmers = {}
	seqlen = 0
	limit = 1000000
	dontprint = int(0.00001*limit) #smallest kmer count to print
	print dontprint
	first = True
	infile = open(sys.argv[1],"r")
	nseqs = 0
	for i in SeqIO.parse(infile,"fastq-illumina"):
		if first ==True:
			first = False
			seqlen = len(i.seq)
			if len(sys.argv) == 2:
				k = seqlen
			else:
				k = int(sys.argv[2])
		start = 0
		end = start + k
		while end <= len(i.seq):
			kmer = i.seq.tostring()[start:end]
			if kmer not in kmers:
				kmers[kmer] = 0
			kmers[kmer] += 1
			start = end
			end = start + k
		nseqs += 1
		if nseqs == limit:
			break
	
	sorted_kmers = sorted(kmers.items(), key=operator.itemgetter(1),reverse=True)
	for j in sorted_kmers:
		if j[1] > dontprint:
			print j,j[1]/float(nseqs)
		
