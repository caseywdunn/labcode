import networkx as nx
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import argparse
import itertools
import edge_coloring as ec

def mcl_graph(mcl_file):
	"""
	converts an mcl file into a networkx graph
	"""
	G = nx.Graph()
	with open(mcl_file, 'r') as fin:
		for line in fin:
			nodes = line.split()
			edges = itertools.combinations(nodes,2)
			G.add_nodes_from(nodes)
			G.add_edges_from(edges)
	return G

def assign_colors(G1, G2):
	"""
	Assigns colors to nodes in G2 based on the connected components of G1.
	"""

	node_colors = {}
	components = nx.connected_components(G1)
	i = 0
	for component in components:
		for node in component:
			node_colors[node] = i
		i = i + 1
	nl = node_colors.keys()
	nc = node_colors.values()
	missing_nodes = list(set(G2.nodes()).difference(set(nl)))            
	return nl, nc, missing_nodes

if __name__ == '__main__':
    
    desc = "plots 2 graphs next to each other with nodes in 2nd graph colored based on the connected components in the 1st graph; can accept mcl or blast tabular input"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument('-t1', '--type1', default='blast',type=str,help='1st file type (mcl or blast)')
    parser.add_argument('-t2', '--type2', default='mcl',type=str,help='2nd file type (mcl or blast)')
    parser.add_argument('-i1', '--input1', help='1st file input name')
    parser.add_argument('-i2', '--input2', help='2nd file input name')
    parser.add_argument('-o', '--out',help='out file')
    
    opts = parser.parse_args()
    out = opts.out
    t1 = opts.type1
    t2 = opts.type2
    i1 = opts.input1
    i2 = opts.input2

    G1 = nx.Graph()
    if t1 == 'mcl':
        G1 = mcl_graph(i1)
    elif t1 == 'blast':
        G1, blast_edges = ec.blast_graph(i1)
    else:
        raise NameError("Options are mcl or blast for t1")

    G2 = nx.Graph()
    if t2 == "mcl":
        G2 = mcl_graph(i2)
    elif t2 == 'blast':
        G2, blast_edges = ec.blast_graph(i2)
    else:
        raise NameError("Options are mcl or blast for t2")

    nl,nc,missing_nodes = assign_colors(G1, G2)
    plt.subplot(121)
    plt.title("Graph 1")
    nx.draw(G1,pos=nx.spring_layout(G1, scale=2),nodelist=nl,node_color=nc,cmap=plt.cm.Blues,with_labels=False)
    plt.subplot(122)
    plt.title("Graph 2")
    pos = nx.spring_layout(G2,scale=2)
    nx.draw_networkx_nodes(G2, pos, nodelist=nl,node_color=nc,cmap=plt.cm.Blues,with_labels=False)
    nx.draw_networkx_nodes(G2, pos, nodelist=missing_nodes,with_labels=False)
    nx.draw_networkx_edges(G2,pos)
    plt.axis("off")
    plt.savefig(out)
    plt.clf()
