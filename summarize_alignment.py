#!/usr/bin/env python	

from Bio import AlignIO
import sys


Usage = """
Usage:

%s alignment*.fa

For each alignment, print out summary stats on taxon sampling

""" % sys.argv[0]


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print Usage
		sys.exit(0)

	print( "file\ttips\ttaxa\tmean" )

	for file_name in sys.argv[1:]:
		alignment = AlignIO.read(open(file_name, 'ru'), "fasta")
		tips = []
		for record in alignment :
			tips.append(record.id.split('@')[0])
		print( '{}\t{}\t{}\t{:.2f}'.format( file_name, len(tips), len(set(tips)), len(tips)/float(len(set(tips))) )  )
