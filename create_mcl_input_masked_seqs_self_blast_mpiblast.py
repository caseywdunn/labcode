import sys,os,math
import subprocess
from Bio import SeqIO
from Bio.Blast import NCBIXML

"""
this is for the creation of the mcl input when using the mpiblast results that results
in a single file for all the blast results. 

"""


if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python create_mcl_input_masked_seqs_self_blast_mpiblast.py infile.xml outfile"
		sys.exit(0)
	#open infile
	infile = open(sys.argv[1],"rU")
	outfile = open(sys.argv[2],"w")
	for k in NCBIXML.parse(infile):
		qutitle = str(k.query)
		for m in k.alignments:
			print qutitle,len(k.alignments)
			evalue = 0
			try:
				evalue = -math.log10(m.hsps[0].expect)
			except:
				evalue = 180 #largest -log10(evalue) seen
			hitid = str(m.hit_def)#str(m.hit_id).split("|")[1]
			if hitid == qutitle: #don't want hits to self
				continue
			outfile.write(hitid.split(" ")[0]+"\t"+qutitle.split(" ")[0]+"\t"+str(evalue)+"\n")
	outfile.close()
"""
		except:#fix weird nonunicode characters
			result_handle.close()
			result_handle = open(i.id+".xml","rU")
			result_handle2 = open(i.id+".2.xml","w")
			for k in result_handle:
				result_handle2.write(str(unicode(k,errors="replace").replace(u'\ufffd',"-")))
			result_handle.close()
			result_handle2.close()
			result_handle2 = open(i.id+".2.xml","rU")
			for k in NCBIXML.parse(result_handle2):
				qutitle = str(k.query)
				print qutitle
				for m in k.alignments:
					evalue = 0
					try:
						evalue = -math.log10(m.hsps[0].expect) 
					except:
						evalue = 180 #largest -log10(evalue) seen
					hitid = str(m.hit_def)
					if hitid == qutitle: #don't want hits to self
						continue
					outfile.write(hitid+"\t"+qutitle+"\t"+str(evalue)+"\n")
			result_handle2.close()
			os.remove(i.id)#remove file
			os.remove(i.id+".xml")#remove file
			os.remove(i.id+".2.xml")#remove file
"""

