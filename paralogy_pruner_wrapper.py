import sys,os

"""
this is wrapping the pruner and the seq extract
"""

treeending = ".mm"
seqending = ".fasta"

ppdir = "/home/smitty/Dropbox/programming/labcode/"

pp_cmd = "python "+ppdir+"paralogy_pruner.py "

pps_cmd = "python "+ppdir+"paralogy_pruner_seq.py "

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python paralogy_pruner_wrapper.py DIR db"
        sys.exit(0)
    DIR = sys.argv[1]
    dbname = sys.argv[2]
    for i in os.listdir(DIR):
        filen = DIR+"/"+i
        if filen[-len(treeending):] == treeending:
            cmd = pp_cmd+filen+" "+filen+".pp"
            print cmd
            os.system(cmd)
            cmd = pps_cmd+filen+".pp "+dbname
            print cmd
            os.system(cmd)
