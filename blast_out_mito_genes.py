import os,sys
from Bio import SeqIO
from Bio.Blast import NCBIXML

"""
this takes a seed mito file and will list the genes that blast to the file

make sure that the line around 27 or so that defines the hitid and the line around
39 or so that gets the sequence ids are defined correct. these change depending on the
technology
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python blast_out_mito_genes.py mitoseqs.fa GENEDIR outfile.list"
		sys.exit(0)
	mitoseqs = sys.argv[1]
	indir = sys.argv[2]
	#run the blast real quick
	cmd = "makeblastdb -in "+mitoseqs+" -dbtype prot -out "+mitoseqs+".db"
	os.system(cmd)
	outfile = open(sys.argv[3],"w")
	for f in os.listdir(indir):
		cmd = "blastp -db "+mitoseqs+".db -outfmt 5 -num_threads 8 -out "+indir+"/"+f+".temp.blast.xml -evalue 0.000001 -query "+indir+"/"+f
		print "running blast"
		os.system(cmd)
		hit = False
		try:
			blin = open(indir+"/"+f+".temp.blast.xml","r")
			for i in NCBIXML.parse(blin):
				for j in i.alignments:
					hitid = str(j.hit_def).split()[0]#this is for 454
					print hitid
					hit = True
					break
				if hit == True:
					break
			blin.close()
			if hit == True:
				outfile.write(f+"\n")
			print "removing temporary files"
			os.system("rm "+indir+"/"+f+".temp.blast.xml")
		except:
			continue
	outfile.close()
