useradd -p `mkpasswd -l 32` dunnlab
mkdir -p /data/lab
chown dunnlab:dunnlab /data/lab
semanage fcontext -a -t samba_share_t "/data/lab(/.*)?"
restorecon -R -v /data/lab/
echo "Create samba password for user 'dunnlab':"
smbpasswd -a dunnlab
SMBCONF=/etc/samba/smb.conf
mv $SMBCONF $SMBCONF.orig
echo "
[global]
	workgroup = DUNNLAB
	server string = Samba Server Version %v
	log file = /var/log/samba/log.%m
	max log size = 50
	security = user
	passdb backend = tdbsam
	load printers = no
[lab]
	comment = Dunn Lab File Share
	path = /data/lab
	browseable = yes
	writable = yes
	guest ok = no
	valid users = dunnlab
" >$SMBCONF
service smb restart
