# List devices for all hard drives here.
DEVICES="/dev/sda /dev/sdb /dev/sdc /dev/sdd /dev/sde"
# Partition each drive.
for d in $DEVICES
do
	parted $d mklabel gpt
	parted $d -a optimal mkpart 1 3t
	pvcreate $d
done
# Merge drives into a virtual group.
vgcreate $DEVICES
# Define a logical volume on the virtual group.
lvcreate -l 100%FREE -n DataVolume Data
# Format the logical volume with a limited number of inodes
# (10 million) that is less than the inode quota on /gpfs/data.
mkfs.ext4 -m0 -N 10000000 /dev/Data/DataVolume
# Automount.
mkdir /data
echo "/dev/Data/DataVolume                      /data                   ext4    noatime         0 0" >>/etc/fstab
mount -a
mkdir -p /gpfs/data
ln -s /data/oscar /gpfs/data/cdunn
