import sys,os,sqlite3
from Bio import SeqIO
from Bio.Blast import NCBIXML

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python extract_species_from_blast_for_inparanoid.py database blast.xml"
        sys.exit(0)
    database = sys.argv[1]
    if os.path.exists(database) == False:
        print "the database has not been created"
    con = sqlite3.connect(database)
    cur = con.cursor()
    #get the species ids and names
    includespecies_id = {}#key is id, value is name
    cur.execute("SELECT id,name FROM species_names;")
    a = cur.fetchall()
    for i in a:
        includespecies_id[i[0]] = i[1]
    #make a dictionary of blast files
    blast_files = {} #key is the from to, so there will be A-A, A-B, A-B but not B-A, they all go in the A-B
    #process the blast file
    blast = open(sys.argv[2],"r")
    count = 1
    for i in NCBIXML.parse(blast):
        qutitle = str(i.query)
        qu = "SELECT species_names_id FROM translated_seqs where id = "+qutitle
        cur.execute(qu)
        a = cur.fetchall()
        fromsp = includespecies_id[a[0][0]]
        for k in i.alignments:
            altitle = k.hit_id.split("|")[1]
            qu2 = "SELECT species_names_id FROM translated_seqs where id = "+altitle
            cur.execute(qu2)
            a = cur.fetchall()
            tosp = includespecies_id[a[0][0]]
            print qutitle,altitle,fromsp,tosp
        if count == 2:
            break
        count += 1
    blast.close()
