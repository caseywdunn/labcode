from Bio import SeqIO
import sys

if __name__ == "__main__":
	print('file\tseq\tunambig_length\tfull_length')
	for filename in sys.argv[1:]:
		for seq_record in SeqIO.parse(filename, "fasta"):
			seq = str( seq_record.seq )
			unambiguous = seq.replace('-','')
			print ( '\t'.join([filename, seq_record.id, str(len( unambiguous )), str(len( seq ))]) )
		