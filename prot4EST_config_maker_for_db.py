import sys,os

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python prot4EST_config_maker.py inputfasta outputdir species_name blastfile"
		print "\t underscores in species_names are important"
		sys.exit(0)
	inputfile = sys.argv[1]
	output = sys.argv[2]
	species = sys.argv[3].replace("_"," ")
	blastfile = sys.argv[4]
	#config template
	print "	Config file for PROT4EST created Wed Nov  9 14:36:29 HST 2005"
	print ""
	print ""
	print "For help on any of these please consult the README file"
	print ""
	print "#Full path to fasta input file, e.g. /home/joe/EST/rubellus.fsa"
	print "1.   Input File [fasta format]:"+inputfile
	print ""
	print "#prot4EST will create this directory, e.g. 'output' will be created in the"
	print "#directory p4e is launched from"
	print "2.   Output Directory:"+os.path.abspath(".")+"/"+output
	print ""
	print "#e.g. Lumbricus rubellus"
	print "3.   Organism Name (full):"+species
	print ""
	print "4.   Location of genetic code file: /home/smitty/apps/mpiblast-1.6.0/ncbi/data/gc.prt" #could change this but set for now
	print ""
	print "#Fasta and BLAST files containing these sequences are included in the prot4EST release."
	print "#Enter the full path."
	print "5.   Ribosomal RNA BLAST database:/home/smitty/apps/prot4EST_2.3/rRNAeuk.bldb/rRNAeukAll_nuc.fsa" #specific for me
	print "6.   Mitochondria BLAST database [protein]:/home/smitty/apps/prot4EST_2.3/mitoMetazoan90.bldb/mitoMetazoan90_pro.fsa" #specific for me
	print ""
	print "#The defaults are shown"
	print "7.   Evalue for rRNA search (BLASTN): 1e-65"
	print "8.   Evalue for BLASTX: 1e-8"
	print ""
	print "#If you have previous carried out BLASTx search on these sequences then enter the path to the report file"
	print "#or directory containing only these files"
	print "#If left blank then prot4EST assumes you wish to carry out a BLASTx search on these sequences"
	print "#You are advised to read the userguide regarding this option"
	print "9.    Location of pre-computed BLASTX report files/directory:"+blastfile
	print ""
	print "#Fill in all entries for 9a-c OR just 9d (if DECODER has already been run on these sequences)"
	print "#e.g. /home/joe/partigene/protein"
	print "10a.  Path to sequence and quality files [protein directory]:"+os.path.abspath(".")+"/TEMP"
	print "#defaults shown"
	print "10b.  Suffix for EST sequence files: seq"
	print "10c.  Suffix for EST quailty files: qlt"
	print "	 or"
	print "10d.  Path to pre-computed DECODER results:"
	print ""
	print "11. ESTScan Matrix File [optional]:"
	print ""
	print "12. Codon Usage Table (gcg format) [optional]:"+os.path.abspath(".")+"/"+"human_high.cod"
	print ""
	print ""
	print "For help on any of these please consult the user guide"
