import sys,os,sqlite3
from Bio.Blast import NCBIXML
from Bio import SeqIO
from colors import *
import math

DB = "/media/data/pln.db"

#Trematoda Taxonomy ID:6178
#Microsporidia Taxonomy ID: 6029
#euk INCLUDETAXA = [2759]
#INCLUDETAXA = [33208]
INCLUDETAXA = [33090]#viridiplantae 

"""
this will make a file with only gi's that are a particular taxa
"""

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python get_gi_list_from_nr.py outfile"
		sys.exit(0)
	
	print "building gi list from nr"
	cmd = 'blastdbcmd -db /media/data/lib/blast_data/nr -entry all -outfmt "%g %T" > all_nr.gilist'
#	os.system(cmd)

	#this is for reading in the parasite information
	conn = sqlite3.connect(DB)
	
	#each position corresponds to a taxon
	lefts = []
	rights = []
	
	print "getting left right"
	
	for i in INCLUDETAXA:
		c = conn.cursor()
		c.execute("select left_value,right_value from taxonomy where ncbi_id=?", (i,))
		fl = None
		fr = None
		for j in c:
			fl = int(j[0])
			fr = int(j[1])
		lefts.append(fl)
		rights.append(fr)
		c.close()

	print "processing file"
	ingi = open("all_nr.gilist","r")
	out = open(sys.argv[1],"w")
	for i in ingi:
		spls = i.strip().split()
		tid = spls[1]
		gid = spls[0]
		spls
		c = conn.cursor()
		c.execute("select left_value,right_value from taxonomy where ncbi_id=?", (str(tid),))
		fl = None
		fr = None
		for n in c:
			fl = int(n[0])
			fr = int(n[1])
		c.close()
		for n in range(len(lefts)):
			if fl >= lefts[n] and fr <= rights[n]:
				out.write(gid+"\n")
				break
	out.close()
		
