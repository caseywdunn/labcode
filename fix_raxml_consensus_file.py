import os,sys

if __name__ == '__main__':
	if len(sys.argv) != 3:
		print "python fix_raxml_consensus_file.py infile outfile"
		sys.exit(0)
	strg = ""
	inf = open(sys.argv[1],"r")
	strg = inf.readline().strip()
	strg = strg.replace(":1.0[","").replace("]","")
	inf.close()
	
	ouf = open(sys.argv[2],"w")
	ouf.write(strg+"\n")
	ouf.close()