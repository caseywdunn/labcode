import sys
from Bio import SeqIO

"""
This should fix the solid results.tab file from problems with the 454AllContigs.fna clipping. This will print out a new
results.tab file with the incorrectly tagged parts of the 454AllContigs excluded.

This also assumes that the cap3 contigs keep their capital C in the contig name. They don't need to be filtered
and this is the identifying characteristic.
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python exclude_subset_contigs_clip.py 454AllContigs.fna results.tab outfile"
		sys.exit(0)
	infile = open(sys.argv[1],"rU")
	contig_clips = {}
	for i in SeqIO.parse(infile,"fasta"):
		spls = i.description.split(" ")
		length = int(spls[2].split("=")[1])
		if length == 0:
			print "warning(fna): contig"+i.id+" length:0"
			contig_clips[i.id] = 100000000 #definitely clip
		else:
			contig_clips[i.id] = int(len(i.seq.tostring())-length)
	infile.close()

	outfile = open(sys.argv[3],"w")
	infile = open(sys.argv[2],"rU")
	first = True
	count = 0
	for i in infile:
		if first == True:
			first = False
			outfile.write(i)
		else:
			spls =  i.strip().split("\t")
			contig = spls[1]
			if contig[0] == 'C': #cap C is cap3 and doesn't need filtering
				outfile.write(i)
			else:
				clip = contig_clips[contig]
				startv = int(spls[2])
				if startv < 0:
					startv -= 4
					startv += 21
					startv = abs(startv)
				if clip <= startv:
					outfile.write(i)
				count += 1
				if count % 1000000 == 0:
					print count
	infile.close()
	outfile.close()
	
