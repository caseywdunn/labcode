import sys
from numpy import *
from Bio import SeqIO

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python remove_singletons_for_quality.py in.fna in.qual out.fna"
		sys.exit(0)

	infile = open(sys.argv[1],"rU")
	infna_dict = {}
	for i in SeqIO.parse(infile,"fasta"):
		infna_dict[i.id] = i
	infile.close()

	inqlt_dict = {}
	inqual = open(sys.argv[2],"rU")
	for i in SeqIO.parse(inqual,"qual"):
		if i.id in infna_dict:
			inqlt_dict[i.id] = i
	inqual.close()

	keep = []
	for i in infna_dict:
		print mean(inqlt_dict[i].letter_annotations["phred_quality"])
