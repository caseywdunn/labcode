import sys,os,sqlite3
import newick3,phylo3

checknames = ['Frillagalma', 'Nanomia','Bargmannia']

nanomia_isotig_to_isogroup_file =  "/media/hd2/SIPHONOPHORE/grant_clusters/nanomia_mapfile"
frill_isotig_to_isogroup_file =  "/media/hd2/SIPHONOPHORE/grant_clusters/frill_mapfile"
barg_isotig_to_isogroup_file = "/media/hd2/SIPHONOPHORE/grant_clusters/barg_mapfile"

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python modify_cluster_tree_guid_to_isogroupnames.py database treedir outdir"
		sys.exit(0)
	
	database = sys.argv[1]
	
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()
	
	nanomia_isotig_to_isogroup_map = {}
	tfile = open(nanomia_isotig_to_isogroup_file,"r")
	for i in tfile:
		spls = i.strip().split("\t")
		for j in range(len(spls)-1):
			nanomia_isotig_to_isogroup_map[spls[j+1]] = spls[0]
	tfile.close()
	
	frill_isotig_to_isogroup_map = {}
	tfile = open(frill_isotig_to_isogroup_file,"r")
	for i in tfile:
		spls = i.strip().split("\t")
		for j in range(len(spls)-1):
			frill_isotig_to_isogroup_map[spls[j+1]] = spls[0]
	tfile.close()
	
	barg_isotig_to_isogroup_map = {}
	tfile = open(barg_isotig_to_isogroup_file,"r")
	for i in tfile:
		spls = i.strip().split("\t")
		for j in range(len(spls)-1):
			barg_isotig_to_isogroup_map[spls[j+1]] = spls[0]
	tfile.close()

	treedir = sys.argv[2]
	outdir = sys.argv[3]
	for i in os.listdir(treedir):
		if "RAxML_bestTree" in i:
			print i
			tfile = open(treedir+"/"+i,"r")
			tline = tfile.readlines()[0].strip()
			tree = newick3.parse(tline)
			for j in tree.leaves():
				#print j.label
				spls = j.label.split("@")
				if spls[0] in checknames:
					cur.execute("SELECT * FROM translated_seqs WHERE id = ?",(spls[1],))
					a = cur.fetchall()
					if len(a) > 0:
						try:
							if spls[0] == "Nanomia":
								j.label = spls[0]+"@"+nanomia_isotig_to_isogroup_map[str(a[0][2])]
							elif spls[0] == "Frillagalma":
								j.label = spls[0]+"@"+frill_isotig_to_isogroup_map[str(a[0][2])]
							elif spls[0] == "Bargmannia":
								j.label = spls[0]+"@"+barg_isotig_to_isogroup_map[str(a[0][2])]
						except:
							j.label = spls[0]+"@"+str(a[0][2])
				#print j.label
			tfile.close()
			ofile = open(outdir+"/"+i,"w")
			ofile.write(newick3.tostring(tree)+";");
			ofile.close()
			#sys.exit(0)
	
