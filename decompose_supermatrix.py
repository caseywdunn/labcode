import sys
import os
from Bio import AlignIO

if len(sys.argv) < 3:
	print >>sys.stderr, "usage: %s <supermatrix.phy> <partitions.txt>" % (
		os.path.basename(sys.argv[0]))
	sys.exit(0)

alignment = AlignIO.read(open(sys.argv[1]), 'phylip-relaxed')

partition = []
for line in open(sys.argv[2]):
	# Parse the range specifier in the RAxML partition format:
	#  WAG,genename=i-j
	# and decrement i by one to get 0-based, exclusive ranges.
	i, j = map(int, line.rstrip().partition('=')[2].split('-'))
	partition.append((i-1, j))

for record in alignment:
	for i, j in partition:
		# Collapse
		seq = str(record.seq[i:j]).strip('-').replace('-', 'X')
		if seq:
			print '>%s_p%d' % (record.id.replace(' ', '_'), i)
			print seq

