#! /usr/bin/env python
import re
import MySQLdb
import sys
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import string


# Set the output file name
FileName = sys.argv[1]

gene = ""

if (len(sys.argv) > 2):
	gene = sys.argv[2]

# Open the file
FileHandle = open(FileName, 'w')


MyConnection = MySQLdb.connect( host = "localhost", user = "root", \
     passwd = "", db = "siphseqs")
MyCursor = MyConnection.cursor()


sql = """SELECT
sequence_id, taxon, seq, gi, sample, location
FROM sequences
where gene = '%s' and mask < 1 order by taxon
;""" % (gene)
#print sql
MyCursor.execute(sql)
SQLLen = MyCursor.execute(sql)
rows = MyCursor.fetchall()
for row in rows:
	#print row
	sequence_id = row[0]
	taxon = row[1]
	sequence = row[2]
	gi = row[3]
	sample = row[4]
	location = row[5]
	print sequence_id, taxon
	
	header = taxon
	
	if (sample):
		header = header + "_" + sample
	if (location):
		header = header + "_" + location
		
	if (len(header)>20):
		header = header[0:20]
		
	if (gi):
		header = header + "_gi" + str(gi)
	
	
	
	header = header + "@" + str(sequence_id)
	
	header = string.replace(header, " ", "_")
	
	record = SeqRecord(Seq(sequence))
	record.id = header
	record.description = ""
	
	outlist = [record]
	SeqIO.write(outlist, FileHandle, "fasta")

FileHandle.close()
MyCursor.close()
MyConnection.close()