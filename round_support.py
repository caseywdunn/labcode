#!/usr/bin/env python	
import sys
import re

Usage = """
Usage:

round_support.py infile.txt > outfile.txt

"""

def roundstr(matchobj):
	instr = matchobj.group(0)
	#print instr
	l = ""
	r = ""
	if instr[0] == "_":
		instr = instr[1:]
		l = "_"
	if instr[len(instr)-1] == "_":
		instr = instr[:len(instr)-1]
		r = "_"
	#print instr
	return l + str(int(round(float(instr)))) + r

if len(sys.argv) < 2:
	print Usage
else:
	
	in_name = sys.argv[1]
	
	all = ""
	in_handle = open(in_name, "r")
	for line in in_handle:
		all = all + line
		
	all = re.sub("[\\d\\.]{2,}_", roundstr, all)
	all = re.sub("_[\\d\\.]{2,}", roundstr, all)
	
	print all