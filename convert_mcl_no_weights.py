import sys,os

"""
this will take the standard mcl input file and convert it to have
no weights at a particular threshold

so say the threshold is 20 , everything that has a weight of 20 or more
will remain and will just be assigned a weight of 20
"""

if __name__ == '__main__':
	if len(sys.argv) != 4:
		print "python convert_mcl_no_weights.py threshold infile outfile"
		sys.exit(0)
	
	threshold  = float (sys.argv[1])
	infile = open(sys.argv[2],"r")
	outfile = open(sys.argv[3],"w")
	for i in infile:
		spls = i.strip().split("\t")
		weight = float(spls[2])
		if weight >= threshold:
			outfile.write(spls[0]+"\t"+spls[1]+"\t20\n")
	outfile.close()
	infile.close()