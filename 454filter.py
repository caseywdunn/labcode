#! /usr/bin/env python
import sys
import re
from Bio import SeqIO

usage="""
454filter.py

Usage: 

454filter.py in.fna out.fna in.qual out.qual
	
"""

notes="""

"""

min_len = 150
min_mean_qual = 20
rarest_min = 0.1


def getMedian(numericValues):
	theValues = sorted(numericValues)
	if len(theValues) % 2 == 1:
		return theValues[(len(theValues)+1)/2-1]
	else:
		lower = theValues[len(theValues)/2-1]
		upper = theValues[len(theValues)/2]
		return (float(lower + upper)) / 2

if len(sys.argv)<4:
	print usage	

else:
	# Create a dictionary of sequences
	in_fasta_name = sys.argv[1]
	out_fasta_name = sys.argv[2]
	in_qual_name = sys.argv[3]
	out_qual_name = sys.argv[4]
	
	seqs = {} #A dictionary of sequence records keyed by id
	ids = [] #The ids of the sequence in the order they appear in the file, so they can be written back in order
	in_fasta_handle = open(in_fasta_name, "r")
	for record in SeqIO.parse(in_fasta_handle, "fasta"):
		#print "id: " + record.id + ", " + record.description
		seqs[record.id] = record
		ids.append(record.id)
	in_fasta_handle.close()
	
	# parse quality scores
	qualss = {}
	in_qual_handle = open(in_qual_name, "r")
	for record in SeqIO.parse(in_qual_handle, "qual"):
		
		id = record.id
		qual = record.letter_annotations['phred_quality']
		
		#print "id: " + id + ", " + str(qual)
		
		seqs[id].letter_annotations['phred_quality'] = qual
	in_qual_handle.close()
	
	out_fasta_handle = open(out_fasta_name,"w")
	out_qual_handle = open(out_qual_name, "w")
	
	print "#min_len = %d, min_mean_qual = %.3f, rarest_min = %.3f" % (min_len,  min_mean_qual, rarest_min)
	print "id	good	oldlen	newlen	mean_qual	median_qual	rarest	Cf	Gf	Tf	Af"
	
	n = 0
	for id in ids:
		n = n + 1
		seqr = seqs[id]
		#print "id: " + seqr.id + ", " + seqr.description

		desc = seqr.description
		seq = str(seqr.seq)
		qual = seqr.letter_annotations['phred_quality']
		
		i = -1
		newseq = ''
		newqual = []
		letter_counts = {}
		for bp in seq:
			i = i + 1
			if bp != 'X' and bp != 'x':
				newseq = newseq + bp
				newqual.append(qual[i])
				letter_counts[bp.upper()] = letter_counts.get(bp.upper(), 0) + 1
				
		newlen = len(newseq)
		mean_qual = -1
		median_qual = -1
		Cf = -1
		Gf = -1
		Tf = -1
		Af = -1
		
		if newlen > 0:
			mean_qual = sum(newqual) / float(newlen)
			median_qual = getMedian(newqual)
			Cf = letter_counts.get('C', 0) / float(newlen)
			Gf = letter_counts.get('G', 0) / float(newlen)
			Tf = letter_counts.get('T', 0) / float(newlen)
			Af = letter_counts.get('A', 0) / float(newlen)
		
		rarest = min([Cf, Gf, Tf, Af])
		
		good = 0
		
		if newlen >= min_len and mean_qual >= min_mean_qual and rarest >= rarest_min:
			good = 1
		
		
		print "%s	%d	%d	%d	%.3f	%.3f	%.3f	%.3f	%.3f	%.3f	%.3f" % (id, good, len(seq), newlen, mean_qual, median_qual, rarest, Cf, Gf, Tf, Af)
		
		if good > 0:
			newdesc = re.sub(r'length=\d+', 'length=' + str(newlen), desc)
			out_fasta_handle.write('>' + newdesc + "\n" + newseq + "\n")
			qual_str = ' '.join(["%s" % el for el in newqual])
			out_qual_handle.write('>' + newdesc + "\n" + qual_str + "\n")
		
		
		