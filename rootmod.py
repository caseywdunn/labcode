#! /usr/bin/env python

import dendropy
import sys

if len(sys.argv) < 2:
	print """

Takes filenames of trees in newick format. Reroots the trees at a new node
introduced at their midpoint, and outputs them in newick format with the 
appended suffix `.rr.tre`.

This is useful when you have trees that is rooted at a node and has a 
basal trifurcation, but require an entirely bifurcating tree.

Usage:
python rootmod.py tree1.tre tree2.tre ...

	"""

else:

	names = sys.argv[1:]

	for name in names:
		# print name
		tree = dendropy.Tree.get_from_path( name, 'newick' )
		tree.reroot_at_midpoint()
		tree.is_rooted = False
		tree.write_to_path( name + '.rr.tre' , 'newick')