import networkx as nx
import sys
import sqlite3

db = sqlite3.connect(sys.argv[1])
taxa = dict(db.execute("SELECT sequence_id, catalog_id FROM sequences;"))

print "Found", len(taxa), "sequences records"

hist = {}
hist_max = {}
hist_sum = {}

graph = nx.Graph()

for line in open(sys.argv[2]):
	id_from, id_to = line.split()[:2]
	if (id_from == id_to): continue
	graph.add_node(id_from)
	graph.add_node(id_to)
	graph.add_edge(id_from, id_to)

for subgraph in nx.connected_component_subgraphs(graph):
	unique = {}
	for i in subgraph.nodes_iter():
		taxon = taxa[int(i)]
		unique[taxon] = unique.get(taxon, 0) + 1
	n = len(unique)
	hist[n] = hist.get(n, 0) + 1
	taxon = max(unique, key=unique.get)
	if unique[taxon] > hist_max.get(n, [0])[0]:
		hist_max[n] = (unique[taxon], taxon)
	hist_max[n] = max(max(unique.itervalues()), hist_max.get(n, 0))
	hist_sum[n] = hist_sum.get(n, 0) + sum(unique.itervalues())

for n in sorted(hist):
	print n, hist[n], hist_sum[n] / float(n*hist[n]), hist_max[n]

