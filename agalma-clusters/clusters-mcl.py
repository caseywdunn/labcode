import sys
import sqlite3

db = sqlite3.connect(sys.argv[1])
taxa = dict(db.execute("SELECT sequence_id, catalog_id FROM sequences;"))

print "Found", len(taxa), "sequences records"

hist = {}
hist_max = {}
hist_sum = {}

for line in open(sys.argv[2]):
	unique = {}
	for i in line.rstrip().split():
		taxon = taxa[int(i)]
		unique[taxon] = unique.get(taxon, 0) + 1
	n = len(unique)
	hist[n] = hist.get(n, 0) + 1
	taxon = max(unique, key=unique.get)
	if unique[taxon] > hist_max.get(n, [0])[0]:
		hist_max[n] = (unique[taxon], taxon)
	hist_max[n] = max(max(unique.itervalues()), hist_max.get(n, 0))
	hist_sum[n] = hist_sum.get(n, 0) + sum(unique.itervalues())

for n in sorted(hist):
	print n, hist[n], hist_sum[n] / float(n*hist[n]), hist_max[n]

