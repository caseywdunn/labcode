# Written by Casey Dunn, ..., 2011
# Brown University
# Please see the LICENSE file included as part of this package.
"""Prepares raw paired end Illumina data for assembly.

"""


from biolite.wrappers import FastQC, RandomizeFastq, FilterIlluminaReads
from biolite.seqliteio import head

import os
from optparse import OptionParser
from datetime import datetime

import pre.diagnostics as log
from biolite.utils import safe_mkdir


def do_fastqc(input_file):
	"""Runs fastqc, taking care of setting up output directories and logging diagnostics
	
	"""
	
	unique_dir = log.unique_diagnostics_dir()
	fastqc_wrapper = FastQC(input_file, unique_dir)
	fastqc_wrapper.run()
	
	log.write(input_file, "fastqc_dir", unique_dir)
	


def prepare_for_assembly(input1_path, input2_path, outdir_path = './', thin = -1, input_format = 'fastq', transcriptome = False):
	"""Prepares raw paired end Illumina data for assembly.
	
	input1_name, input2_name - The paths to the two input fastq files
	outdir_name - The path to the directory where the output should be written
	thin - the minimum mean quality score for each read in each pair
	inputformat - used to determine the quality ascii offset, default apropriate for casava 1.8
	transcriptome - True if the dataset is from a transcriptome
	filter - True if reads that did not pass illumina filter need to be removed
	
	"""
	
	log.initialize_diagnostics(input1_path, outdir_path) # make initial entry
	
	# Convert paths to absolute paths so that log files etc are completely explicit
	input1_path = os.path.abspath( input1_path )
	input2_path = os.path.abspath( input2_path )
	outdir_path = os.path.abspath( outdir_path )
	
	(input1_dir, input1_file) = os.path.split( input1_path )
	(input2_dir, input2_file) = os.path.split( input2_path )
	
	# Prepare the output directory
	print("Setting up the output directory...")
	safe_mkdir(outdir_path)
	
	# Create the output paths, based on the output directory and the names of the input files
	output1_path = os.path.join(outdir_path, input1_file)
	output2_path = os.path.join(outdir_path, input2_file)
	
	# Prepare fastqc reports of raw reads
	# These will included reads that did not pass illumina filter, need to exclude these by
	# having fastqc ignore them or being sure that casava does not export them
	
	print("Preparing fastqc reports on raw data...")
	do_fastqc(input1_path)
	
	do_fastqc(input2_path)
	
	# Sanitize the data
	print("Sanitizing the raw data...")
	
	sanitize_suffix = ".s"
	sanitized1_path = output1_path + sanitize_suffix
	sanitized2_path = output2_path + sanitize_suffix
	
	filter = FilterIlluminaReads(input1_path, sanitized1_path, input2_path, sanitized2_path, i = True, quality = 28)
	filter.run()
	filter_summary = filter.getsummary()
	
	for attribute, value in filter_summary.items():
		log.write(input1_path, attribute, value)
	
	# Randomize the order of the reads
	print("Randomizing the order of the sanitized reads...")
	order_path = sanitized1_path + ".order.txt"
	randomized_suffix = ".r"
	randomized1_path = sanitized1_path + randomized_suffix
	randomized2_path = sanitized2_path + randomized_suffix
	
	print("Randomizing order of reads in first file")
	(randomized1_dir, randomized1_file) = os.path.split( randomized1_path )
	orderfile = randomized1_file + ".order"
	randomize_1 = RandomizeFastq(sanitized1_path, randomized1_path, 'w', orderfile)
	randomize_1.run()
	log.write(input1_path, "read1_randomize_filename", randomized1_path)
	
	print("Randomizing order of reads in second file (in the same order as the first file)")
	randomize_2 = RandomizeFastq(sanitized2_path, randomized2_path, 'r', orderfile)
	randomize_2.run()
	log.write(input1_path, "read2_randomize_filename", randomized2_path)
	
	# Remove the intermediate files
	print("Removing the sanitized reads...")
	os.remove(sanitized1_path)
	os.remove(sanitized2_path)
	
	print("Done with general preprocessing.")
		
		

def main():
	"""Parse options for command line access
	
	"""
	
	parser = OptionParser()
	parser.add_option("-a", "--input1", action="store", type="string", dest="input1_name", help="Path to the fastq file containing read 1")
	parser.add_option("-b", "--input2", action="store", type="string", dest="input2_name", help="Path to the fastq file containing read 2")
	parser.add_option("-o", "--outdir", action="store", type="string", dest="outdir_path", help="Path to the output directory")
	parser.add_option("-n", "--thin", action="store", type="float", dest="thin", default=-1, help="Remove pairs of reads where either read has a mean quality lower than this value")
	parser.add_option("-t", action="store_true", dest="transcriptome", default=False, help="Set this flag if the dataset is a transcriptome")
	parser.add_option("-f", action="store_true", dest="filter", default=False, help="Set this flag if reads that didn't pass filter need to be removed")
	(options, args) = parser.parse_args()
	
	
	prepare_for_assembly(options.input1_name, options.input2_name, outdir_path = options.outdir_path, thin = options.thin, transcriptome = options.transcriptome)
	
	

if __name__ == "__main__":
	main()
