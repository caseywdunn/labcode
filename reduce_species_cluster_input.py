import sys,os,sqlite3
import os.path
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from colors import *

"""
mostly to be used for testing the cluster methods

this script will reduce the mcl input like

sample1 sample2 value

to just include the species from the include_species file
"""

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python reduce_species_cluster_input.py database include_species mcl_in outfile"
		sys.exit(0)
	
	database = sys.argv[1]
	include_species = open(sys.argv[2],"r")
	mcl_in = sys.argv[3]
	mcl_file = open(mcl_in,"rU")
	mcl_out = sys.argv[4]
	mcl_outfile = open(mcl_out,"w")

	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()

	include = []
	for i in include_species:
		include.append(i.strip())
	include_species.close()

	species_names = []
	species_names_id = {} #key id value species
	cur.execute("SELECT id,name FROM species_names;")
	for i in cur:
		if str(i[1]) in include:
			species_names.append(str(i[1]))
			species_names_id[str(i[0])] = str(i[1])

	for i in mcl_file:
		spls = i.strip().split("\t")
		cur.execute("SELECT species_names_id FROM translated_seqs WHERE id = ?",(spls[0],))
		a = cur.fetchall()
		cur.execute("SELECT species_names_id FROM translated_seqs WHERE id = ?",(spls[1],))
		b = cur.fetchall()
		if len(a) > 0 and len(b) > 0:
			if str(a[0][0]) in species_names_id and str(b[0][0]) in species_names_id:
				mcl_outfile.write(i)
	mcl_file.close()
	mcl_outfile.close()
		
