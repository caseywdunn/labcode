#! /usr/bin/env python
import sys
import re
from Bio import SeqIO

#Takes a dictionary of strings and returns a dictionary of compiled regular expressions
def re_compile (dict_in):
	dict_out = {}
	for key in dict_in.keys():
		dict_out[key] = re.compile(dict_in[key], re.IGNORECASE)
	return dict_out

#For use in generating the replacement term in regular expressions
#Creates a replacement term that is as long is the matched term but
#consists only of X's
def XXX( match ):
	return 'X' * len( match.group() )

usage="""
454diagnostics.py

Usage: 

454diagnostics.py 3.TCA.454Reads > out.txt
	
"""

notes="""
454diagnostics_v0.0000001.py
had issues with reading the poly-A by itself

454diagnostics_v0.00001.py
"""

fastasuffix = ".fna"
qualsuffix = ".qual"
diagsuffix = ".diagnostics"
maskedsuffix = ".m"


#3' adapters
	#PD243			ATTCTAGAGGCCACCTTGGCCGACATGTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTVN
	#PD243-30TC		ATTCTAGAGGCCACCTTGGCCGACATGTTTTCTTTTCTTTTTTTTTCTTTTTTTTTTVN
	#PD243Mme-30TC	ATTCTAGAGCGCACCTTGGCCTCCGACTTTTCTTTTCTTTTTTTTTCTTTTTTTTTTVN
	#PD243Mme-24TC	ATTCTAGAGCGCACCTTGGCCTCCGACTTTTCTTTTTTTTCTTTTTTTTTTVN
	#three prime	           CACCTTGGCC
	#Mme			                     TCCRAC
	#Mme cut		                     TCCRAC                  $$$$

#5' adapters
	#PD242      AAGCAGTGGTATCAACGCAGAGTGGCCACGAAGGCCGGG
	#PD242AsiSI AAGCAGTGGTATCAACGCAGAGTGCGAT     CGCGGG
	


# Adapter sequences are used for masking as well as finding adapters, so they need to extend
# from the anchor sequence
adapters = {
'PD243' : r"\w*ATTCTAGA\w{17,21}[TCN]{4,}|[GAN]{4,}\w{17,21}TCTAGAAT\w*",
'PD243_SfiI_residual' : r"^TGGCCGAC|GTCGGCCA$",
#'PD243Mme' : r"\w+CCTCCGAC[CTN]+|[GAN]+GTCGGAGG\w+", 
'PD242' : r"^A{1,2}GCAGTG\w{21,31}GG|CC\w{21,31}CACTGCT{1,2}$", # Works for SfiI or AsiSI version 
'PD242_SfiI_residual' : r"^AGGCCGG+|C+CGGCCT$",
'PD242_AsiSI_residual' : r"^CGCGGG|CCCGCG$"
}

adapters_comp = re_compile(adapters)

endonuc = {
'AsiSI' : r"GCGATCGC",
'AsiSI_cut' : r"GCG$|^CGC",
'MmeI' : r"tcc[ag]ac|gt[ct]gga",
'MmeI_cut' : r"tcc[ag]ac\w{16,20}$|^\w{16,20}gt[ct]gga",
'SfiI' : r"GGCC\w{5}GGCC",
'SfiI_cut' : r"GGCC\w{1}$|^\w{1}GGCC",
'polytail' : r"^TTTTTT[CT]*|[GA]*AAAAAA$"
}

endonuc_comp = re_compile(endonuc)

if len(sys.argv)<2:
	print usage	

else:
	# Create a dictionary of sequences
	filename = sys.argv[1]
	
	seqs = {} #A dictionary of sequence records keyed by id
	ids = [] #The ids of the sequence in the order they appear in the file, so they can be written back in order
	shandle = open(filename + fastasuffix, "r")
	for record in SeqIO.parse(shandle, "fasta"):
		#print "id: " + record.id + ", " + record.description
		seqs[record.id] = record
		ids.append(record.id)
	shandle.close()
	
	# parse quality scores
	qualss = {}
	qhandle = open(filename + qualsuffix, "r")
	for record in SeqIO.parse(qhandle, "qual"):
		
		id = record.id
		qual = record.letter_annotations['phred_quality']
		
		#print "id: " + id + ", " + str(qual)
		
		seqs[id].letter_annotations['phred_quality'] = qual
	qhandle.close()
	
	
	dhandle = open(filename + diagsuffix,"w")
	mhandle = open(filename + maskedsuffix, "w")
	
	prefix_list = ['id', 'raw_l', 'trim_l', 'raw_q', 'trim_q', 'rarest']
	adapters_list = adapters_comp.keys()
	adapters_list.sort()

	endonuc_list = endonuc_comp.keys()
	endonuc_list.sort()
	
	header = "\t".join(prefix_list + adapters_list + endonuc_list)
	dhandle.write(header + "\n")
	
	
	n = 0 #The number of sequences analyzed
	count = 0 #Number of sequences read, may differ from n if analysis of some sequences is
			  #skipped during debugging
	
	
	total_raw_length = 0 # Total length of analyzed sequences
	total_trimmed_length = 0 # Total length of analyzed sequences after trimming
	total_raw_qsum = 0
	total_trimmed_qsum = 0
	
	adapters_results_sum = {}
	for key in adapters_list:
		adapters_results_sum[key] = 0
	
	endonuc_results_sum = {}
	for key in endonuc_list:
		endonuc_results_sum[key] = 0
	
	
	#Loop over the sequences, in the order they were read from the file
	for id in ids:
		count = count + 1
		#only look at 1 out of 10 sequences for debugging
		#if count % 10 > 0:
		#	continue
		n = n + 1
		seqr = seqs[id]
		raw_l = len(seqr.seq)
		total_raw_length = total_raw_length + raw_l
		seq = str(seqr.seq)
		endonuc_results = {}
		for key in endonuc_comp.keys():
			hits = endonuc_comp[key].findall(seq)
			endonuc_results[key] = len(hits)
			
		adapters_results = {}
		for key in adapters_comp.keys():
			seq, nmatch = adapters_comp[key].subn(XXX, seq)
			adapters_results[key] = nmatch

		qual = seqr.letter_annotations['phred_quality']
		
		raw_qsum = sum(qual)
		
		total_raw_qsum = total_raw_qsum + raw_qsum
		
		trim_l = 0
		trim_qsum = 0
		position = -1
		letter_counts = {}
		for bp in seq:
			position = position + 1
			if bp != 'X' and bp != 'x':
				trim_l = trim_l + 1
				trim_qsum = trim_qsum + qual[position]
				letter_counts[bp.upper()] = letter_counts.get(bp.upper(), 0) + 1
		
		total_trimmed_length = total_trimmed_length + trim_l
		total_trimmed_qsum = total_trimmed_qsum + trim_qsum
		# Calculate the frequency of the rarest letter
		rarest_count = min([letter_counts.get('C', 0), letter_counts.get('G', 0), letter_counts.get('T', 0), letter_counts.get('A', 0)])
		
		rarest = -1
		if trim_l > 0:
			rarest = rarest_count/float(trim_l)
		
		raw_q = -1
		if raw_l > 0:
			raw_q = raw_qsum/float(raw_l)
			
		trim_q = -1
		if trim_l > 0:
			trim_q = trim_qsum/float(trim_l)
		#['id', 'raw_l', 'trim_l', 'raw_q', 'trim_q', 'rarest']
		line_list = [id, str(raw_l), str(trim_l), '%.2f' % raw_q, '%.2f' % trim_q, '%.2f' % rarest]
		
		for key in adapters_list:
			line_list.append(str(adapters_results[key]))
			adapters_results_sum[key] = adapters_results_sum[key] + adapters_results[key]

		for key in endonuc_list:
			line_list.append(str(endonuc_results[key]))
			endonuc_results_sum[key] = endonuc_results_sum[key] + endonuc_results[key]
		
		line = "\t".join(line_list)
		dhandle.write(line + "\n")
		
		# Write the masked sequence to a fasta file. The X's mess up the alphabet of the seqio module,
		# so just do it as text
		mhandle.write('>' + seqr.description + "\n")
		mhandle.write(seq + "\n")
	
	raw_length_mean = -1
	trimmed_length_mean = -1
	if n > 0:
		raw_length_mean = total_raw_length/float(n)
		trimmed_length_mean = total_trimmed_length/float(n)
	raw_q_mean = -1
	fraction_retained = -1
	if total_raw_length > 0:
		raw_q_mean = total_raw_qsum/float(total_raw_length)
		fraction_retained = float(total_trimmed_length)/total_raw_length
	trimmed_q_mean = -1
	if total_trimmed_length > 0:
		trimmed_q_mean = total_trimmed_qsum/float(total_trimmed_length)
	
	print "Number of sequences: " + str(n)
	print "Total raw length: " + str(total_raw_length)
	print "Average raw length: %.2f" % raw_length_mean
	print "Average raw quality: %.2f" % raw_q_mean
	print "Fraction retained after trimming: %.2f" % fraction_retained
	print "Total trimmed length: " + str(total_trimmed_length)
	print "Average trimmed length: %.2f" % trimmed_length_mean
	print "Average trimmed quality: %.2f" % trimmed_q_mean
	
	for key in adapters_list:
		print "Adapter count " + key + ": " + str(adapters_results_sum[key])
		
	for key in endonuc_list:
		print "Endonuclease count " + key + ": " +str(endonuc_results_sum[key])
