import sys,os,newick3,phylo3,sqlite3

"""
this is going to wrap the 
it is going to write out the seqs for each of the trees
"""

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python paralogy_prunner_seqs.py intree db"
        sys.exit(0)
    infile = open(sys.argv[1],"r")
    database = sys.argv[2]
    if os.path.exists(database) == False:
        print "the database has not been created or you are not in the right place"
    con = sqlite3.connect(database)
    cur = con.cursor()
    count = 0
    for i in infile:
        if len(i) > 3:
            intree = newick3.parse(i)
            lvs = intree.leaves()
            names = []
            for j in lvs:
                names.append(j.label)
            outfile = open(sys.argv[1]+"."+str(count)+".fasta","w")
            for j in names:
                spls = j.split("@")
                cur.execute("SELECT seq FROM translated_seqs WHERE id = ?",(spls[1],))
                a = cur.fetchall()
                if len(a) != 1:
                    print "there is a problem with seq id: "+spls[1]
                    sys.exit(0)
                seq = a[0][0]
                outfile.write(">"+spls[0]+"\n"+seq+"\n")
            outfile.close()
            count += 1
