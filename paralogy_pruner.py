import newick3,phylo3,os,sys

"""
the pattern for the tip names is name@seq, ignore the @NAME, just the front
"""
#if pattern changes, change it here
def get_name(name):
    return name.split("@")[0]

def get_leaf_names(leaves):
    nms = []
    for i in leaves:
        nms.append(i.label)
    return nms

#for forward
def get_front_leaf_names_score(node):
    leaves = node.leaves()
    uniq_lf_nms = []
    score = 0
    for i in leaves:
        nm = get_name(i.label)
        if nm in uniq_lf_nms:
            score = -1
            break
        else:
            uniq_lf_nms.append(nm)
            score += 1
    if score < 3:
        score = -1
    return score

#for back
def get_back_leaf_names_score(node,root):
    rtlvs = get_leaf_names(root.leaves())
    ndlvs = get_leaf_names(node.leaves())
    leaves = set(rtlvs) - set(ndlvs)
    #print len(leaves),len(rtlvs),len(ndlvs)
    uniq_lf_nms = []
    score = 0
    for i in leaves:
        if i.split("@")[0] in uniq_lf_nms:
            score = -1
            break
        else:
            uniq_lf_nms.append(i.split("@")[0])
            score += 1
    if score < 3:
        score = -1
    return score

#takes a scoring array for front or back and then takes the node
def prune(scr, node, root,outfile):
    if scr[0] > scr[1]: #front, simple
        outfile.write(newick3.tostring(node)+";\n")
        node.prune()
        #print "forward"
        return root,node==root
    else: #more complicated
        #need to prune the node from the tree
        #print scr
        if node != root:
            par = node.parent
            par.remove_child(node)
        node.prune()
        #print "reverse"
        #print newick3.tostring(node)
        #print newick3.tostring(root)
        outfile.write(newick3.tostring(root)+";\n")
        return node,node == root

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python paralogy_pruner.py infile.tre outfile.tre"
        sys.exit(0)
    infile = open(sys.argv[1],"r")
    intree = newick3.parse(infile.readline())
    infile.close()

 #   print "Leaves: ",len(intree.leaves())

    #go to each node and create the score array with left right and back scores
    #score of -1 means that the including the node and clipping toward whichever direction, includes
    #bad paralogy

    trees = []
    

    
    going = True
    #need a while loop here with while going, set not going if highest_node == None
    curroot = intree
    outfile = open(sys.argv[2],"w")
    outfile2 = open(sys.argv[2]+".residuals","w")
    while going:
        highest = 0
        highest_node = None 
        score_hashes = {} # key is node, value is array
        for i in curroot.iternodes():
            fr = get_front_leaf_names_score(i)
            bk = get_back_leaf_names_score(i,curroot)
            score_hashes[i] = [fr,bk]
            if fr > highest or bk > highest:
                highest_node = i
                if fr > highest:
                    highest = fr
                else:
                    highest = bk
        #print highest
        if highest_node != None:
            curroot,done = prune(score_hashes[highest_node],highest_node,curroot,outfile)
            if done:
                going = False
                break
            if len(curroot.leaves()) < 3:
                going = False
                break
            curroot  = newick3.parse(newick3.tostring(curroot)+";")
        else:
            going = False
            break

#        print highest,highest_node,score_hashes[highest_node]
        outfile2.write(newick3.tostring(curroot)+";")
        #end while
    outfile.close()
    outfile2.close()
