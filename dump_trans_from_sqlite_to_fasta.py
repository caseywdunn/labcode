import sys,sqlite3,os
from Bio import SeqIO
from colors import *

#database = "mollusk.db"

"""
this will export all the translations into the outfile with the id from 
the database as the unique id

this will not include species that aren't in the listfile -- these are just
species_a
species_b
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python dump_trans_from_sqlite_to_fasta.py database includespecies outfile"
		sys.exit(0)
	database = sys.argv[1]
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()

	includespecies_id = {}
	includefile = open(sys.argv[2],"r")
	for i in includefile:
		cur.execute("SELECT id FROM species_names where name = ?;",(i.strip(),))
		a = cur.fetchall()
		spid = a[0][0]
		includespecies_id[i.strip()] = spid
	includefile.close()
	
	handle = open(sys.argv[3],"w")
	for j in includespecies_id:
		count = 0 
		cur.execute("SELECT id,seq FROM translated_seqs where species_names_id = ?;",(includespecies_id[j],))
		for i in cur:
			handle.write(">"+str(i[0])+"\n"+str(i[1])+"\n")
			count += 1
		print GREEN+j+": "+BLUE+str(count)+RESET
	
	handle.close()
	con.close()
