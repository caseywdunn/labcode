import sys
from Bio import SeqIO
from Bio.Sequencing import Ace

def cut_ends(read, start, end):
	"""Replace residues on either end of a sequence with gaps.
	In this case we want to cut out the sections of each read which the assembler has 
  decided are not good enough to include in the contig and replace them with gaps 
  so the other information about positions in the read is maintained"""
  
	return (start-1) * '-' + read[start-1:end] + (len(read)-end) * '-'
 
def pad_read(read, start, conlength):
	""" Pad out either end of a read so it fits into an alignment.
	The start argument is the position of the first base of the reads sequence in
	the contig it is part of. If the start value is negative (or 0 since ACE files 
	count from 1, not 0) we need to take some sequence off the start 
	otherwise each end is padded to the length of the consensus with gaps."""
	if start < 1:
		seq = read[-1*start+1:]
	else:
		seq = (start-1) * '-' + read
	seq = seq + (conlength-len(seq)) * '-'
	return seq

def parse_cont_graph_file(file):
	contcondepth = {}
	contdepth = {}
	contlength = {}
	isograph = {}
	firstC = False
	for i in contgra:
		if i[0] != "C" and firstC == False:
			spls = i.strip().split("\t")
			contdepth[int(spls[0])] = float(spls[3])
			contlength[int(spls[0])] = float(spls[2])
		elif i[0] == "C":
			firstC = True
			spls = i.strip().split("\t")
			if int(spls[1]) not in contcondepth:
				contcondepth[int(spls[1])] = {}
			if int(spls[3]) not in contcondepth:
				contcondepth[int(spls[3])] = {}
			contcondepth[int(spls[1])][int(spls[3])] = float(spls[5])
			contcondepth[int(spls[3])][int(spls[1])] = float(spls[5])
		elif i[0] == "S":
			spls = i.strip().split("\t")
			isograph[int(spls[1])] = []
			spls3 = spls[3].split(";")
			for j in spls3:
				isograph[int(spls[1])].append(int(j.split(":")[0]))
	return [contcondepth,contdepth,contlength,isograph]


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python summarize_isotig_ace_to_pile.py isotig.ace 454ContigGraph.txt outfile"
		sys.exit(0)
	#open the 454ContigGraph
	contgra = open(sys.argv[2],"rU")
	contcondepth,contdepth,contlength,isograph = parse_cont_graph_file(contgra)
	contgra.close()
	
	contigs_left = []
	#stores the isotigs containing each contig, key : contig, value: list of isotigs
	isotigs_w_contigs = {}
	
	for i in contlength:
		contigs_left.append(i)
		isotigs_w_contigs[i] = []
	
	for i in isograph:
		for j in isograph[i]:
			isotigs_w_contigs[j].append(i)

	outw = open(sys.argv[3],"w")
	for isotig in Ace.parse(open(sys.argv[1])):
		nm = isotig.name
		if "contig" in nm:
			continue
		inm = int(nm[-5:])
		print inm,isograph[inm]
		tseqs = []
		gaps = []
		for i in range(len(isotig.sequence)):
			if isotig.sequence[i] == "*":
				gaps.append(i)
		gaps = sorted(gaps,reverse=True)
		#print gaps
		# print isotig.sequence
		for readn in range(len(isotig.reads)):
			#print contig.reads[readn].qa.align_clipping_start
			clipst = isotig.reads[readn].qa.qual_clipping_start
			clipe = isotig.reads[readn].qa.qual_clipping_end
			start = isotig.af[readn].padded_start
			seq = cut_ends(isotig.reads[readn].rd.sequence, clipst, clipe)
			seq = pad_read(seq, start, len(isotig.sequence))
			#take out to leave in gaps
			for j in gaps:
				seq = seq[:j]+seq[j+1:]
			tseqs.append(seq)
			#print clipst,clipe,start
		curcount = 0
		for c in isograph[inm]:
			if c in contigs_left:
#				print c,contlength[c]
				clist = [0] * (int(contlength[c]))
				for s in range(int(contlength[c])):
					cnt = 0
					for k in tseqs:
						try:
							if k[s+curcount].upper() == 'A' or k[s+curcount].upper() == 'C' or k[s+curcount].upper() == 'G' or k[s+curcount].upper() == 'T':
								cnt += 1
						except:
							print "length issue: trying "+str(s+curcount)+" on "+str(len(k))
							continue
					clist[s] = cnt
				stri = "contig"+str(c)
				for s in range(int(contlength[c])):
					stri += " "+str(clist[s])
				#print inm,c
				outw.write(stri+"\n")
				contigs_left.remove(c)
				print len(contigs_left)
			curcount += int(contlength[c])
	outw.close()
