import sys
from Bio import SeqIO

def list_to_string(li):
	stri = ""
	count = 0
	for i in li:
		if count != 0:
			stri += " "
		stri += str(i)
		count += 1
	return stri

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python exclude_subset_contigs_clip.py fna qual"
		sys.exit(0)
	infile = open(sys.argv[1],"rU")
	outfile = open(sys.argv[1]+".clipped","w")
	for i in SeqIO.parse(infile,"fasta"):
		spls = i.description.split(" ")
		length = int(spls[2].split("=")[1])
		if length == 0:
			print "warning(fna): contig"+i.id+" length:0"
		else:
			subseq = i.seq.tostring()[-length:]
			outfile.write(">"+str(i.description)+"\n"+subseq+"\n")
	infile.close()
	outfile.close()
	
	infile = open(sys.argv[2],"rU")
	outfile = open(sys.argv[2]+".clipped","w")
	for i in SeqIO.parse(infile,"qual"):
		spls = i.description.split(" ")
		length = int(spls[2].split("=")[1])
		if length == 0:
			print "warning(qual): contig"+i.id+" length:0"
		else:
			subseq = i.letter_annotations["phred_quality"][-length:]
			outfile.write(">"+str(i.description)+"\n")
			subseq = list_to_string(subseq)
			outfile.write(subseq+"\n")
	infile.close()
	outfile.close()
