import sys

"""
this assumes that the infile will be the files that have the type:
contig	HF	HR	IF	IR	SF	SR
Contig4513	0	0	0	0	0	0
Contig4513	0	0	0	0	0	0
Contig4513	0	0	0	0	0	0
Contig4513	0	0	0	0	0	0

"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python solid_counts_summary.py infile outfile"
		sys.exit(0)
	
	infile = open(sys.argv[1],"rU")
	first = True
	curcontig = None
	stillonplus = False
	stillonminus = False
	curpluspile = None
	curminuspile = None
	maxdepth = 0
	pluspiles = {} #key is contig, value is array of arrays for each pile with startpos, endpos, maxdepth, orientation
	minuspiles = {}
	curpos = 0
	for i in infile:
		if first == True:
			first = False
			continue
		spls = i.strip().split("\t")
		if curcontig == None:
			curcontig = spls[0]
		elif curcontig != spls[0]: #new contig
			stillonplus = False
			stillonminus = False
			maxdepth = 0
			curpluspile = None
			curminuspile = None
			curpos = 0
			curcontig = spls[0]
		plus = int(spls[5])
		minus = int(spls[6])
		pluspile = []
		minuspile = []
		if plus > 0 and stillonplus == False:
			pluspile = [curpos,0,plus,"+"]
			stillonplus = True
			if curpluspile == None:
				curpluspile = 0
			else:
				curpluspile += 1
			if curcontig not in pluspiles:
				pluspiles[curcontig] = []
			if curcontig not in minuspiles:
				minuspiles[curcontig] = []
			pluspiles[curcontig].append(pluspile)
		elif plus > 0 and stillonplus == True:
			if pluspiles[curcontig][curpluspile][2] < plus:
				pluspiles[curcontig][curpluspile][2] = plus
		if minus > 0 and stillonminus == False:
			minuspile = [curpos,0,minus,"-"]
			stillonminus = True
			if curminuspile == None:
				curminuspile = 0
			else:
				curminuspile += 1
			if curcontig not in minuspiles:
				minuspiles[curcontig] = []
			if curcontig not in pluspiles:
				pluspiles[curcontig] = []
			minuspiles[curcontig].append(minuspile)
		elif minus > 0 and stillonminus == True:
			if minuspiles[curcontig][curminuspile][2] < minus:
				minuspiles[curcontig][curminuspile][2] = minus
		if plus == 0 and stillonplus == True:
			stillonplus = False
			pluspiles[curcontig][curpluspile][1] = curpos
		if minus == 0 and stillonminus == True:
			stillonminus = False
			minuspiles[curcontig][curminuspile][1] = curpos
		curpos += 1
	infile.close()

	outfile = open(sys.argv[2],"w")
	outfile.write("contig\tstart\tend\tmaxdepth\torientation\n")
	for i in pluspiles:
		#plus
		for j in range(len(pluspiles[i])):
			outfile.write(i+"\t"+str(pluspiles[i][j][0])+"\t"+str(pluspiles[i][j][1])+"\t"+str(pluspiles[i][j][2])+"\t"+str(pluspiles[i][j][3])+"\n")
		#minus
		for j in range(len(minuspiles[i])):
			outfile.write(i+"\t"+str(minuspiles[i][j][0])+"\t"+str(minuspiles[i][j][1])+"\t"+str(minuspiles[i][j][2])+"\t"+str(minuspiles[i][j][3])+"\n")
	outfile.close()
