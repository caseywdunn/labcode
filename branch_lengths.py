#!/usr/bin/env python
import sys
from collections import defaultdict
from ete3 import Tree, TreeStyle
from operator import attrgetter

threshold = 0.05

ts = TreeStyle()
ts.show_leaf_name = True
#ts.show_branch_length = True
ts.show_branch_support = True
tree = Tree(sys.argv[1])
for node in tree.traverse(strategy="postorder"):
	if node.is_leaf():
		node.support = 0
		node.add_feature("under", True)
	if not node.is_leaf():
		children = node.get_children()
		branchlength = children[0].get_distance(children[1]) + children[0].support + children[1].support
		node.support = branchlength
		if branchlength < threshold:
			node.add_feature("under", True)
		else:
			node.add_feature("under", False)
		print '%f' % branchlength, node, '\n'
tree.render(sys.argv[1] + ".pdf", tree_style=ts)

for node in tree.traverse(strategy="levelorder", is_leaf_fn=attrgetter("under")):
	if node.support != 0 and node.under == True:
		candidates = defaultdict(set)
		for leaf in node.get_leaves():
			species, _, model_id = leaf.name.partition('@')
			candidates[species].add(model_id)
		for child in node.get_children():
			child.checked = True
		for species in candidates:
			if len(candidates[species]) > 1:
				print species, candidates[species]



# vim: noexpandtab ts=4 sw=4
