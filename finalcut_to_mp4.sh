#! /bin/bash

# Given the path to a final cut pro library, create mp4
# videos from all original media. Parses and applies 
# time stamps from the file names.
# 
# Still unstable - has issues with spaces in some folder names etc.

# The final cut pro library
LIBRARYDIR="$1"

OUTDIR="$2"

find $LIBRARYDIR -name "Original Media" -type d -print | while read mediadir; do
    for file in "$mediadir"/*
	do
 		if [[ $file == *.dv ]] || [[ $file == *.mov ]] ; then
 			#ffmpeg -i "$file" "${file%.html}.txt"
 			# The name without the path
 			filename=`basename "$file"`

 			# The name without the extension
 			base=${filename%.*}

 			echo $file
 			echo $filename
 			echo $base

 			outfile=$OUTDIR"$base".mp4
 			echo "Processing $file"
 			echo "Writing output to $outfile"
 			< /dev/null ffmpeg -i "$file" "$outfile"

 			if [[ $base == clip-20* ]] ; then
 				stamp=${base/clip-/}
 				stamp=${stamp//[- ;]/}

 				# Get rid of second
 				stamp=${stamp:0:12}
 				echo "Setting stamp to $stamp"
 				touch -t "$stamp" "$outfile"
 			fi

 		else
 			cp "$file" "$OUTDIR"
 		fi
	done
done

