import sys,os
import os.path
from Bio import SeqIO
from colors import *

"""
this will take the output from the mcl analysis and write out the fasta
files for each cluster (excluding the genbank names)

this requires the directory structure to be somewhat predictable

it will look for files in the directory translations

USE THE OTHER ONE THAT READS THE FILES FIRST! MUCH FASTER
"""

SMALLESTCLUSTER = 3 

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python extract_clusters_from_mcl_out.py mcl_in INDIR OUTDIR"
		sys.exit(0)

	mcl_in = sys.argv[1]
	mcl_file = open(mcl_in,"rU")
	clusters = {}
	count = 0
	for i in mcl_file:
		spls = i.strip().split("\t")
		tcl = []
		for j in spls:
			try:
				int(j)
			except:
				tcl.append(j)
		clusters[count]=tcl
		count += 1

	species_files = {}
	
	INDIR = sys.argv[2]
	OUTDIR = sys.argv[3]
	for i in os.listdir(INDIR):
		species_name = i
		species_files[i] = []
		print "working with files from",BOLD,species_name,RESET
		TRANDIR = INDIR+"/"+i+"/translations/"
		for j in os.listdir(TRANDIR):
			print BLUE,j,RESET
			species_files[i].append(j)
	count = 0
	error_file = open("cluster_errors","w")
	for i in clusters:
		if len(clusters[i]) >= SMALLESTCLUSTER:
			print i,len(clusters[i])
			for j in clusters[i]:
				#print j
				TRANDIR = INDIR+"/"+j.split("_")[0]+"_"+j.split("_")[1]+"/translations/"
				#in case the species is denoted with a single name
				cid_index = 2
				if os.path.exists(TRANDIR) == False:
					TRANDIR =  INDIR+"/"+j.split("_")[0]+"/translations/"
					cid_index = 1
				found = False
				seq = None
				for k in os.listdir(TRANDIR):
					seqhandle = open(TRANDIR+k,"r")
					for m in SeqIO.parse(seqhandle,"fasta"):
						#finding the bit for prot4EST species which have
						# the pattern ABC##### ABP#####
						sid = m.id
						if sid[:3] == "jgi":
							sid = sid.split("|")[2]
						cid = ""
						cid_spls = j.split("_")
						if len(cid_spls) == cid_index-1:
							cid = cid_spls[cid_index]
						else:
							for n in cid_spls[cid_index:]:
								cid += n+"_"
							cid = cid[:-1]
						if cid[:3] == "jgi":
							cid = cid.split("|")[2]
						if len(sid) == 8 and len(cid) == 8:
							sid = sid[3:]
							cid = cid[3:]
						if sid == cid:
							seq = m
							found = True
							break
					if found == True:
						break
					seqhandle.close()
				if found != True:
					print "ERROR:no break"
					error_file.write(j+"\n")
					print j
					#sys.exit(0)
			count += 1
	error_file.close()
	mcl_file.close()
		
