#! /usr/bin/env python
import sys
from Bio import AlignIO

InFileName = sys.argv[1]

alignment = AlignIO.read(InFileName, "fasta")
#print alignment

AlignIO.write([alignment], InFileName + ".phy", "phylip-relaxed")
