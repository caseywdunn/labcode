from Bio import SeqIO

"""
this will calculate the mean quality for each sequence
"""


import sys

def mean(li):
	mn = 0
	ll = float(len(li))
	for i in li:
		mn += (i/ll)
	return mn

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python calculate_mean_quality_fastq.py input output"
		sys.exit(0)

	sample = 100000 #sample only every
	numofseqs = 0
	seq_quals = []
	infile = open(sys.argv[1],"r")
	outfile = open(sys.argv[2],"w")
	#old illumina files use this
	#	for i in SeqIO.parse(infile,"fastq-solexa"):
	for i in SeqIO.parse(infile,"fastq-illumina"):
		if numofseqs % sample == 0:
			#old illumina files use this
			#mn = mean(i.letter_annotations['solexa_quality'])
			mn = mean(i.letter_annotations['phred_quality'])
			outfile.write(str(mn))
			outfile.write("\n")
			outfile.flush()
		numofseqs += 1
		if numofseqs % sample == 0:
			print numofseqs
	infile.close()
	outfile.close()
	
	
	#outfile = open(sys.argv[2],"w")
	#for i in range(len(seq_quals)):
		#seq_quals[i] /= float(numofseqs)
		#outfile.write(str(seq_quals[i])+" ")
	#outfile.write("\n")
	#print seq_quals
	#outfile.close()
