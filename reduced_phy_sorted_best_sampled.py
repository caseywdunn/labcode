import sys,os
import random
"""
in order to conduct the phylobayes analyses we have to shorten the matrices and this is one way that we can do this


"""

#to identify missing data
missing = "X-?"

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python reduced_phy_sorted_best_sampled.py infile.phy outfile proportion"
		sys.exit(0)
	infile= open(sys.argv[1],"r")
	outfile = open(sys.argv[2],"w")
	prop = float(sys.argv[3])

	seqs = {} #key is name and value is seq
	finalseqs = {}
	first = True
	seqlength = 0
	for i in infile:
		if first ==True:
			first = False
			continue
		i = i.strip()
		if len(i) > 1:
			spls = i.split()
			seqs[spls[0]] = spls[1]
			seqlength = len(spls[1])
			finalseqs[spls[0]] = ""
	#calc_site_sample
	site_samp = {}
	samp_site = {}
	vals = []
	for i in range(seqlength+1):
		count = 0
		for j in seqs:
			if seqs[j][i] not in missing:
				count += 1
		site_samp[i] = count
		if count not in samp_site:
			samp_site[count] = []
			vals.append(count)
		samp_site[count].append(i)
	vals.sort(reverse=True)
	numfin = int(prop * seqlength)
	print "final size: ",numfin
	samp = []
	count = 0
	for i in vals:
		for j in samp_site[i]:
			samp.append(j)
			if count >= numfin:
				break
			count += 1
		if count >= numfin:
			break
	outfile.write("#NEXUS\n")
	outfile.write("BEGIN DATA;\n")
	outfile.write("\tDIMENSIONS NTAX="+str(len(seqs))+" NCHAR="+str(numfin)+";\n")
	outfile.write("\tFORMAT DATATYPE = protein GAP=- MISSING=?;\n")
	outfile.write("\tMATRIX\n")
	for i in samp:
		for j in finalseqs:
			finalseqs[j] += seqs[j][i]
	for i in finalseqs:
		outfile.write("\t"+i+"\t"+finalseqs[i]+"\n")
	outfile.write(";\n")
	outfile.write("END;\n")
