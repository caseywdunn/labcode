import sys
from svg_lib import *
from Bio import SeqIO

"""
creates and SVG of the technologies and the ACE for the contig

if using the GC content then takes the 454AllContigs.fna.clipped file as well.

this one has added features:
1) colors for the nucleotides
2) vertical and horizontal
3) filename (last two digits) labels
4) percentage of the page layout
5) multiple samples

"""


SLIDING_WINDOW = 31
vertical = True
XSCALE = .1 #probably don't want to change this
YSCALE = .001 #lower this to something like 0.1 or 0.01 if too large to read in file
ACE_SCALE = 100 #scale the 454
SEQCOLOR = {'A':'green','C':'orange','G':'red','T':'blue'}
REV_COMP = False

def average(nums):
	avg = 0
	for i in nums:
		avg += i/len(nums)
	return avg

if __name__ == "__main__":
	if len(sys.argv) < 6:
		print "usage: python convert_combined_to_svg_contig.py contig_id infile_ace_pile_file 454AllContigs.fna.clipped outfile infile_3types_## [infile_3types_## ...]"
		sys.exit(0)

	contig = sys.argv[1]

	data = {}
	ordered_d = []
	lengthofseq = 0
	#should make this for each technology
	MAX_Y = [] #upper and lower
	for i in range(4):
		MAX_Y.append([0,0])#upper then lower max for each technology
	MAX_Y[3][1] = 2 # lower max for ace is small
	for i in sys.argv[5:]:
		ordered_d.append(i)
		print "reading: "+i
		infile = open(i,"r")
		tdata = []
		lc = len(contig)
		for j in infile:
			if j[:lc] == contig:
				spls = j.strip().split("\t")
				tdata.append(spls[1:])
				heu = int(spls[1:][0])
				if heu > MAX_Y[0][0]:
					MAX_Y[0][0] = heu
				hel = int(spls[1:][1])
				if hel > MAX_Y[0][1]:
					MAX_Y[0][1] = hel
				ilu = int(spls[1:][2])
				if ilu > MAX_Y[1][0]:
					MAX_Y[1][0] = ilu
				ill = int(spls[1:][3])
				if ill > MAX_Y[1][1]:
					MAX_Y[1][1] = ill
				sou = int(spls[1:][4])
				if sou > MAX_Y[2][0]:
					MAX_Y[2][0] = sou
				sol = int(spls[1:][5])
				if sol > MAX_Y[2][1]:
					MAX_Y[2][1] = sol
		data[i] = tdata
		if len(tdata) != 0:
			lengthofseq = len(tdata)
		infile.close()

	if REV_COMP == True:
		for i in ordered_d:
			data[i].reverse()
			for j in range(len(data[i])):
				hu = data[i][j][0]
				data[i][j][0] = data[i][j][1]
				data[i][j][1] = hu
				iu = data[i][j][2]
				data[i][j][2] = data[i][j][3]
				data[i][j][3] = iu
				su = data[i][j][4]
				data[i][j][4] = data[i][j][5]
				data[i][j][5] = su
		hu = MAX_Y[0][0]
		MAX_Y[0][0] = MAX_Y[0][1]
		MAX_Y[0][1] = hu
		il = MAX_Y[1][0]
		MAX_Y[1][0] = MAX_Y[1][1]
		MAX_Y[1][1] = il
		so = MAX_Y[2][0]
		MAX_Y[2][0] = MAX_Y[2][1]
		MAX_Y[2][1] = so
	#just a little bit of white space
	XOFFSET = 600
	YOFFSET = 10
	
	ld = lengthofseq
	
	ace_list = None
	infile_ace = open(sys.argv[2],"r")
	for i in infile_ace:
		if i[:lc] == contig:
			spls = i.strip().split(" ")
			ace_list = spls[1:]
			for j in spls[1:]:
				if int(j) > MAX_Y[3][0]:
					MAX_Y[3][0] = int(j)
	if REV_COMP == True:
		ace_list.reverse()

	MAX_Y[3][0] *= ACE_SCALE

	infile_ace.close()
	if ace_list == None:
		print "contig not found in the ace summary file"
	
	sliding = []
	
	tsliding = []
	infile_gc = open(sys.argv[3],"r")
	seq = ""
	defline = ""
	for i in SeqIO.parse(infile_gc,"fasta"):
		if i.id == contig:
			defline = i.description
			seq = i.seq.tostring()
			if REV_COMP == True:
				seq = i.seq.reverse_complement().tostring()
			for curcount in range(len(i.seq)):
				if i.seq.tostring()[curcount].upper() == 'G' or i.seq.tostring()[curcount].upper() == 'C':
					tsliding.append(1.)
				else:
					tsliding.append(0.)
	count = 0
	sliding = tsliding
	for i in range(len(tsliding)):
		avg = average(tsliding[int(max(i-(SLIDING_WINDOW/2.),0)):int(min(i+(SLIDING_WINDOW/2.),len(tsliding)))])
		sliding[count] = avg
		count += 1

	#scene = Scene(sys.argv[4],(((YOFFSET+MAX_Y)*5)-(MAX_Y/2)+MAX_Y_ACE)*YSCALE,((2*XOFFSET*len(data)+len(data))+(lengthofseq*len(data)))*XSCALE)
	sumMAX_Y = 0
	for i in range(len(MAX_Y)):
		sumMAX_Y += sum(MAX_Y[i])
	
	scene = Scene(sys.argv[4],((YOFFSET*len(data))+sumMAX_Y)*YSCALE,((XOFFSET*len(data)+XOFFSET)+(lengthofseq*len(data)))*XSCALE)

	#doing this for each file
	curoffset = 0

	#placing the text for the contig, length, numreads and the scale
	midpointx = int((((XOFFSET*len(data)+XOFFSET)+(lengthofseq*len(data)))*XSCALE)/2)
	scene.add(Text((midpointx-(int(len(defline)/2)*30),-75),defline,size=60))
	startingscalex = midpointx+((int(len(defline))*30))
	scene.add(CLine((startingscalex,-75),(startingscalex,-75-(50)), "black")) #scene.add(CLine((startingscalex,-75),(startingscalex,-75-(50)), "black")) change all 50s to whatever
	scene.add(CLine((startingscalex,-75),(startingscalex+(50),-75), "black"))
	scene.add(Text((startingscalex+50,-75),str(50/XSCALE),size=18))
	scene.add(Text((startingscalex,-75-50),str(50/YSCALE),size=18))

	for k in range(len(ordered_d)):
		i = data[ordered_d[k]]
		#setup the space
		textplacementx = int((((ld+XOFFSET+curoffset)*XSCALE)-((0+XOFFSET+curoffset)*XSCALE))/2)+curoffset
		scene.add(Text((textplacementx,((YOFFSET+MAX_Y[0][0])-25)*YSCALE),ordered_d[k][-2:],size=48))
		cumulate = 0
		#he red
		scene.add(CLineWidth(((0+XOFFSET+curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[0][0]))*YSCALE),((ld+XOFFSET+curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[0][0]))*YSCALE), "red",XSCALE))
		cumulate += sum(MAX_Y[0])+YOFFSET
		#il green
		scene.add(CLineWidth(((0+XOFFSET+curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[1][0]))*YSCALE),((ld+XOFFSET+curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[1][0]))*YSCALE), "green",XSCALE))
		cumulate += sum(MAX_Y[1])+YOFFSET
		#so blue
		scene.add(CLineWidth(((0+XOFFSET+curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[2][0]))*YSCALE),((ld+XOFFSET+curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[2][0]))*YSCALE), "blue",XSCALE))
		cumulate += sum(MAX_Y[2])+YOFFSET
		#ace file
		scene.add(CLineWidth(((0+XOFFSET+curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[3][0]))*YSCALE),((ld+XOFFSET+curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[3][0]))*YSCALE), "black",XSCALE))
		count = 0
		for j in i:
			cumulate = 0
			##he
			if int(j[0]) > 0:
				scene.add(CLineWidth(((count + XOFFSET + curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[0][0]))*YSCALE), ((count + XOFFSET + curoffset)*XSCALE,(((cumulate+YOFFSET+MAX_Y[0][0]))-int(j[0]))*YSCALE), "pink",XSCALE))
			if int(j[1]) > 0:
				scene.add(CLineWidth(((count + XOFFSET + curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[0][0]))*YSCALE), ((count + XOFFSET + curoffset)*XSCALE,(int(j[1])+((cumulate+YOFFSET+MAX_Y[0][0])))*YSCALE), "darkred",XSCALE))
			cumulate += sum(MAX_Y[0])+YOFFSET
			##il
			if int(j[2]) > 0:
				scene.add(CLineWidth(((count + XOFFSET + curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[1][0]))*YSCALE), ((count + XOFFSET + curoffset)*XSCALE,(((cumulate+YOFFSET+MAX_Y[1][0]))-int(j[2]))*YSCALE), "lightgreen",XSCALE))
			if int(j[3]) > 0:
				scene.add(CLineWidth(((count + XOFFSET + curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[1][0]))*YSCALE), ((count + XOFFSET + curoffset)*XSCALE,(int(j[3])+((cumulate+YOFFSET+MAX_Y[1][0])))*YSCALE), "darkgreen",XSCALE))
			cumulate += sum(MAX_Y[1])+YOFFSET
			#so
			if int(j[4]) > 0:
				scene.add(CLineWidth(((count + XOFFSET + curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[2][0]))*YSCALE), ((count + XOFFSET + curoffset)*XSCALE,(((cumulate+YOFFSET+MAX_Y[2][0]))-int(j[4]))*YSCALE), "lightblue",XSCALE))
			if int(j[5]) > 0:
				scene.add(CLineWidth(((count + XOFFSET + curoffset)*XSCALE,((cumulate+YOFFSET+MAX_Y[2][0]))*YSCALE), ((count + XOFFSET + curoffset)*XSCALE,(int(j[5])+((cumulate+YOFFSET+MAX_Y[2][0])))*YSCALE), "darkblue",XSCALE))
			cumulate += sum(MAX_Y[2])+YOFFSET
			#ace
			try:
				scene.add(CLineWidth(
					((count + XOFFSET +curoffset)*XSCALE,(cumulate+YOFFSET+MAX_Y[3][0])*YSCALE)
				       ,((count + XOFFSET +curoffset)*XSCALE,(cumulate+YOFFSET+MAX_Y[3][0]-(int(ace_list[count])*ACE_SCALE))*YSCALE)
						, SEQCOLOR[seq[count].upper()],XSCALE))
				num  = 255-int(((100.*float(sliding[count]))/60.)*255)
				scene.add(CLineWidth(
					((count + XOFFSET +curoffset)*XSCALE,(cumulate+YOFFSET+MAX_Y[3][0])*YSCALE),
					((count + XOFFSET +curoffset)*XSCALE,(cumulate+YOFFSET+MAX_Y[3][0]+MAX_Y[3][1])*YSCALE), "rgb("+str(num)+","+str(num)+","+str(num)+")",XSCALE))
			except:
				print "length issue"
			#gc content
			count += 1
		curoffset += lengthofseq + XOFFSET
	scene.write_svg()
	#scene.display()
	#convert svg to pdf
	#command = "/Applications/Inkscape.app/Contents/Resources/bin/inkscape -f "+sys.argv[4]+".svg -T -A "+sys.argv[4]+".pdf"
	command = "inkscape -f "+sys.argv[4]+".svg -P "+sys.argv[4]+".ps"
	os.system(command)
	#scene.display()
	
	
