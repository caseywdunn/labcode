#!/usr/bin/env python	
import sys
import re

Usage = """
Usage:

rename.py index.txt infile.txt > outfile.txt

Takes a tab delimited file with old names in forst column and new names in 
second column. Replaces the corresponding names in infile.txt, and 
prints the output.

Skips lines in index.txt that start with #

"""

if len(sys.argv) < 3:
	print Usage
else:
	index_name = sys.argv[1]
	in_name = sys.argv[2]
	

	index_handle = open(index_name, "r")
	
	names = {}
	
	for line in index_handle:
		line = re.sub("\\#.+", "", line)
		line = line.strip()
		if len(line) < 1:
			continue

		fields = line.split('\t')
		old = fields[0]
		new = fields[1]
		names[old] = new
		
	all = ""
	
	in_handle = open(in_name, "r")
	for line in in_handle:
		all = all + line
	
	keys = names.keys()
	for key in keys:
		#print "replacing", key, "with", names[key]
		all = re.sub(key, names[key], all)
	
	print all
	

		
	
		