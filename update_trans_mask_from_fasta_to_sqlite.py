import sys,sqlite3,os
from Bio import SeqIO
from colors import *

#database = "mollusk.db"

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python update_trans__mask_from_fasta_to_sqlite.py database infile.fasta"
		sys.exit(0)
	database = sys.argv[1]
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()
	handle = open(sys.argv[2],"r")
	for i in SeqIO.parse(handle,"fasta"):
		sid = str(i.id)
		seq = str(i.seq.tostring())
		cur.execute("update translated_seqs set masked_seq = ? where id = ?;",(seq,sid))
	handle.close()
	con.commit()
	con.close()
