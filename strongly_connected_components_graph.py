import itertools
import sys

sys.setrecursionlimit(1500)

def strong_connect(vertex):
    global edges, indices, lowlinks, connected_components, index, stack
    indices[vertex] = index
    lowlinks[vertex] = index
    index += 1
    stack.append(vertex)

    for v, w in (e for e in edges if e[0] == vertex):
        if indices[w] < 0:
            strong_connect(w)
            lowlinks[v] = min(lowlinks[v], lowlinks[w])
        elif w in stack:
            lowlinks[v] = min(lowlinks[v], indices[w])

    if indices[vertex] == lowlinks[vertex]:
        connected_components.append([])
        while stack[-1] != vertex:
            connected_components[-1].append(stack.pop())
        connected_components[-1].append(stack.pop())

def run_example():
	edges = [('A', 'B'), ('B', 'C'), ('C', 'D'), ('D', 'E'), ('E', 'A'), ('A', 'E'), ('C', 'A'), ('C', 'E'), ('D', 'F'), ('F', 'B'), ('E', 'F')]
	vertices = set(v for v in itertools.chain(*edges))
	indices = dict((v, -1) for v in vertices)
	lowlinks = indices.copy()
	connected_components = []
	
	index = 0
	stack = []
	for v in vertices:
	    if indices[v] < 0:
	        strong_connect(v)

	print(connected_components)

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python strongly_connected_components_graph.py infile outfile"
		sys.exit(0)
	infile = open(sys.argv[1],"r")
	edges = []
	weight_col = 2
	cutoff = 20
	print "reading graph"
	for i in infile:
		spls = i.strip().split()
		edgeA = spls[0]
		edgeB = spls[1]
		weight = float(spls[weight_col])
		if weight > cutoff:
			edges.append((edgeA,edgeB))
	infile.close()
	print "calculating vertices"
	vertices = set(v for v in itertools.chain(*edges))
	print "calculating indices"
	indices = dict((v,-1) for v in vertices)
	lowlinks = indices.copy()
	connected_components = []
	index = 0
	stack = []
	print "calculating components"
	count = 0
	outfile = open(sys.argv[2],"w")
	for v in vertices:
		print count
		if indices[v] < 0:
			strong_connect(v)
		count += 1
	for i in connected_components:
		outfile.write(" ".join(i)+"\n")
	outfile.close()
