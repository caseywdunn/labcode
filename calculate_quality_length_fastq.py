from Bio import SeqIO

import sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python calculate_qual_length_fastq.py input output"
		sys.exit(0)

	sample = 100000 #sample only every
	numofseqs = 0
	seq_quals = []
	infile = open(sys.argv[1],"r")
	outfile = open(sys.argv[2],"w")
	#for i in SeqIO.parse(infile,"fastq-solexa"):
	for i in SeqIO.parse(infile,"fastq-illumina"):
		if numofseqs % sample == 0:
			if len(seq_quals) == 0:
				#seq_quals = [0] * len(i.letter_annotations['solexa_quality'])
				seq_quals = [0] * len(i.letter_annotations['phred_quality'])
			for j in range(len(i.letter_annotations['phred_quality'])):
				seq_quals[j] += i.letter_annotations['phred_quality'][j]
				outfile.write(str(i.letter_annotations['phred_quality'][j])+" ")
			#for j in range(len(i.letter_annotations['solexa_quality'])):
			#	seq_quals[j] += i.letter_annotations['solexa_quality'][j]
			#	outfile.write(str(i.letter_annotations['solexa_quality'][j])+" ")
			outfile.write("\n")
			outfile.flush()
		#print seq_quals
		#sys.exit(0)
		numofseqs += 1
		if numofseqs % sample == 0:
			print numofseqs
	infile.close()
	outfile.close()
	
	
	#outfile = open(sys.argv[2],"w")
	#for i in range(len(seq_quals)):
		#seq_quals[i] /= float(numofseqs)
		#outfile.write(str(seq_quals[i])+" ")
	#outfile.write("\n")
	#print seq_quals
	#outfile.close()
