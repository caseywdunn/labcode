"""
Calculates the likelihood of a tree, given a sequence alignment.

"""

import sys,os
from Bio import SeqIO
import numpy, scipy

def build_GTR_rate_matrix( piA, piC, piG, a, b, c, d, e, f, mu ):
	""" Calculates a GTR rate matrix given basic parameters:


		A		C		G		T
	A	-		piC*a	piG*b	piT*c
	C	piA*a	-		piG*d	piT*e
	G	piA*b	piC*d	-		piT*f
	T	piA*c	piC*e	piG*f	-
	
	The whole matrix is multiplied by mu
	
	the diagonal values are chosen so that each row sums to 0

	"""

	piT = 1 - piA - piC -piG

	rate_matrix = array(
	[
	[ -(piC*a + piG*b + piT*c), piC*a, piG*b, piT*c],
	[ piA*a, -(piA*a + piG*d + piT*e), piG*d, piT*e],
	[ piA*b, piC*d, -(piA*b + piC*d + piT*f), piT*f],
	[ piA*c, piC*e, piG*f, -(piA*c + piC*e + piG*f)]
	]
	)

	rate_matrix = rate_matrix * mu

	return rate_matrix

def build_HKY85_rate_matrix(  piA, piC, piG, beta, kappa ):
	return build_GTR_rate_matrix( piA, piC, piG, beta, beta * kappa, beta, beta, beta * kappa, beta, 1 )

def transition_matrix( Q, t ):
	"""Calcualtes the transition probability matrix given a rate matrix Q and time t

	"""

	return scipy.linalg.expm( Q * t )


if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python tree.tre alignment.nex"
		sys.exit(0)
	treefile= open(sys.argv[1],"r")
	alnfile = open(sys.argv[2],"r")
