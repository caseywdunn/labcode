import sys,sqlite3,os
from colors import *
from Bio import SeqIO


outgroups = ["Lingula_anatina","Schmidtea_mediterranea", "Chaetopterus_variopedatus","Terebratalia_transversa","Cerebratulus_lacteus", "Carinoma_mutabilis","Capitella","Helobdella_robusta"]

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python add_one_name_to_sqlite.py name database"
		sys.exit(0)
	database = sys.argv[2]
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	species_name = sys.argv[1]
	cur = con.cursor()
	cur.execute("SELECT * FROM species_names where name = '"+species_name+"';")
	a = cur.fetchall()
	if len(a) == 0:
	       	print BLUE,"adding ",RESET,GREEN,species_name,RESET
	       	ingroup = species_name not in outgroups
	       	cur.execute("INSERT INTO species_names(name,ingroup) VALUES (?,?)",(species_name,ingroup))
	       	con.commit()
	else:
	       	print RED,species_name,"already there",RESET
	con.close()
