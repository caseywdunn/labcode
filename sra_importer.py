#!/usr/bin/env python

import os
import sys
import lxml.etree
import wget
import shutil
import subprocess
import glob

from collections import defaultdict
from Bio import Entrez

Entrez.email = raw_input("To query NCBI, you need to provide an email: ")


def get_ids(numb):
	handle = Entrez.esearch(db = 'sra', MaxRet = 100, term = numb)
	record = Entrez.read(handle)
	ids = record['IdList']
	return ids

def get_xmls(ids):
	id_xmls = []
	for i in ids:
		handle = Entrez.efetch(db = 'sra', id = i)
		id_xmls.append(handle)
	return id_xmls

def get_metadata(id_xmls):	
	accn_metadata = {} # can be used to get more metadata per accession number
	for x in id_xmls:
		record = lxml.etree.parse(x)
		accession = ''.join(record.xpath('//EXPERIMENT_PACKAGE_SET/EXPERIMENT_PACKAGE/EXPERIMENT/@accession'))
		run = record.xpath('//EXPERIMENT_PACKAGE_SET/EXPERIMENT_PACKAGE/RUN_SET/RUN/@accession')
		lib_source = record.xpath('//EXPERIMENT_PACKAGE_SET/EXPERIMENT_PACKAGE/EXPERIMENT/DESIGN/LIBRARY_DESCRIPTOR/LIBRARY_SOURCE')[0]
		lib_strategy = record.xpath('//EXPERIMENT_PACKAGE_SET/EXPERIMENT_PACKAGE/EXPERIMENT/DESIGN/LIBRARY_DESCRIPTOR/LIBRARY_STRATEGY')[0]
		platform = record.xpath('//EXPERIMENT_PACKAGE_SET/EXPERIMENT_PACKAGE/EXPERIMENT/PLATFORM/ILLUMINA/INSTRUMENT_MODEL')[0]
		taxon_id = record.xpath('//EXPERIMENT_PACKAGE_SET/EXPERIMENT_PACKAGE/SAMPLE/SAMPLE_NAME/TAXON_ID')[0]
		sci_name = record.xpath('//EXPERIMENT_PACKAGE_SET/EXPERIMENT_PACKAGE/SAMPLE/SAMPLE_NAME/SCIENTIFIC_NAME')[0]
		#print list(lib_source.itertext())
		print ''.join(lib_source.itertext())
		print ''.join(lib_strategy.itertext())
		print ''.join(platform.itertext())
		print ''.join(taxon_id.itertext())
		print ''.join(sci_name.itertext())
		accn_metadata[accession] = run
	return accn_metadata

def get_url(accn_metadata):
	accn_url = defaultdict(list)
	url_template = 'ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByRun/sra/{first3}/{first6}/{all}/{all}.sra'
	for k, r in accn_metadata.iteritems():
		if len(accn_metadata[k]) == 1:
			run =''.join(accn_metadata[k])
			accn_url[k].append(url_template.format(first3=run[:3], first6=run[:6], all = run))
		else:
			for r in accn_metadata[k]:
				accn_url[k].append(url_template.format(first3=r[:3], first6=r[:6], all = r))
	return accn_url

def get_data(sra_numb):
	all_ids = get_ids(sra_numb)
	all_xmls = get_xmls(all_ids)
	all_accn_runs = get_metadata(all_xmls)
	all_accn_urls = get_url(all_accn_runs)
	
	for k, v in all_accn_urls.iteritems():
		sra_dir = os.path.join(os.getcwd(), k)
		os.makedirs(sra_dir)
		for i in v:
			print "Dowloading '%s' for accession %s\n" % (i, k)
			sra_file = wget.download(i)
			print "\n"
			shutil.move(sra_file, sra_dir)
			#for f in glob.glob('./*.sra'):
			#	subprocess.call(["fastq-dump", "--split-files", f])
				# if file loose pair information in headers, run
				#awk '/^@/{print "@"$2"/1"; getline; print; getline; print "+"; getline; print; }' f_1.fastq > f_1.fastq &
				#awk '/^@/{print "@"$2"/2"; getline; print; getline; print "+"; getline; print; }' f_2.fastq > f_2.fastq &
			#for q in glob.glob('./*.fastq'):
			#	subprocess.call(["gzip", q])


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "usage: sra_importer <SRAnumber>"
		sys.exit(0)
	get_data(str(sys.argv[1]))
