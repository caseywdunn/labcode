import sys,os

"""
this will take a file that just has the line from the phylobayes param file
that has the membership of each character to each partition

it also takes the phylip input file and will output a sorted phylip file for each model and a model 
file to run in raxml with protgtr model
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python process_phylobayes_components_to_raxml.py infile.param infile.phy"
		sys.exit(0)
	infile = open(sys.argv[2],"r")
	numsites = int(infile.readline().strip().split()[1])
	infile.close()
	inpb = open(sys.argv[1],"r")
	mod = ""
	count = 0
	for i in inpb:
		spls = i.strip().split()
		#assumes that the assignment vector is the second one of this size
		if len(spls) == numsites:
			mod = spls
			if count == 2:
				break
			count += 1
	inpb.close()
	seq_models = {}
	max_model = 0
	count = 0
	for i in mod:
		n = int(i)
		if n not in seq_models:
			seq_models[n] = []
		if n > max_model:
			max_model = n
		seq_models[n].append(count)
		count += 1
	infile = open(sys.argv[2],"r")
	first = True
	outfile = open(sys.argv[1]+".phy","w")
	outfilem = open(sys.argv[1]+".model","w")
	for i in infile:
		if first == True or len(i) < 2:
			if first == True:
				outfile.write(i)
			first = False
			continue
		spls = i.split("\t")
		seqstr = ""
		for j in range(max_model+1):
			for k in seq_models[j]:
				seqstr += spls[1][k]
		outfile.write(spls[0]+"\t"+seqstr)
		outfile.write("\n")
	current = 1
	first = True
	for j in range(max_model+1):
		outfilem.write("GTR,"+str(j)+"="+str(current)+"-"+str(current+len(seq_models[j])-1)+"\n")
		current += len(seq_models[j])
	infile.close()
	outfile.close()
	outfilem.close()
	
