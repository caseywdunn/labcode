from Bio import SeqIO
from numpy import mean
import sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python trim_illumina_fastq_based_on_quality_pe.py pe_in1 pe_in2"
		sys.exit(0)
	count = 0
	outfile = open(sys.argv[1]+".thinned","a")
	outfile2 = open(sys.argv[2]+".thinned","a")
	infile = open(sys.argv[1],"r")
	infile2 = open(sys.argv[2],"r")
	secondg = SeqIO.parse(infile2,"fastq-illumina")
	for first in SeqIO.parse(infile,"fastq-illumina"):
		second = secondg.next()
		seq_quals1 = first.letter_annotations['phred_quality']
		seq_quals2 = second.letter_annotations['phred_quality']
		if mean(seq_quals1) > 32 and mean(seq_quals2) > 32:
			seqs = [first]
			seqs2 = [second]
			SeqIO.write(seqs,outfile,"fastq-illumina")
			SeqIO.write(seqs2,outfile2,"fastq-illumina")
		count += 1
		if count % 10000 == 0:
			print count	
	infile.close()
	infile2.close()
