import os,sys

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "usage: python mafft_align_folder.py indir"
    DIR = sys.argv[1]
    for i in os.listdir(DIR):
        if ".fasta" in i:
            print i
            os.system("mafft --auto --amino --thread 16 "+DIR+"/"+i+" > "+DIR+"/aligned/"+i.split(".fasta")[0]+".aln")
