import sys,os
import subprocess
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastnCommandline
from Bio.Blast import NCBIXML

DEBUG = False

def blast2(seq1,dbf):
	in1 = open("tseq1","w")
	in1.write(">"+seq1.id+"\n"+seq1.seq.tostring().upper()+"\n")
	in1.close()
	cline = NcbiblastnCommandline(query="tseq1", db=dbf, evalue=0.001, outfmt=5,out="temp.xml",perc_identity="95")
	#print cline
	return_code = subprocess.call(str(cline), shell=(sys.platform!="win32"))
	#sys.exit()

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python exclude_subset_contigs.py infile outfile"
		sys.exit(0)
	#create a blastDB for the infile
	print "making db"
	cmd = "makeblastdb -in "+sys.argv[1]+" -dbtype nucl -out "+ sys.argv[1]+".db"
	os.system(cmd)
	print "finished making db"
	#open infile
	infile = open(sys.argv[1],"rU")
	keep = []
	remove = []
	allseqs = []
	allseqs_dict = {}
	for i in SeqIO.parse(infile,"fasta"):
		allseqs.append(i)
		allseqs_dict[i.id] = i
	allseqs.reverse()
	count = 0
	hit = 0
	while len(allseqs) > 0:
		i = allseqs.pop()
		match = False
		if len(i.seq.tostring()) < 6:
			match = True
		else:
			blast2(i,sys.argv[1]+".db")
			result_handle = open("temp.xml","rU")
			for j in NCBIXML.parse(result_handle):
				if len(j.alignments) > 1:
					if DEBUG:
						print "here"
					for k in j.alignments:
						title = k.title.split("|")[2].split(" ")[1] # Name of the subject
						if title == i.id:
							if DEBUG:
								print "broke"
							continue
						else:
							for l in k.hsps:
								#print l
								qulen = l.identities/float(len(i.seq))
								sublen = l.identities/float(len(allseqs_dict[title].seq))
								if DEBUG:
									print i.id,title,len(i.seq),len(allseqs_dict[title].seq),qulen,sublen
								if qulen == 1.0:
									match = True
									if DEBUG:
										print "matched"
									break
								elif sublen == 1.0:
									if allseqs_dict[title] in allseqs:
										allseqs.remove(allseqs_dict[title])
										remove.append(allseqs_dict[title])
										if DEBUG:
											for m in keep:
												print m.id
											print "---"
											for m in remove:
												print m.id
											print "==="
										#sys.exit(0)
						if match == True:
							break
					if match == True:
						break
					#sys.exit(0)
		if match == False:
			keep.append(i)
		else:
			hit += 1
			remove.append(i)
		if DEBUG:
			if hit > 2:
				print "+++"
				for k in keep:
					print k.id
				print "---"
				for k in remove:
					print k.id
				print "==="
				sys.exit(0)
		count += 1
		if count%100 == 0:
			print count,len(keep),len(allseqs)
	outfile = sys.argv[2]
	output_handle = open(outfile, "w")
	SeqIO.write(keep, output_handle, "fasta")
	output_handle.close()
	output_handle = open(outfile+".removed", "w")
	SeqIO.write(remove, output_handle, "fasta")
	output_handle.close()
