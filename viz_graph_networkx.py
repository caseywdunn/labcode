import sys,os
import networkx as nx
import matplotlib.pyplot as plt

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python viz_graph_network.py ingraph outfile.png"
		sys.exit(0)
	infile = open(sys.argv[1],"r")
	graph = nx.Graph()
	nodes = []
	for i in infile:
		spls = i.strip().split("\t")
		if spls[0] not in nodes:
			nodes.append(spls[0])
			graph.add_node(spls[0])
		if spls[1] not in nodes:
			nodes.append(spls[1])
			graph.add_node(spls[1])
		graph.add_edge(spls[0],spls[1],weight = float(spls[2]))
	infile.close()
	nodelist = graph.nodes()
	colors = []
	for i in nodelist:
		if "high_" in i:
			colors.append('r')
		if "first_3_" in i:
			colors.append('b')
		if "454_" in i:
			colors.append('g')

	#nx.draw_spring(graph,node_color=colors,node_size=100,font_size=8)
	nx.draw_graphviz(graph,node_color=colors,node_size=100,font_size=8)
	F = plt.gcf()
	F.set_size_inches(28.5,28.5)
	plt.savefig(sys.argv[2],dpi=100)
