from Bio import SeqIO
import sys,os

"""
this should report how well represented the taxa are in the clusters

"""

file_ending = ".fasta"

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python get_taxa_representation_in_clusters.py DIR outfile"
		sys.exit(0)
	path = sys.argv[1]
	taxa = {}
	for i in os.listdir(path):
		print i
		if i[-len(file_ending):] == file_ending:
			handle = open(path+"/"+i,"rU")
			for j in SeqIO.parse(handle,"fasta"):
				lab = j.id.split("@")[0]
				if lab not in taxa:
					taxa[lab] = 0
				taxa[lab] += 1
			handle.close()
	outfile = sys.argv[2]
	out = open(outfile,"w")
	for i in taxa:
		out.write(str(i)+"\t"+str(taxa[i])+"\n")
	out.close()
