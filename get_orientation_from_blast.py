import sys
from Bio.Blast import NCBIXML
from Bio import SeqIO

"""
this will take the fasta and the blast xml
and keep only sequences that were in the blast
and then output them in the right orientation
in the outfile
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python get_orientation_from_blast.py infile.fsa infile.xml outfile"
		sys.exit(0)
	
	keepids = []
	orientation = {} #key is the id and value is the orientation + is forward and - is reverse
	inxml = open(sys.argv[2],"r")
	for k in NCBIXML.parse(inxml):
		qutitle = str(k.query).split(" ")[0]
		for m in k.alignments:
			hsp = m.hsps[0] #only look at the first
			print qutitle
			keepids.append(qutitle)
			if int(hsp.frame[0]) >= 0:
				orientation[qutitle] = "+"
			else:
				orientation[qutitle] = "-"
			break#only look at the first
	inxml.close()
	
	infasta = open(sys.argv[1],"r")
	outfile = open(sys.argv[3],"w")
	for i in SeqIO.parse(infasta,"fasta"):
		if i.id in keepids:
			if orientation[i.id] == "+":
				outfile.write(">"+i.id+"\n"+str(i.seq)+"\n")
			else:
				outfile.write(">"+i.id+"\n"+str(i.seq.reverse_complement())+"\n")
	infasta.close()
	outfile.close()
