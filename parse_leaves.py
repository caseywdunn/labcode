#!/usr/bin/env python

import argparse
from datetime import datetime
import time

parser = argparse.ArgumentParser(description='Reformat leaf phenology data.')
parser.add_argument('output', metavar='O', type=str)
parser.add_argument('files', metavar='F', type=str, nargs='+',
                   help='the input files to be processed')

args = parser.parse_args()

print ( args.output )

OutFile = open(args.output, 'w')

for input_name in args.files:

	print ( " Processing file: " + input_name )
	InFile = open(input_name, 'r')

	n = 0

	header = list()
	species = ""
	plant = ""

	OutFile.write("species\tplant\tleaf\tcomment\tdata\tpresent\n")

	for line in InFile:
		line = line.rstrip()
		fields = line.split(',')
		if (n == 0):
			id = fields[0]
			
			# Assume that species is the first two words, and everything else is the plant id
			words = id.split(' ')
			species = ' '.join(words[0:2])
			plant = ' '.join(words[2:])
			print ( " species: " + species )
			print ( " plant: " + plant )
			
		elif (n == 2):
			header = fields
			print ( " header elements: " + ",".join(header) ) 
		elif (n > 2):
			if len( fields ) > 0:
				leaf = fields[0]
				comment = header[1] + ":" + fields[1]
				x = 2
				for datum in fields[2:]:
					# The date is in header[x]
					# date_object = datetime.strptime( header[x], '%d-%b')
					# posix_date = time.mktime(date_object.timetuple())
				
					s = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n".format( species,  plant, leaf, comment, header[x], datum)
					OutFile.write( s )
					x = x +1
		n = n +1
		
	InFile.close()
	
OutFile.close()