#!/usr/bin/perl -s
use strict;
my $inputfile = shift;

open (IFILE, $inputfile) or die "can't open $inputfile";

my %hgH; #hg keys, each value is a hash with mcl keys giving the number of sequences with that hg and mcl
my %mclH; #mcl keys, each value is a hash with hg keys giving the number of sequences with that mcl and hg

my %histo;	#Hash of arrays of sequences for making a histogram of the size of one-to-one clusters

print"\t\t\thg w/ one mcl\tmcl w/one hg\tone to one\nI\tnhg\tnmcl\tn_grp\tn_seq\tn_grp\tn_seq\tn_grp\tn_seq\n";

my $inflation = -1;
my $linenum = 0;
while (my $line = <IFILE>) {
	$linenum ++;
	
	if ($line =~ m/^[^\d]/) { next; }
	
	chomp $line;
	my @fields = split /\t/, $line;
	my $I = $fields[0];
	my $mcl = $fields[1];
	my $hg = $fields[2];

	if ($inflation != $I && $inflation > 0){
		#A new value has been detected, time to do analyses
		summarize();
	
		
		#Clear the hashes
		#print "Resetting at line $linenum\n";
		%hgH = ();
		%mclH = ();

	}
	
	$inflation = $I;
	
	$hgH {$hg}{$mcl} ++;
	$mclH {$mcl}{$hg} ++;
}
summarize(); #Summarize one last time





sub summarize {
		
	my $numhg = keys (%hgH);
	my $nummcl = keys (%mclH);
	
	my %hgnummcl;
	my $numhgseqonetoone;
	my $numhggrponetoone;

	my %hghasonemcl; #hash of hgs that is 1 if there are no more than one mcl in it
	my %badmcl; #hash of mcls that are found in hgs with more than one mcl
	
	foreach my $hgv (sort keys %hgH){
		my %mcls = %{$hgH{$hgv}};
		my @mclv = sort keys %mcls;
		my $nmcl = @mclv;
		$hgnummcl{$hgv} = $nmcl;
		my $nseqs;
		foreach my $m (@mclv){
			$nseqs += $mcls{$m};
		}
		
		if ($nmcl == 1){
			$numhggrponetoone ++;
			$numhgseqonetoone += $nseqs;
			$hghasonemcl{$mclv[0]} = 1; #############
		}
		else{
			foreach my $m (@mclv){
				$badmcl{$m}++;
			}
		}
		#print "$inflation\t$hgv\t$nmcl\t$nseqs\n";
	}
	
	my %mclnumhg;	# hash with key mcl and value number of mcl groups with sequences in that mcl
	my $nummclseqonetoone;
	my $nummclgrponetoone;
	
	my $numgrponetoone;
	my $numseqonetoone;
	
	foreach my $mclv (sort keys %mclH){
		my %hgs = %{$mclH{$mclv}};
		my @hgv = sort keys %hgs;
		my $nhg = @hgv;
		$mclnumhg{$mclv} = $nhg;
		my $nseqs;
		foreach my $h (@hgv){
			$nseqs += $hgs{$h};
		}
		
		if ($nhg == 1){
			#if ($hghasonemcl{$mclv} > 0){
			if ($badmcl{$mclv} < 1){
				$numgrponetoone ++;
				$numseqonetoone += $nseqs;
				
				push @{$histo{$inflation}}, $nseqs;
			}
			$nummclgrponetoone ++;
			$nummclseqonetoone += $nseqs;
		}
		
		#print "$inflation\t$mclv\t$nhg\t$nseqs\n";
	}
	
	print "$inflation\t$numhg\t$nummcl\t$numhggrponetoone\t$numhgseqonetoone\t$nummclgrponetoone\t$nummclseqonetoone\t$numgrponetoone\t$numseqonetoone\n";
	

	
}

print "\n";
print "\n";

foreach my $inf (sort keys %histo){
	my @sizes = @{$histo{$inf}};
	
	print "$inf";
	foreach my $size (@sizes){
		print "\t$size";
	}
	
	print "\n";
}