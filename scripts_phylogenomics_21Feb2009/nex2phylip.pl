#!/usr/bin/perl -s
use strict;

my $inputfile = shift;
my $basename = $inputfile;
$basename =~ s/\.nex$//;

my $phylip_name = $basename . ".phy";
my $q_name = $basename . ".q";

open (IFILE, $inputfile) or die "can't open $inputfile";
open (PFILE, ">$phylip_name") or die "can't open $phylip_name";
open (QFILE, ">$q_name") or die "can't open $q_name";

my $ntaxa = 0;
my $nchar = 0;
my $flag = 0;

my %data;
my $position = 0;
my $lastlength = 0;

while (my $line = <IFILE>) {
	chomp $line;
	if ($flag < 1){
		if ($line =~ m/dimensions ntax = (\d+) nchar = (\d+)/){
			$ntaxa = $1;
			$nchar = $2;
			print"$ntaxa $nchar\n";
		}
		elsif ($line =~ m/matrix/){
			$flag = 1;
			print "Encountered matrix start\n";
		}
	}
	elsif ($line =~ m/^\s*([A-Za-z]+_[A-Za-z]+)\s+([A-Za-z-\?]+)/){
		my $taxon = $1;
		my $seq = $2;
		#print "  $taxon\n";
		$lastlength = length ($seq);
		$data{$taxon} .= $seq;
	}
	elsif ($line =~ m/\[Source of sequence data: (.+)\]/){
		my $name = $1;
		$position += $lastlength;
		#print "position = $position, lastlength = $lastlength\n";
		if ($position>0){
			print QFILE "$position\n";
		}
		my $start = $position +1;
		#WAG, gene3 = 801-1000
		print QFILE "WAG, $name = $start-";
	}
	elsif ($line =~ m/;/){
		print "Encountered matrix end\n";
		last;
	}
}

$position += $lastlength;
print QFILE "$position\n";

foreach my $key (keys %data){
	my $seq = $data{$key};
	my $length = length($seq);
	if ($length != $nchar){
		die ("$key has $length characters, which does not equal the matrix length of $nchar\n");
	}
}

print PFILE "$ntaxa $nchar\n";

foreach my $key (sort keys %data){
	my $seq = $data{$key};
	my $length = length($seq);
	if ($length != $nchar){
		die ("$key has $length characters, which does not equal the matrix length of $nchar\n");
	}
	print PFILE "$key  $seq\n";
}

