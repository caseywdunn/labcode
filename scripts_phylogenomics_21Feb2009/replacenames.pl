#! /usr/bin/perl -pi.orig


$namefile="/Users/cdunn/est_solutions/analyses/protol/names_to_fix.txt";

open (NAMES, $namefile) or die "can't open $namefile";

# Loop through all the names to replace
while ($line = <NAMES>) {
	chomp $line;
	@fields = split /\t/, $line;

	$old = $fields[0];
	$new = $fields[1];


	s/$old/$new/g;
}

close NAMES;

