#!/usr/bin/perl
#Will Jeck 2007
#Verified Consensus Assembly by K-mer Extension

#NAME
#	VCAKE v1.0 William Jeck, May 2007
#   SSAKE v1.0  Rene Warren, October 2006

#SYNOPSIS
#   Progressive clustering of millions of short DNA sequences by Kmer extension

#LICENSE
#   Based on SSake, Copyright (c) 2006 Canada's Michael Smith Genome Science Centre.  All rights reserved.
#   VCAKE Copyright (c) 2007 University of North Carolina at Chapel Hill. All rights Reserved.

#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

use strict;
use Getopt::Std;
use vars qw( %opt );

### Get command line options ###

&getopt('bcefklmnoqstvx', \%opt);

### If insufficient options, print usage###

if(! ($opt{'f'} && $opt{'b'} && $opt{'s'} && $opt{'k'}) ) {
   print "Usage: $0\n";
   print "-s <string> suffix (contigs will be output to 'file.contigs.suffix')\n";
   print "-b <file.fasta> backbone to seed contigs\n";
   print "-c <number> ratio of most represented base required for extension (default=0.6)\n";
   print "-e <number> minimum overlap allowed for assembling k-mers with a single error (optional, default=inf => no errors allowed, reccomended: e = 22 or more)\n";
   print "-f <file.fasta> unassembled reads\n";
   print "-k <number> length of reads in -f file (required)\n";
   print "-n <number> standard minimum overlap for getting coverage seqs (default = 18)\n";
   print "-t <number> number of reads before tripping to the liberal overlap requirement (default=5)\n";
   print "-m <number> minimum overlap allowed for assembling k-mers if the liberal overlap trip is reached (default = 16)>\n";
   print "-o <number> minimum contig size to print to .contig file - speeds up assembly (optional, default=0)\n";
   print "-q <number> quit after a set number of contigs (optional, default=never)\n";
   print "-v <number> maximum number of disagreements with the majority base call allowed before termination (prevents extension through repeat elements) (default=3)\n";
   print "-x <number> maximum number of duplicated reads accepted (avoids assembling repeated elements) (default=infinite, recc -> 10-20x expected coverage)\n";
   die "-l <log file> (optional)";
}

### Set parameters ###

my  $quitCount = 0;     #-q parameter: Quits the program after generating $quitCount contigs
our $minConserv = 18;   #-n parameter: automatically gets all kmer overlaps up to this many bases
our $tripToLib = 5;     #-t parameter: Number of required reads required. If not met, gets reads up to $minLib bases overlap
our $minWerr = 0;       #-e parameter: Minimum overlap for getting reads with a single error beyond the first 11 bases of overlap
our $minLib = 16;       #-m parameter: Minimum overlap for getting reads with no error after -n has produced insufficient reads
our $consensusThresh = 0.60;  #-c paramter: 'majority rule' ratio threshold for adding the most represnted base 
our $minVerif = 4;      #-v parameter: Number of 'disagreeing' base calls at a given position before it is considered a duplicate element rather than error
our $minContigLeng = 0; #-o parameter: minimum contig size to output.

my $prefile = $opt{'b'};
if ($opt{'c'}) { $consensusThresh = $opt{'c'};}
if ($opt{'e'}) { $minWerr = $opt{'e'};}
my $file = $opt{'f'};
our $TRACELENGTH = $opt{'k'};
my $logfile = $opt{'l'};
if ($opt{'m'}) { $minLib = $opt{'m'}; }
if ($opt{'n'}) { $minConserv = $opt{'n'}; }
if ($opt{'o'}) { $minContigLeng = $opt{'o'};}
my $suffix = $opt{'s'};
if ($opt{'q'}) { $quitCount = $opt{'q'};}
if ($opt{'t'}) { $tripToLib = $opt{'t'};}
if ($opt{'v'}) { $minVerif = $opt{'v'};}
my $coverage = $opt{'x'};

### test file ###
if(! -e $file){
   die "Invalid file: $file\n";
}
if(! -e $prefile){
   die "Invalid file: $prefile\n";
}

###Load Data from Fasta###

print "Reading Fasta\n";

my $set = &readSet($prefile);
my $bin = &readBin($file, $TRACELENGTH);

### Begin Assembly###
print "Beginning Assembly\n";

our $tig_count=0;  #counts number of contigs

my $contigFile = "$file.contigs.$suffix"; #set the filename for output

##open outfile and log file. Log file can be useful to troubleshoot assembly parameters, but produces massive output
open (OUT, ">$contigFile") || die "can write to $contigFile. Exiting.\n";
open (LOG, ">$logfile") || print "No log file specified or failure to open log file.\n";

eval{
ASSEMBLY:
foreach my $seq (sort {$set->{$b}<=>$set->{$a}} keys %$set){   #cycle through each 'seed' sequence (usually the same as read sequences). Starts with high coverage areas
									                      
	my $orig_mer = length($seq); 
	
	if( ($opt{'x'}) and (defined $set->{$seq}) and (my $numReads >= ($coverage)) )  { #Removes reads with excessive representation. Prevents extension of repeated elements.
		&DELETEREAD($bin, $set, $seq);
	}																						
	
	if(defined $set->{$seq}){    #ensures an identical sequence has not been used to construct another contig
			
		&DELETEREAD($bin, $set, $seq);	# Remove the seed sequence from further consideration
		
		print "starting $tig_count\n";
		print LOG "starting $tig_count\n";
		
		my $seq3pExtend = &EXTEND($bin, $set, $seq);  # Extend the 3' direction as far as possible
		$seq3pExtend = &REVCOMP($seq3pExtend);        # Reverse compliment the extended contig
		
		my $seq5p3pExtend = &EXTEND($bin, $set, $seq3pExtend); # Extend the 3' direction (former 5') as far as possible
		if (length($seq5p3pExtend) >= $minContigLeng) { print OUT ">Contig_$tig_count length:" . length($seq5p3pExtend) . "\n$seq5p3pExtend\n";}
		print length($seq5p3pExtend) . "\n\n";
		print LOG length($seq5p3pExtend) . "\n\n";
		
		$tig_count++;
		
		if (($quitCount) && ($tig_count >= $quitCount)) {last;} #test that 
	}
}	
};
close OUT;
close LOG;

exit;

#-------------------
#Deletes a given sequence from bin and set hash tables

sub DELETEREAD {
	my ($binPt,$setPt,$sequence2rem) = @_; 

	my @o=split(//,$sequence2rem);

	my $comp_seq = &REVCOMP($sequence2rem);
	my @c=split(//,$comp_seq);
													
	#remove kmer from hash table and prefix tree
	delete $binPt->{$o[0]}{$o[1]}{$o[2]}{$o[3]}{$o[4]}{$o[5]}{$o[6]}{$o[7]}{$o[8]}{$o[9]}{$o[10]}{$sequence2rem};
	delete $binPt->{$c[0]}{$c[1]}{$c[2]}{$c[3]}{$c[4]}{$c[5]}{$c[6]}{$c[7]}{$c[8]}{$c[9]}{$c[10]}{$comp_seq};
	delete $setPt->{$sequence2rem};
	delete $setPt->{$comp_seq};
}
	
#-------------------	
#Reads in all seed sequences from a multi-fasta file and puts them into the 'set' hash table. Key is the sequence, value is the number of occurences

sub readSet{
   my $file = shift;

   my $set;
   my $ctrd = 0;

   print "Now reading SET\n";
   open(IN,$file) || die "could not open $file\n";
   while(<IN>){  
   	  
      chomp;
      if(/^([ACGT]*)$/i){     #get only nucleotide  (case insensitive)
         $ctrd++;
         my $tester = $ctrd / 10000;
   	 print "$ctrd\n" if ($tester == int($tester)); #notify user of progress
   
         my $seq=$1;
         my $orig=uc($seq);  #all seqs converted to upper case

         if ($orig ne ''){ 

            my @sf=split(//,$orig);
            $set->{$orig}++;  #add the sequence to the hash or increment the number of occurences by one
         }
      }
   }
   print LOG "\tTotal sequences read: $ctrd\n";
   print LOG "\tNumber of unique sequences: " . keys( %$set ) . "\n";

   close IN;
   return $set;
}

#----------------
#Reads in the short read sequences stores the sequence and its reverse complement in the 'bin' hash.
#The keys to the bin are a tree, beginning with 11 keys representing the first 11 bases of the sequence, then a 12th key 
#representing the entire sequence, and the value is the number of times that sequence or its reverse complement is represented
#in the multi-fasta file containing the reads.

sub readBin{
   my $file = shift;
   my $tossout = shift;

   my $bin;
   my $ctrd = 0;
   
   print "Loading bin\n";
   open(IN,$file) || die "could not open $file\n";
   while(<IN>){  
   	  
      chomp;
      if(/^([ACGT]*)$/i){
         $ctrd++;
         my $tester = $ctrd / 10000;
   	     print "$ctrd\n" if ($tester == int($tester));
   
         my $seq=$1;
         next if (length($seq) < $tossout);
         my $orig=uc($seq);
         if ($tossout) { $orig = substr($orig,0,$tossout); }  

         if ($orig ne ''){ 

            my @sf=split(//,$orig);
            my $rc = REVCOMP($orig);  
            my @sr = split(//,$rc);

            $bin->{$sf[0]}{$sf[1]}{$sf[2]}{$sf[3]}{$sf[4]}{$sf[5]}{$sf[6]}{$sf[7]}{$sf[8]}{$sf[9]}{$sf[10]}{$orig}++;
            $bin->{$sr[0]}{$sr[1]}{$sr[2]}{$sr[3]}{$sr[4]}{$sr[5]}{$sr[6]}{$sr[7]}{$sr[8]}{$sr[9]}{$sr[10]}{$rc}++;
         }
      }
   }
   print LOG "\tTotal sequences read: $ctrd\n";
   print LOG "\tNumber of unique sequences: " . keys( %$set ) . "\n";

   close IN;
   return $bin;
}

#----------------
sub EXTEND {
	my ($bin, $set, $seq) = @_;

	my %usedReads = (); #usedReads is an odd addition used to avoid some very rare situations in which sequences will extend indefinitely.
                            #This can occur within certain kinds of repeat regions with a large number of short repeats.

	###### Delete anthing up to the relevant overlap region. Removes possibility of repitition #######
	my @matches2Delete;
	my @dummy;
	for (my $index = 0; $index <= (length($seq) - $TRACELENGTH); $index++) {  
		my $subsubseq = substr($seq, $index);			
		my $numFound = GETOVERLAPSEQS($bin, $subsubseq, \@matches2Delete, \@dummy, 0, 0);
	}
	foreach my $foundRead (@matches2Delete) {
		if ( $seq =~ /$foundRead/) {
			&DELETEREAD($bin,$set,$foundRead);
		}
	}

	###### Extend ######
	my $extended = 1; 					
	while ($extended > 0) {
	
		my $seqLength = length($seq);
   	    	print "$seqLength\n" if ($seqLength % 1000 == 0); #notify user of extension progress

		my $span = $TRACELENGTH + 1;
		if ($span > $seqLength) { $span = $seqLength; }		
		my $subseq = substr($seq, -($span));


		my @sequencesMatches = ();  #Will hold the sequences that show matching
		my @sequenceLocs = (); 	    #Will record the offset of the sequence

		for (my $index = 0; $index <= (length($subseq) - $minConserv); $index++) {  #<----*** THE 18mer default readin overlap restriction is currently HARD CODED
			
			my $subsubseq = substr($subseq, $index);			
			my $numFound = GETOVERLAPSEQS($bin, $subsubseq, \@sequencesMatches, \@sequenceLocs, 0, $index);
			if ($numFound == -1) {return $seq;}
		}
		my $numberReads = @sequencesMatches;

		my $index = length($subseq) - $minConserv + 1;
		until (($numberReads >= $tripToLib) || ($index > (length($subseq) - $minLib))) {
			my $subsubseq = substr($subseq, $index);			
			my $numFound = GETOVERLAPSEQS($bin, $subsubseq, \@sequencesMatches, \@sequenceLocs, 0, $index);
			if ($numFound == -1) {return $seq;}
			$index++;
		}
		$numberReads = @sequencesMatches;
		
		#get sequences with single mismatch in the 'nonprefix' section of the overlap (after the first 11 bases of overlap)
		my $index = 0;
		until (($numberReads >= $tripToLib) || ($index > (length($subseq) - $minWerr)) || (not $minWerr)) {
			my $subsubseq = substr($subseq, $index);			
			my $numFound = GETERRSEQS($bin, $subsubseq, \@sequencesMatches, \@sequenceLocs, 0, $index);
			if ($numFound == -1) {return $seq;}
			$index++;
			$numberReads = @sequencesMatches;
		}
		
		##Sequences now stored in @sequencesMatches and their positions are in @sequenceLocs
		# run through the added on positions that are currently empty
		# The program looks both for adequate numbers of overlapping reads and a theshold of
		
		$numberReads = @sequencesMatches;
		my $aa = 0;
		my $tt = 0;
		my $gg = 0;
		my $cc = 0;

		last if ($numberReads < 1);
		
		for (my $matchingReadIndex = 0; $matchingReadIndex < $numberReads; $matchingReadIndex++) {

			my $seq2process = $sequencesMatches[$matchingReadIndex];
			my $seq2pLength = length($seq2process);
			my $seq2pIndex = $sequenceLocs[$matchingReadIndex];
			
			#print "$seq2process $seq2pLength $fromLeftOffset $seq2pIndex\n" unless ($newposition); #debug line
			#Will there be a base?
			if ($span - $seq2pIndex >= length($seq2process)) {
				$numberReads--;
			} else {
				my $vote = substr($seq2process, $span - $seq2pIndex, 1);	
				if ($vote eq "A") {
					$aa++;
				} elsif ($vote eq "T") {
					$tt++;
				} elsif ($vote eq "G") {
					$gg++;
				} elsif ($vote eq "C") {
					$cc++;
				} else {
					$numberReads--;
				}
			}
		}

		my @sortedVotes = sort { $b <=> $a} ($aa, $tt, $gg, $cc); 
		if ($sortedVotes[0] < 3) {
			print LOG "Unverified move: $seqLength\n";
		} 
		if (($minVerif) and ($sortedVotes[1] >= $minVerif)) {
			print LOG "Verified disagreement\n";
			last;	
		}
		
		my $appendableSequence = "";
		
		if ($numberReads > 0) {
			if ($aa / $numberReads > $consensusThresh) {
				$appendableSequence .= "A";
			} elsif ($tt / $numberReads > $consensusThresh) {
				$appendableSequence .= "T"; 
			} elsif ($gg / $numberReads > $consensusThresh) {
				$appendableSequence .= "G"; 
			} elsif ($cc / $numberReads > $consensusThresh) {
				$appendableSequence .= "C"; 
			} else {
				print LOG "Failing extension on lack of $consensusThresh majority of $numberReads: @sortedVotes\n";
			}
		} else {
			print "no reads found\n";
		}
		
		#prepare for next itteration...
		#add back incompletely used sequences to the bin, keep them removed from the set, include enough tail sequence so that it will be included in the next analysis

		$extended = length($appendableSequence);
		$seq .= $appendableSequence;		
		#print "\n";
		
		my $lastSeq2Process = "";
		while (my $seq2process = shift(@sequencesMatches)) {

			next if ($lastSeq2Process eq $seq2process);
			$lastSeq2Process = $seq2process;

			$usedReads{$seq2process}++;
			
			####FOR TESTING!!!
			if ($usedReads{$seq2process} > (2*$TRACELENGTH + 2)) { &DELETEREAD($bin,$set,$seq2process); }
			####
			
			#Does the read already appear COMPLETELY and IDENTICALLY in the extended sequence
			if ($seq =~ /$seq2process/) {
				&DELETEREAD($bin,$set,$seq2process);
			}
		}
		
		
	}
	
	
	################################################################################
	####  Diagnose why extension died (primarily for development purposes)    ######
	################################################################################
	
	my $subseq = substr($seq, -($TRACELENGTH + 2));	
	print LOG "Died on:\n$subseq\nreads:\n";
	for (my $index = 0; $index <= (length($subseq) - $minLib); $index++) {
		
		my @inFrameSeqs = ();
		my @dummy = ();
		my $subsubseq = substr($subseq, $index);			
		GETOVERLAPSEQS($bin, $subsubseq, \@dummy, \@dummy, \@inFrameSeqs, $index);
	
		foreach my $pass (@inFrameSeqs) {	
			for (my $spacer = 1; $spacer <= $index; $spacer++) {
				print LOG " ";
			}
			print LOG "$pass\n";
		}
	}
	################################################################################
	
	return $seq;
}

#-------------------------
sub GETOVERLAPSEQS {
	
	my ($binPt, $seq2find, $foundPt, $locsPt, $thisFramePt, $index) = @_;
	my $numfound = 0;
	
	my @ss=split(//,$seq2find);
	my $subset = $binPt->{$ss[0]}{$ss[1]}{$ss[2]}{$ss[3]}{$ss[4]}{$ss[5]}{$ss[6]}{$ss[7]}{$ss[8]}{$ss[9]}{$ss[10]};  #Will grab everything, even the reverse complement ones
	
	foreach my $pass (keys %$subset){		      #Get the keys, find the matches, take them, hold them, care for them. 
		if($pass =~ /^$seq2find([ACGT]*)/){         #can we align perfectly that subseq kmer
	
			#check to see if this sequence is already found in the array. If it has then there are multiple possible alignment frames, and the sequence should
			#not be used. Remove it from consideration on this run, but keep it in the main archive (Perhaps the RC will match uniquely? Improbable but keep it anyway)
			my $testIfMultipleFrames = 0;
			foreach my $seq2checkAgainst (@$foundPt) {
				if ($pass eq $seq2checkAgainst) {
					$testIfMultipleFrames++;
				}
			}
			if ($testIfMultipleFrames > 1) {
				print LOG "\n****FRAMING ISSUE****\n";
				return -1;
			}
			
			#push in as many reads as are represented by sequencing, and add to the total read count
			for (my $numReplicants = 0; $numReplicants < $subset->{$pass}; $numReplicants++) {
				if ($thisFramePt) { push(@$thisFramePt, $pass); }
				push(@$foundPt, $pass);
				push(@$locsPt, $index);
				$numfound++;
			}
		}
	}
	
	return $numfound;
}
#-------------------------
sub GETERRSEQS {
	
	my ($binPt, $seq2find, $foundPt, $locsPt, $thisFramePt, $index) = @_;
	my $numfound = 0;
	
	my @ss=split(//,$seq2find);
	my $subset = $binPt->{$ss[0]}{$ss[1]}{$ss[2]}{$ss[3]}{$ss[4]}{$ss[5]}{$ss[6]}{$ss[7]}{$ss[8]}{$ss[9]}{$ss[10]};  #Will grab everything, even the reverse complement ones
	
	#print "looking for sloppy seqs!\n";
	foreach my $pass (keys %$subset){		      #Get the keys, find the matches, take them, hold them, care for them. 
		 unless ($pass =~ /^$seq2find([ACGT]*)/) {         #ignore those we can align perfectly at that subseq kmer ... they've already been added
			#print "testing \n$pass\n$seq2find\n";
			#Check that overlap quality only has a single snp
			my $missmatch = 0;
			for (my $k = 0; $k < length($seq2find); $k++) {
				if (substr($seq2find, $k, 1) ne substr($pass, $k, 1)) {
					$missmatch++;
				}
			}
			#print "mismatch: $missmatch\n";
		
			unless ($missmatch > 1) {  #note: missmatch should be AT LEAST 1
				#check to see if this sequence is already found in the array. If it has then there are multiple possible alignment frames, and the sequence should
				#not be used. Remove it from consideration on this run, but keep it in the main archive (Perhaps the RC will match uniquely? Improbable but keep it anyway)
				my $testIfMultipleFrames = 0;
				foreach my $seq2checkAgainst (@$foundPt) {
					if ($pass eq $seq2checkAgainst) {
						$testIfMultipleFrames++;
					}
				}
				if ($testIfMultipleFrames > 1) {
					print LOG "\n****FRAMING ISSUE****\n";
					return -1;
				}
				#push in as many reads as are represented by sequencing, and add to the total read count
				#print "pushing...\n";
				for (my $numReplicants = 0; $numReplicants < $subset->{$pass}; $numReplicants++) {
					if ($thisFramePt) { push(@$thisFramePt, $pass); }
					push(@$foundPt, $pass);
					push(@$locsPt, $index);
					$numfound++;
				}
			}
		}
	}
	
	return $numfound;
}
#-----------------------
#reverse complements the input

sub REVCOMP {
	$_ = shift;
	$_ = uc();
	tr/ATGC/TACG/;
	return (reverse());
}
