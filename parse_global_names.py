#! /usr/bin/env python
import sys
import re
import csv

usage="""
parse core.csv records from http://gnaclr.globalnames.org/classifications

Data are expected in the following format:

taxonID,parentNameUsageID,datasetID,scientificName,taxonRank,modified

"""

nodes = dict() # The tree, keyed on taxonID with value list
names = set() # Set of names that have already been added, ignore subsequent entries

verbose = True

def getparent(nodeid):
	return nodes[nodeid][1]

def setdate(nodeid, date):
	nodelist = nodes[nodeid]
	nodelist[3] = date
	nodes[nodeid] = nodelist
	
def setparent(nodeid, parentid):
	nodelist = nodes[nodeid]
	nodelist[1] = parentid
	nodes[nodeid] = nodelist
	
def getdate(nodeid):
	return nodes[nodeid][3]
	
def getchildren(nodeid):
	children = set()
	for taxonid, nodelist in nodes.iteritems():
		if nodelist[1] == nodeid:
			children.add(taxonid)
	return children

if len(sys.argv)<2:
	print usage	
	
	


else:
	filename = sys.argv[1]
	subtree_node = -1
	if len(sys.argv) > 2:
		subtree_node = int(sys.argv[2]) # reduce the tree to this node and its descendents
	
	root_node = -1
	
	
	#infile = open(filename, "r")
	csv_reader = csv.reader(open(filename, "r"), delimiter=',', quotechar='"')
	
	taxon_re = re.compile("(\w+ \w+).+(\d\d\d\d)$", re.IGNORECASE)
	
	n = 0
	
	taxonRank = set()
	
	if verbose:
		print "# Loading Tree"
	
	# Load the tree into memory, parsing data from tips
	for fields in csv_reader:
		n = n+1
		if n < 2:
			continue
		
		#line = line.rstrip()
		#fields = line.split(',')
		taxon = fields[3]
		date = -1
		tip = 0
		#taxonRank.add(fields[4])
		name = taxon
		parentid = -1 # Default value, will apply to root since it has no parent
		taxonid = int(fields[0])
		if len(fields[1]) > 0:
			parentid = int(fields[1])
		else:
			root_node = taxonid
		
		
		
		if fields[4] == "subsp." or fields[4] == "subspecies":
			continue
			
		if fields[4] == "sp." or fields[4] == "species":
			tip = 1
			taxon_stripped = taxon.replace(")", "").replace("(", "")
			m = taxon_re.search(taxon)
			
			if m:
				name = m.group(1)
				date = int(m.group(2))
				if name in names:
					continue
				else:
					names.add(name)
		else:
			name = name.split()[0]
		
		
		nodes[taxonid] = [taxonid, parentid, name, date, tip]
		
	
	# If looking at a subtree, delete the real root and set it to something else
	
	if subtree_node > 0:
		del nodes[root_node]
		setparent(subtree_node, -1)
		root_node = subtree_node
	
	
	
	
	# Prune out nodes that aren't descended from the root
	n_orphans = 1
	
	
	if verbose:
		print "# removing orphans"
	
	
	while n_orphans > 0:
		n_orphans = 0
		nodeidlist = nodes.keys()
		nodeidset = set(nodeidlist)
		for nodeid in nodeidlist:
			parentid = getparent(nodeid)
			if parentid > 0 and not parentid in nodeidset:
				n_orphans = n_orphans + 1
				del nodes[nodeid]
	
	
	
				
	
	
	if verbose:
		print "#  painting dates on internal edges"
		
	# Paint each node with date of the youngest tip descendent
	# Loop over nodes
	for taxonid, nodelist in nodes.iteritems():
		# Check to see if it is a tip
		if nodelist[4] > 0:
			tipdate = getdate(taxonid)
			if tipdate > 0:
				# Traverse to root, setting the date to that of the youngest tip
				id = getparent(taxonid)
				while id > 0:
					date = getdate(id)
					newdate = date
					if (date < 0):
						newdate = tipdate
					elif (tipdate < date):
						newdate = tipdate
					setdate(id, newdate)
					id = getparent(id)
	
	
	# Delete nodes that don't have an age
	if verbose:
		print "#  deleting nodes without ages"
		
	ids = nodes.keys()
	for taxonid in ids:
		nodelist = nodes[taxonid]
		if nodelist[3] < 0:
			del nodes[taxonid]
		
		
	
	# collapse nodes that have only one child
	if verbose:
		print "#  collapsing internal nodes"
	
	ids = nodes.keys()
	for taxonid in ids:
		nodelist = nodes[taxonid]
		if nodelist[4] < 1:
			#isn't a tip
			
			children = getchildren(taxonid)
			
			if verbose:
					print "#   processing taxonid ", taxonid, "with children ", children
					
			if len(children) == 1:
				if verbose:
					print "#     removing taxonid ", taxonid
				child = list(children)[0]
				setparent(child, nodelist[1])
				del nodes[taxonid]
	
	print "id\tparent\tname\tdate\ttip"
	for taxonid, nodelist in nodes.iteritems():
		print '\t'.join([str(nodelist[0]), str(nodelist[1]), str(nodelist[2]), str(nodelist[3]), str(nodelist[4])])