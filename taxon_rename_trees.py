import os
import argparse
import sqlite3
import re

def query_trees(db, run_id):
	'''
	Queries the database and writes out tree file
	with new trees
	'''
	conn = sqlite3.connect(db)
	c = conn.cursor()

	sql1 = """
		SELECT tree
		FROM agalma_trees
		WHERE run_id = ?
		LIMIT 50;
		"""
	sql2 = """
		SELECT gene
		FROM agalma_sequences
		WHERE id =?;
		"""

	outfile = os.path.join(os.getcwd(), "trees_new_headers.trees")
	with open(outfile, "w") as f:
		c.execute(sql1, (run_id,))
		rows = c.fetchall()
		for tree in rows:
			tree = str(tree[0].strip("\n"))
			seq_ids = re.findall("(\@\d+)\:", tree)
			seq_headers = {}
			for i in seq_ids:
				s_id = i.replace("@", "") #ugly hack!
				c.execute(sql2, (s_id,))
				new_header = c.fetchall()
				seq_headers[i] = str(new_header[0][0])
			pattern = re.compile("|".join(seq_headers.keys()))
			tree = pattern.sub(lambda m: "@" + seq_headers[m.group(0)], tree)
			print >>f, tree + '\n'

	conn.close()
	
if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Takes a sqlite database \
		and a run_id number for genetrees, and returns genetrees with taxon \
		names including Trinity headers. By default it selects the top 50\
		trees in the database.')
	parser.add_argument('database', help="""full path to sqlite database.""")
	parser.add_argument('run_id', help="""run_id for genetrees run.""")
	args = parser.parse_args()
	query_trees(args.database, args.run_id)
	
#TO DO 
#argument to get any number of trees without editing code
#fix the search for the seq_id
