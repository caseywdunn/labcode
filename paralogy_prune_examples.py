from biolite import workflows
from dendropy import Tree
import sys

# To install biolite:
#
# git clone https://caseywdunn@bitbucket.org/caseywdunn/biolite.git
# cd biolite
# git checkout devel
# pip install matplotlib
# ./setup.py install
# ./setup.py test

Usage = """
An example of how to use the paralogy_prune() function

Usage:

To run with a prespecified tree:
pralogy_prune_example.py

To run with your own tree:
pralogy_prune_example.py "(E@1,(A@1,(B@1,(C@1,(D@1,(A@2,B@2))))));"

"""

tree_string = "((((((A@1,B@1),C@1),A@2),B@2),C@2),A@3);"

if len(sys.argv) > 1:
	tree_string = sys.argv[1]
else:
	print( Usage )


def parse_tree( tree_string ):
	# Convert string to tree object, abstract this since it changes between dendropy versions
	return Tree.get_from_string( tree_string, schema='newick', as_unrooted=False )

tree = parse_tree ( tree_string )

print ( "Original tree:" )
print ( tree.as_ascii_plot(show_internal_node_labels=True) )

pruned_trees = []
workflows.phylogeny.paralogy_prune(tree, pruned_trees)

print ( "Pruned trees:" )
for pruned_tree in pruned_trees:
	# .as_ascii_plot() errors on single taxon trees in Dendropy 4,
	# handle this case separately 
	try:
		print ( pruned_tree.as_ascii_plot(show_internal_node_labels=True) )
	except:
		if len(pruned_tree.leaf_nodes()) == 1:
			print ( "single tip tree:" + pruned_tree.leaf_nodes()[0].taxon.label )
