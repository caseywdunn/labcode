from Bio import SeqIO
import sys,os

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage python: get_seq_of_length_n.py length infile.fa outfile.fa"
		sys.exit(0)
	length = int(sys.argv[1])
	infile = open(sys.argv[2],"r")
	outfile = open(sys.argv[3],"w")
	
	keep = []
	for i in SeqIO.parse(infile,"fasta"):
		if len(i.seq) > length:
			keep.append(i)
	SeqIO.write(keep,outfile,"fasta")