import sys
from svg_lib import *

"""
this looks at the ace summarized infile for contigs
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python convert_combined_to_svg_contig.py contig infile outfile"
		sys.exit(0)
	contig = sys.argv[1]
	
	infile = open(sys.argv[2],"r")
	data = None
	lc = len(contig)
	MAX_Y = 0
	for i in infile:
		if i[:lc] == contig:
			spls = i.strip().split(" ")
			data = spls[1:]
			for j in spls[1:]:
				if int(j) > MAX_Y:
					MAX_Y = int(j)
			
	infile.close()
	
	SCALE = 1
	XOFFSET = 5
	YOFFSET = 5
	
	MAX_Y *= 2
	
	ld = len(data)
	
	scene = Scene(sys.argv[3],(YOFFSET+MAX_Y),(2*XOFFSET)+(len(data)*SCALE))
	#setup the space
	#he red
	scene.add(CLine((0+XOFFSET-1,(YOFFSET+MAX_Y)),((ld*SCALE)+XOFFSET,(YOFFSET+MAX_Y)), "black"))
	count = 0
	for i in data:
		scene.add(CLine(((count + XOFFSET),((YOFFSET+MAX_Y)*1)), ((count + XOFFSET),((YOFFSET+MAX_Y)*1)-int(i)), "gray"))
		count += 1
		
	scene.write_svg()
	#scene.display()
	
