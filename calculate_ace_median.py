import sys,numpy
from Bio import SeqIO
from Bio.Sequencing import Ace

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python calculate_ace_median.py isotig.ace"
		sys.exit(0)
	acefile = open(sys.argv[1],"rU")
	read_lens = []
	for isotig in Ace.parse(acefile):
		for read in isotig.reads:
			read_lens.append(len(read.rd.sequence))
			#print len(read.rd.sequence)
	print numpy.median(read_lens)
