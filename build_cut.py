import sys,os
from Bio import SeqIO
from Bio.Blast import NCBIXML

"""
constructs codon usage bias table for prot4EST
"""

DEBUG = False

#from the codons file
codon_template = {'ACC': 'Thr', 'ATG': 'Met', 'AAG': 'Lys', 'AAA': 'Lys', 'ATC': 'Ile', 'AAC': 'Asn', 'ATA': 'Ile', 'AGG': 'Arg', 'CCT': 'Pro', 'CTC': 'Leu', 'AGC': 'Ser', 'ACA': 'Thr', 'AGA': 'Arg', 'CAT': 'His', 'AAT': 'Asn', 'ATT': 'Ile', 'CTG': 'Leu', 'CTA': 'Leu', 'ACT': 'Thr', 'CAC': 'His', 'ACG': 'Thr', 'TAG': 'End', 'AGT': 'Ser', 'CAG': 'Gln', 'CAA': 'Gln', 'CCC': 'Pro', 'CTT': 'Leu', 'TAT': 'Tyr', 'GGT': 'Gly', 'TGT': 'Cys', 'CGA': 'Arg', 'CCA': 'Pro', 'TCT': 'Ser', 'GAT': 'Asp', 'CGG': 'Arg', 'TTT': 'Phe', 'TGC': 'Cys', 'GGG': 'Gly', 'TGA': 'End', 'GGA': 'Gly', 'TGG': 'Trp', 'GGC': 'Gly', 'TAC': 'Tyr', 'GAG': 'Glu', 'TCG': 'Ser', 'TTA': 'Leu', 'GAC': 'Asp', 'TCC': 'Ser', 'GAA': 'Glu', 'TAA': 'End', 'GCA': 'Ala', 'GTA': 'Val', 'GCC': 'Ala', 'GTC': 'Val', 'GCG': 'Ala', 'GTG': 'Val', 'TTC': 'Phe', 'GTT': 'Val', 'GCT': 'Ala', 'TTG': 'Leu', 'CCG': 'Pro', 'CGT': 'Arg', 'TCA': 'Ser', 'CGC': 'Arg'}
codon_order_codon = ['GGG', 'GGA', 'GGT', 'GGC', 'GAG', 'GAA', 'GAT', 'GAC', 'GTG', 'GTA', 'GTT', 'GTC', 'GCG', 'GCA', 'GCT', 'GCC', 'AGG', 'AGA', 'AGT', 'AGC', 'AAG', 'AAA', 'AAT', 'AAC', 'ATG', 'ATA', 'ATT', 'ATC', 'ACG', 'ACA', 'ACT', 'ACC', 'TGG', 'TGA', 'TGT', 'TGC', 'TAG', 'TAA', 'TAT', 'TAC', 'TTG', 'TTA', 'TTT', 'TTC', 'TCG', 'TCA', 'TCT', 'TCC', 'CGG', 'CGA', 'CGT', 'CGC', 'CAG', 'CAA', 'CAT', 'CAC', 'CTG', 'CTA', 'CTT', 'CTC', 'CCG', 'CCA', 'CCT', 'CCC']
codon_order_aa = ['Gly', 'Gly', 'Gly', 'Gly', 'Glu', 'Glu', 'Asp', 'Asp', 'Val', 'Val', 'Val', 'Val', 'Ala', 'Ala', 'Ala', 'Ala', 'Arg', 'Arg', 'Ser', 'Ser', 'Lys', 'Lys', 'Asn', 'Asn', 'Met', 'Ile', 'Ile', 'Ile', 'Thr', 'Thr', 'Thr', 'Thr', 'Trp', 'End', 'Cys', 'Cys', 'End', 'End', 'Tyr', 'Tyr', 'Leu', 'Leu', 'Phe', 'Phe', 'Ser', 'Ser', 'Ser', 'Ser', 'Arg', 'Arg', 'Arg', 'Arg', 'Gln', 'Gln', 'His', 'His', 'Leu', 'Leu', 'Leu', 'Leu', 'Pro', 'Pro', 'Pro', 'Pro']

three2one = {

'Ala' : 'A',
'Cys' : 'C',
'Asp' : 'D',
'Glu' : 'E',
'Phe' : 'F',
'Gly' : 'G',
'His' : 'H',
'Ile' : 'I',
'Lys' : 'K',
'Leu' : 'L',
'Met' : 'M',
'Asn' : 'N',
'Pro' : 'P',
'Gln' : 'Q',
'Arg' : 'R',
'Ser' : 'S',
'Thr' : 'T',
'Val' : 'V',
'Trp' : 'W',
'Tyr' : 'Y',
'End' : '*'

}

def build_cut(in_fasta_name, blast_name, out_name, format='gcg'):

	#open infile
	infile = open(in_fasta_name,"rU")
	allseqs_dict = {}
	for i in SeqIO.parse(infile,"fasta"):
		allseqs_dict[i.id] = i
	inblast = open(blast_name,"rU")
	usage = {}
	count = 0 # no need to go through all the hits
	maxcount = 5
	for j in NCBIXML.parse(inblast):
		qutitle = j.query.split(" ")[0]
		if len(j.alignments) > 0:
			aln = j.alignments[0]  #only look at the first subject
			if len(aln.hsps) > 0:
				hsp = aln.hsps[0] #only look at the first hit
				seq = None
				try:
					seq = allseqs_dict[qutitle].seq[int(hsp.query_start)-1:int(hsp.query_end)]
				except:
					continue
				ans = False
				if int(hsp.frame[0]) < 0: #ignore antisense
					ans = True #antisense
					seq = seq.reverse_complement()
				#print seq,int(hsp.query_start),int(hsp.query_end)
				#print "-",seq.translate()
				start = 0
				while start <= len(seq)-3:
					codon = seq[start:start+3].upper().tostring()
					if codon not in usage:
						usage[codon] = 0
					usage[codon] += 1
					start += 3
				if count >= maxcount:
					continue
				count += 1
	inblast.close()
	total = 0.
	aa_count = {} # key aa and value number of instances of the amino acid
	for i in usage:
		total += usage[i]
		if i not in codon_template:
			continue
		if codon_template[i] not in aa_count:
			aa_count[codon_template[i]] = 0
		aa_count[codon_template[i]] += usage[i]
	
	protein_length = 500. # a guess at the average length of proteins; used to estimate the number of stop codons so that there aren't empty fields
	#can't this be estimated from either the blast results or the lengths?

	end_estimate  = total/protein_length
	outfile = open(out_name,"w")
	i = 0
	
	
	
	
	if format == 'cusp':
		outfile.write("#Codon\tAA\tFraction\tFrequency\tNumber")
		
	
	for codon in codon_order_codon:
		i += 1
		aa = codon_template[codon]
		number = 0
		try:
			number = usage[codon]
		except:
			number = 0

		per_mil = 0
		if total > 0:
			per_mil = number/float(total)*1000.
		
		fraction = 0
		if number > 0:
			fraction = number / float(aa_count[aa])
		
		if format == 'gcg':
			pr_str = "%s     %s     %.2f     %.2f     %.2f" % (aa,codon,number,per_mil,fraction)
		elif format == 'cusp':
			pr_str = "%s     %s     %.2f     %.2f     %d" % (codon,three2one[aa],fraction,per_mil,number)
		
		outfile.write(pr_str+"\n")

		if (format == 'gcg') and (i%4 == 0):
			outfile.write("\n")
	outfile.close()


if __name__ == "__main__":
	if len(sys.argv) < 5:
		print "usage: python build_cut.py infile.fna blast_file.xml outfile [gcg|cusp, defaults to gcg output format]"
		sys.exit(0)
	format = 'gcg'
	if len(sys.argv) > 4:
		format = sys.argv[4]
	
	build_cut(sys.argv[1], sys.argv[2], sys.argv[3], format)
	
