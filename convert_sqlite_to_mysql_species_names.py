import sys,os,sqlite3

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python convert_sqlite_to_mysql_species_names.py infile.db outfile"
        sys.exit(0)
    dbname = sys.argv[1]
    if os.path.exists(dbname) == False:
        print "the database doesn't exist"
        sys.exit(0)
    con = sqlite3.connect(dbname)
    species = []
    cur = con.cursor()
    cur.execute("SELECT * FROM species_names;")
    a = cur.fetchall()
    outfile = open(sys.argv[2],"w")
    for i in a:
        outfile.write("INSERT INTO taxa (name) values ('"+i[1]+"');\n")
    outfile.close()
    con.close()
