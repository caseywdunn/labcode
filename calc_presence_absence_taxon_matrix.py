import sys,os

"""
This will calculate the presence absence matrix for a phylip file
it will output for each taxon whether it has data for that particular
site.
"""

if __name__ == '__main__':
	if len(sys.argv) != 3:
		print "python calc_presence_absence_taxon_matrix.py in.phy outfile"
		sys.exit(0)
	
	taxon = {} # this is a dictionary of lists with the taxon name as the key
	
	infile = open(sys.argv[1],"r")
	first = True
	for i in infile:
		if first == True or len(i) < 2:
			first =False
			continue
		spls = i.strip().split("\t")
		print spls[0]
		nlist = []
		for j in range(len(spls[1])):
			if spls[1][j] != '-' and spls[1][j] != '?' and spls[1][j] != 'X':
				nlist.append(1)
			else:
				nlist.append(0)
		taxon[spls[0]] = nlist
	infile.close()
	
	outfile = open(sys.argv[2],"w")
	for j in taxon:
		outfile.write(j+"\t")
		for i in taxon[j]:
			outfile.write(str(i)+" ")
		outfile.write("\n")
	outfile.close()