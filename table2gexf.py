#! /usr/bin/env python
import sys
import re
import csv

usage="""

"""






if len(sys.argv)<2:
	print usage	

else:
	filename = sys.argv[1]
	infile = open(filename, 'r')
	
	
	nodes = list()
	edges = list()
	
	n = 0
	
	for line in infile:
		n = n + 1
		if n < 2:
			continue
		line = line.rstrip()
		fields = line.split('\t')
		node = '  <node id="{0}" label="{1}">\n    <attvalue for="0" value="{2}"/>\n    <attvalue for="1" value="{3}"/>\n  </node>\n'.format(fields[0], fields[2], fields[3], fields[4])
		nodes.append(node)
		
		id = int(fields[0])
		parent = int(fields[1])
		
		# Don't add an edge subtending the root
		if parent > 0:
			edge = '  <edge id="{0}" source="{1}" target="{0}" />'.format(id, parent)
		edges.append(edge)
	
	print('<gexf xmlns="http://www.gexf.net/1.2draft" version="1.2">\n<graph mode="static" defaultedgetype="directed">')
	print('<attributes class="node">\n <attribute id="0" title="date" type="integer"/>\n  <attribute id="1" title="tip" type="integer"/>\n</attributes>')
	print('<nodes>')
	
	for node in nodes:
		print node
	
	print('</nodes>')
	
	print('<edges>')
	
	for edge in edges:
		print edge
	
	print('</edges>')
	
	print("</graph>\n</gexf>")