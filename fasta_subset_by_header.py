#! /usr/bin/env python

import sys
import re
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord



def check_string_against_list(query, reference):
	"""Checks to see if string query contains any of the strings in list reference"""
	refterm =  '(' + '|'.join(reference) + ')'
	match = False
	if (re.search(refterm, query)):
			match = True
	return match


# Set the input fasta file name
InFileName = sys.argv[1]

# Set the the name of the file with the headers to keep
SubFileName = sys.argv[2]

# Set the input fasta file name
OutFileName = sys.argv[3]

# Open the files
InFile = open(InFileName, 'r')
SubFile = open(SubFileName, 'r')
OutFile = open(OutFileName, 'w')

patterns = list()


for line in SubFile:
	line = line.rstrip()
	if len(line) > 0:
		patterns.append(line)

# Loop over each line in the file
for seq_record in SeqIO.parse(InFile, "fasta") :
	desc = seq_record.description
	
	if check_string_against_list(desc, patterns):
		outlist = [seq_record]
		SeqIO.write(outlist, OutFile, "fasta")