import sys
from Bio.Blast import NCBIXML

"""
this investigates the hits of the subject sequence in blast results for
each transcript in the oases transcripts file
"""

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python investigate_oases_transcripts_blast.py infile.xml"
		sys.exit()
	
	infile = open(sys.argv[1],"r")
	
	locus_trans = {} #key is Locus_x, value is a list of Transcript_x/x that have hits
	
	transcript_hit = {} #key is Locus_x_Transcript_x/x, value is the 0-length of hit with 1's where there is a hit
	transcript_conf = {} #key is Locus_x_Transcript_x/x, value is the confidence
	transcript_hitid = {} #key is Locus_x_Transcript_x/x, value the id of the hit
	
	#count = 0
	#count_lim = 200
	for i in NCBIXML.parse(infile):
		qutitle = i.query.split(" ")[0]
		if len(i.alignments) > 0:
			first = True
			for k in i.alignments:
				#only do the first hit
				if first == True:
					first = False
					first2 = True
					for j in k.hsps:
						#if first2 == False:
							#break
						if "N" not in j.query or "N" in j.query:
							#print qutitle
							#print j.sbjct_start,j.sbjct_end
							spls = qutitle.split("_Confidence_")
							if spls[0] not in transcript_hit:
								transcript_hit[spls[0]] = [0]*int(j.sbjct_end)
								transcript_conf[spls[0]] = float(spls[1])
								transcript_hitid[spls[0]] = k.hit_id
								spls2 = spls[0].split("_Transcript_")
								if spls2[0] not in locus_trans:
									locus_trans[spls2[0]] = []
								locus_trans[spls2[0]].append("Transcript_"+spls2[1])
							if int(j.sbjct_end) > len(transcript_hit[spls[0]]):
								vec = [0]*int(j.sbjct_end)
								for m in range(len(transcript_hit[spls[0]])):
									vec[m] = transcript_hit[spls[0]][m]
								transcript_hit[spls[0]] = vec
							for m in range(int(j.sbjct_end)-int(j.sbjct_start)):
								if j.query[m] != "N":
									transcript_hit[spls[0]][m+j.sbjct_start] = 1
							#print transcript_hit[spls[0]],transcript_hitid[spls[0]]
							#sys.exit(0)
						first2 = False
				else:
					break
		#print qutitle,len(i.alignments)
		#count += 1
		#if count == count_lim:
		#	break
	
	for i in locus_trans:
		print i
		for j in locus_trans[i]:
			print j,transcript_hitid[i+"_"+j], transcript_conf[i+"_"+j],sum(transcript_hit[i+"_"+j])

