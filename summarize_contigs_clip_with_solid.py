import sys
from Bio import SeqIO

"""
This should summarize the solid results.tab file using problem files of the 454AllContigs.fna (on which the mapping occurred). 
So this file accomodates for the clipping.

This also assumes that the cap3 contigs keep their capital C in the contig name.

This will assume that the input 454AllContigs.fna is the incorrect clipping one (the one with errors) and will output 
CORRECT values

MAKE SURE CLIPPING OCCURS CORRECTLY

"""

MAX_MISMATCH = 2

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python summarize_contigs_clip_with_solid.py 454AllContigs.fna cap3.contigs results.tab outfile"
		sys.exit(0)
		
	#open the 454AllContigs.fna
	infile = open(sys.argv[1],"rU")
	contig_clips_begin = {}
	contig_clips_end = {}
	for i in SeqIO.parse(infile,"fasta"):
		spls = i.description.split(" ")
		length = int(spls[2].split("=")[1])
		if length == 0:
			print "warning(fna): "+i.id+" length:0"
			contig_clips_begin[i.id] = 0 #definitely clip
			contig_clips_end[i.id] = 0
		else:
			contig_clips_begin[i.id] = int(len(i.seq.tostring())-length)
			contig_clips_end[i.id] = int(length)
	infile.close()

	#open the cap3.contigs
	infile = open(sys.argv[2],"rU")
	cap3lens = {}
	for i in SeqIO.parse(infile,"fasta"):
		cap3lens[i.id] = len(i.seq)
	infile.close()

	contig_clips_match = {} #key is contig, and returns a vector

	
	dict = {} #this is to keep the data in order to work with later
	dictrev = {} #this is the reverse
	
	infile = open(sys.argv[3],"rU")
	first = True
	count = 0
	for i in infile:
		if first == True:
			first = False
		else:
			spls =  i.strip().split("\t")
			contig = spls[1]
			startv = int(spls[2])
			mismatch = int(spls[4])
			if mismatch <= MAX_MISMATCH:
				if contig not in dict:
					dict[contig] = {}
				if contig not in dictrev:
					dictrev[contig] = {}
				if contig[0] == 'C': #cap C is cap3 and doesn't need filtering
					if startv > 0:
						for i in range(21):
							if (abs(startv)+i) not in dict[contig]:
								dict[contig][abs(startv)+i] = 0
							dict[contig][abs(startv)+i] += 1
					else:#rev
						startv -= 3 #because of counting -3 not -4
						for i in range(21):
							if (abs(startv)-i) not in dictrev[contig]:
								dictrev[contig][abs(startv)-i] = 0
							dictrev[contig][abs(startv)-i] += 1
#					print "f:",dict[contig]
#					print "r:",dictrev[contig]
				else:
					clip = contig_clips_begin[contig]
					neg = False
					if startv < 0:
						startv -= 4
						startv += 21
						startv = abs(startv)
						neg = True
					if clip <= startv:
						for i in range(21):
							if neg == True:
								if ((startv-clip)+i) not in dictrev[contig]:
									dictrev[contig][(startv-clip)+i] = 0
								dictrev[contig][(startv-clip)+i] += 1
							else:#not rev
								if ((startv-clip)+i) not in dict[contig]:
									dict[contig][(startv-clip)+i] = 0
								dict[contig][(startv-clip)+i] += 1
#					print "cf:",dict[contig]
#					print "cr:",dictrev[contig]
					count += 1
					if count % 1000000 == 0:
						print count
#					if count ==50:
#						break
	infile.close()
	
	outfile = open(sys.argv[4],"w")
	outfile.write("contig\tfd\trd\n")
	for i in dict:
		start = 0
		end = 0
		if i[0] == "C": #cap3 contigs
			end = cap3lens[i]
		else:
			end = contig_clips_end[i]
		for j in range(end):
			d = 0
			dr = 0
			if j in dict[i]:
				d = dict[i][j]
			if j in dictrev[i]:
				dr = dictrev[i][j]
			outfile.write(str(i)+"\t"+str(d)+"\t"+str(dr)+"\n")
	outfile.close()
	
