#! /bin/env python
from glob import glob
import json


if __name__ == "__main__":

	users = {}
	with open('users.json') as data_file:    
		users_data = json.load(data_file)
		for user in users_data:
			users[ user["id"] ] = user["name"]

	print users
