import sys,sqlite3,os

"""
probably want the database name to be something like
mollusk.db
"""

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python create_base_database.py database_name"
		sys.exit(0)
	db_name = sys.argv[1]
	if os.path.exists(db_name):
		print "database file exists -- please delete before running this"
		sys.exit(0)

	con = sqlite3.connect(db_name)
	curup = con.cursor()

	curup.execute("CREATE TABLE species_names (id INTEGER PRIMARY KEY,name VARCHAR(255),ingroup INTEGER);")
	curup.execute("CREATE INDEX species_names_name on species_names(name);")

	curup.execute("CREATE TABLE blast_results (id INTEGER PRIMARY KEY, species_names_id INTEGER, query_full_id VARCHAR(255), query_edited_id VARCHAR(255), hit_num INTEGER, hit_id VARCHAR(255), hit_def VARCHAR(255), hit_accession VARCHAR(255), evalue VARCHAR(255), file VARCHAR(255));")
	curup.execute("CREATE INDEX blast_results_hit_id on blast_results(hit_id);")
	curup.execute("CREATE INDEX blast_results_hit_num on blast_results(hit_num);")
	curup.execute("CREATE INDEX blast_results_hit_def on blast_results(hit_def);")
	curup.execute("CREATE INDEX blast_results_hit_accession on blast_results(hit_accession);")
	curup.execute("CREATE INDEX blast_results_query_edited_id on blast_results(query_edited_id);")
	curup.execute("CREATE INDEX blast_results_species_names_id on blast_results(species_names_id);")


	curup.execute("CREATE TABLE translated_seqs (id INTEGER PRIMARY KEY, species_names_id INTEGER, seq_id VARCHAR(255), edited_seq_id VARCHAR(255),seq LONGTEXT, file VARCHAR(255), masked_seq LONGTEXT);")
	curup.execute("CREATE INDEX translated_seqs_species_names_id on translated_seqs(species_names_id);")
	curup.execute("CREATE INDEX translated_seqs_sequence_id  on translated_seqs(seq_id);")
	curup.execute("CREATE INDEX translated_seqs_edited_sequence_id on translated_seqs(edited_seq_id);")

	con.commit()
	con.close()
