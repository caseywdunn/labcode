import sys

if len(sys.argv) < 3:
	print "usage: python number_lines.py infile OUTFILE"
infile = open(sys.argv[1], 'r')
outfile = open(sys.argv[2], 'w')
infileData = infile.readlines()

linenumber = 1
newdata = ""
for i in range(len(infileData)):
	newdata += str(linenumber) + "\t" + infileData[i]
	linenumber += 1

outfile.write(newdata)
outfile.close()
infile.close