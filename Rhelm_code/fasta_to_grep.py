"""
written by Rhelm
09DEC2011
last updated: 09DEC2011
"""
import sys

InputFile = open(sys.argv[1], "r")
InputFileContent = InputFile.readlines()
GrepString= ""

for line in InputFileContent:
	if line[0] == ">":
		newline = line.rstrip('\r\n')
		GrepString+= "\n"+newline+"\t"
	else:
		GrepString+= line.rstrip('\r\n')
GrepFileName = "grepable_"+sys.argv[1]
NewGrepReference = open(GrepFileName, 'w')
NewGrepReference.write(GrepString)
NewGrepReference.close()