import sys,os
from Bio import SeqIO

if __name__=="__main__":
    if len(sys.argv) != 3:
        print "python convert_fastq_to_fasta.py infile.fastq outfile.fasta"
        sys.exit(0)
    SeqIO.convert(sys.argv[1],"fastq",sys.argv[2],"fasta")
