
# Example commands to run previous to this script:
# export AGALMA_DB=/gpfs/data/cdunn/analyses/agalma-siphonophora-20170501.sqlite
# module load agalma/1.0.0
# agalma diagnostics list
# Then identify the id of the genetree run you want to analyze, eg 92

import sys
import re
from collections import Counter
from agalma import config
from agalma import database


Usage = """
Print out taxon sampling of specified alignments

The first argument is the run_id for the gene_tree run
All following arguments are ids of the gene trees you 
want to analyze.

Usage:

To run with a prespecified tree:
python gene_alignment_taxon_sampling.py 97 7497 12514 5022

"""

if len(sys.argv) > 2:
	multalign_run_id = int( sys.argv[1] )
	alignment_ids = map( int, sys.argv[2:] )
else:
	print Usage

print("multalign run_id: " + str(multalign_run_id))

p = re.compile(r'\n[^>][\w-]+\n')

alignments = database.select_alignments(multalign_run_id)

for i, a in alignments:
	if i in alignment_ids:
		print( "\talignment_id: " + str(i) )
		names = p.split(a)
		names = names[:-1] # get rid of last empty item
		species = []
		print("\t\tAll Tips:")
		for name in names:
			name = name.strip()
			name = name[1:]
			print("\t\t\t" +name)
			sp = name.split('@')[0]
			species.append(sp)
		counts = dict()
		for sp in species:
			counts[sp] = counts.get(sp, 0) + 1
		print("\t\tSpecies counts:")
		for sp, count in counts.items():
			print("\t\t\t" + str(count) + "\t" + sp)
			
