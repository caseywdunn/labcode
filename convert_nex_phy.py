import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python convertnexphy.py infile outfile"
		sys.exit(0)
	infile = open(sys.argv[1],"r")
	outfile = open(sys.argv[2],"w")
	
	start = False
	startdata = False
	ntax = ""
	nsite = ""
	for i in infile:
		if startdata == True:
			if ";" in i:
				startdata = False
			else:
				outfile.write(i.strip()+"\n")
		if start == True:
			if "NTAX" in i:
				ntax = i.split("NTAX=")[1].split(" ")[0]
			if "NCHAR" in i:
				nsite = i.split("NCHAR=")[1][:-2]
				outfile.write(ntax+" "+nsite+"\n")
		if "BEGIN DATA" in i:
			start = True
		if "MATRIX" in i:
			startdata = True
	outfile.close()
	infile.close()

