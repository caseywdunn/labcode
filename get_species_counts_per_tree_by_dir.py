import os,sys
import newick3,phylo3

"""
This assumes that the names will be speciesnames@number and will ignore the number
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python get_species_counts_per_tree_by_dir.py folder outfile"
		sys.exit()	
	folder = sys.argv[1]
	counts = [] #this will be a list of dictionaries for each tree
	names = []
	for i in os.listdir(folder):
		if "mm" == i[-2:]:
			dict = {}
			x = open(str(folder)+"/"+str(i),"r")
			tree = newick3.parse(x.readline())
			for s in tree.leaves():
				nm = s.label.split("@")[0]
				if nm not in names:
					names.append(nm)
				if nm not in dict:
					dict[nm] = 0
				dict[nm] += 1
			if len(dict) > 3:
				counts.append(dict)
			print i
			break
	out = open(sys.argv[2],"w")
	outstr = ""
	for j in names:
		outstr += j+"\t"
	outstr = outstr[:-1]
	out.write(outstr+"\n")
	for i in counts:
		outstr = ""
		for j in names:
			if j in i:
				outstr += str(i[j])+"\t"
			else:
				outstr += "0\t"
		outstr = outstr[:-1]
		out.write(outstr+"\n")
	out.close()
