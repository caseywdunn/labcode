from Bio import SeqIO
import sys,os

"""
this should report how well represented the taxa are in the clusters

"""

file_ending = ".fasta"

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python get_taxa_representation_in_clusters.py DIR_focal DIR_2 DIR_3 outfile"
		sys.exit(0)
	path = sys.argv[1]
	clusters = []
	for i in os.listdir(path):
		print i
		if i[-len(file_ending):] == file_ending:
			names = []
			handle = open(path+"/"+i,"rU")
			for j in SeqIO.parse(handle,"fasta"):
				names.append(j.id)
			handle.close()
			clusters.append(names)
	path2 = sys.argv[2]
	clusters2 = []
	for i in os.listdir(path2):
		print i
		if i[-len(file_ending):] == file_ending:
			names = []
			handle = open(path2+"/"+i,"rU")
			for j in SeqIO.parse(handle,"fasta"):
				names.append(j.id)
			handle.close()
			clusters2.append(names)
	path3 = sys.argv[3]
	clusters3 = []
	for i in os.listdir(path3):
		print i
		if i[-len(file_ending):] == file_ending:
			names = []
			handle = open(path3+"/"+i,"rU")
			for j in SeqIO.parse(handle,"fasta"):
				names.append(j.id)
			handle.close()
			clusters3.append(names)
	outfile = sys.argv[4]
	out = open(outfile,"w")
	count = 0
	for i in clusters:
		sub1 = False
		super1 = False
		equal1 = False
		sub2 = False
		super2 = False
		equal2 = False
		for j in clusters2:
			if i[0] in j:
				sub1 = set(i).issubset(set(j))
				super1 = set(i).issuperset(set(j))
				equal1 = set(i) == set(j)
				if equal1 == True:
					super1 = False
					sub1 = False
				break
		for j in clusters3:
			if i[0] in j:
				sub2 = set(i).issubset(set(j))
				super2 = set(i).issuperset(set(j))
				equal2 = set(i) == set(j)
				if equal2 == True:
					super2 = False
					sub2 = False
				break
		out.write(str(count)+"\t"+str(int(sub1))+"\t"+str(int(super1))+"\t"+str(int(equal1))+"\t"+str(int(sub2))+"\t"+str(int(super2))+"\t"+str(int(equal2))+"\n")
		count += 1
	out.close()
