import sys,os

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "python run_inparanoid.py specieslistfile"
        sys.exit(0)
    species_list = []
    infile = open(sys.argv[1],"r")
    for i in infile:
        species_list.append(i.strip())
    infile.close()

    for i in species_list:
        for j in species_list:
            if i != j:
                cmd = "perl inparanoid.pl "+i+" "+j
                print cmd
                os.system(cmd)
