from Bio import SeqIO
import sys

"""
Converts quality scores in fastq files according to standard formats:

fastq-illumina - Solexa/Illumina 1.3 to 1.7 variant of the FASTQ format which encodes PHRED quality scores with an ASCII offset of 64 (not 33). Note as of version 1.8 of the CASAVA pipeline Illumina will produce FASTQ files using the standard Sanger encoding.

fastq - A "FASTA like" format used by Sanger which also stores PHRED sequence quality values (with an ASCII offset of 33).

"""

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python fastq_convert_quality.py infile informat outfile outformat"
		sys.exit(0)
	count = 0

	infile = open(sys.argv[1],"r")
	outfile = open(sys.argv[3],"w")
	seqs = []
	for i in SeqIO.parse(infile, sys.argv[2]):
		SeqIO.write([i],outfile, sys.argv[4])
		count += 1
		if count % 100000 == 0:
			print count	
	infile.close()
	outfile.close()
