import sys,os

from Bio import SeqIO

"""
	this assumes that the vector output is in outfmt 6
	can run this like
	blastn -query infile -db ~/lib/blast_data/univec.db -outfmt 6
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python clean_vectors_by_blast_out.py infile.blastout infile.fsa outfile.fsa"
		sys.exit(0)
	
	seqcutstarts = {}
	seqcutends = {}
	infilebl = open(sys.argv[1],"r")
	for i in infilebl:
		spls = i.strip().split("\t")
		start = int(spls[6])
		end = int(spls[7])
		if spls[0] not in seqcutstarts:
			seqcutstarts[spls[0]] = None
			seqcutends[spls[0]] = None
		if end < start:
			temp = start
			start = end
			end = temp
		if seqcutstarts[spls[0]] == None:
			seqcutstarts[spls[0]] = start
			seqcutends[spls[0]] = end
		if start < seqcutstarts[spls[0]]:
			seqcutstarts[spls[0]] = start
		if end > seqcutends[spls[0]]:
			seqcutends[spls[0]] = end
	infilebl.close()

	infilefa = open(sys.argv[2],"r")
	outseqs = []
	for i in SeqIO.parse(infilefa,"fasta"):
		if i.id in seqcutstarts:
			i = i[0:seqcutstarts[i.id]-1]+i[seqcutends[i.id]:len(i.seq)]
			if len(i.seq) > 20:
				outseqs.append(i)
		else:
			outseqs.append(i)
	infilefa.close()

	outfile = open(sys.argv[3],"w")
	SeqIO.write(outseqs,outfile,"fasta")
	outfile.close()
