from Bio import SeqIO
import sys,os

"""
this is to get the list to concatenate based on a minimum number of taxa

it can easily be used for the other files just by changing the file ending
"""

file_ending = ".aln-gb"

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python get_gb_min_sites_taxa.py DIR numofsites numoftaxa outfile"
		sys.exit(0)
	path = sys.argv[1]
	numofsites = int(sys.argv[2])
	numoftaxa = int(sys.argv[3])
	outfile = sys.argv[4]
	out = open(outfile,"w")
	for i in os.listdir(path):
		print i
		if i[-len(file_ending):] == file_ending:
			handle = open(path+"/"+i,"rU")
			seqs = list(SeqIO.parse(handle,"fasta"))
			numbertaxa = len(seqs)
			numbersites = len(seqs[0].seq)
			handle.close()
			if numbersites >= numofsites:
				if numbertaxa >= numoftaxa:
					out.write(str(i)+" ")
	out.write("\n")
	out.close()
