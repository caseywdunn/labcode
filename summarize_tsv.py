import sys
from Bio import SeqIO
import numpy

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python summarize_tsv.py fna 454AlignmentInfo.tsv"
		sys.exit(0)
	ref = open(sys.argv[1],"rU")
	tsv = open(sys.argv[2],"rU")
	
	# key reference sequence, value list of mapping depth at each position
	counts = {}
	lengths = {}
	
	for i in SeqIO.parse(ref,"fasta"):
		length = len(i.seq)
		id = i.id.strip()
		# print i.id,length
		counts[id] = [0] * length
		lengths[id] = length
	ref.close()
	
	n=0
	id = ""
	for line in tsv:
		n += 1
		if n > 1:
			line = line.rstrip()
			fields = line.split()
			if len(fields) == 2:
				# Header specifying sequence id
				id = fields[0][1:] # get the id out of the first field, skipping the first >
			else:
				# line contains position info 
				# Position        Reference       Consensus       Quality Score   Unique Depth    Align Depth     Total Depth     Signal  StdDeviation
				position = int(fields[0])
				depth = int(fields[4])
				counts[id][position-1] = depth
				
	keys = counts.keys()
	print "id\tlength\tmedian_depth"
	for id in keys:
		depth_median = numpy.median(counts[id])
		print id + "\t" + str(lengths[id]) + "\t" + str(depth_median)