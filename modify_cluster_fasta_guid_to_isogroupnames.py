import sys,os,sqlite3
import newick3,phylo3
from Bio import SeqIO

checknames = ['Frillagalma', 'Nanomia']

nanomia_isotig_to_isogroup_file =  "/media/hd2/SIPHONOPHORE/grant_clusters/nanomia_mapfile"
frill_isotig_to_isogroup_file =  "/media/hd2/SIPHONOPHORE/grant_clusters/frill_mapfile"

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python modify_cluster_fasta_guid_to_isogroupnames.py database fastadir outdir"
		sys.exit(0)
	
	database = sys.argv[1]
	
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()
	
	nanomia_isotig_to_isogroup_map = {}
	tfile = open(nanomia_isotig_to_isogroup_file,"r")
	for i in tfile:
		spls = i.strip().split("\t")
		for j in range(len(spls)-1):
			nanomia_isotig_to_isogroup_map[spls[j+1]] = spls[0]
	tfile.close()
	
	frill_isotig_to_isogroup_map = {}
	tfile = open(frill_isotig_to_isogroup_file,"r")
	for i in tfile:
		spls = i.strip().split("\t")
		for j in range(len(spls)-1):
			frill_isotig_to_isogroup_map[spls[j+1]] = spls[0]
	tfile.close()
	
	fastadir = sys.argv[2]
	outdir = sys.argv[3]
	for i in os.listdir(fastadir):
		if ".fasta" in i:
			print i
			tfile = open(fastadir+"/"+i,"r")
			seqs = []
			for j in SeqIO.parse(tfile,"fasta"):
				#print j.label
				j.name = ""
				j.description = ""
				spls = j.id.split("@")
				if spls[0] in checknames:
					cur.execute("SELECT * FROM translated_seqs WHERE id = ?",(spls[1],))
					a = cur.fetchall()
					if len(a) > 0:
						try:
							if spls[0] == "Nanomia":
								j.id = spls[0]+"@"+nanomia_isotig_to_isogroup_map[str(a[0][2])]
							elif spls[0] == "Frillagalma":
								j.id = spls[0]+"@"+frill_isotig_to_isogroup_map[str(a[0][2])]
						except:
							j.id = spls[0]+"@"+str(a[0][2])
				seqs.append(j)
				#print j.label
			tfile.close()
			ofile = open(outdir+"/"+i,"w")
			SeqIO.write(seqs,ofile,"fasta")
			ofile.close()
			#sys.exit(0)
	
